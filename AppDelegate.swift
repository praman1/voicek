//
//  AppDelegate.swift
//  Voicek
//
//  Created by Prashanth on 18/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//
//Original Bundle ID:com.volive.VoicekApp
//Account Name:Ss.alkhozaim@hotmail.com
import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKCoreKit
import TwitterKit
import MOLH
import AVKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var signInFor = NSString()
    var soundPlayer : AVAudioPlayer?
    var recordingSession : AVAudioSession?
 
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        MOLHLanguage.setDefaultLanguage("en")
        MOLH.shared.activate(true)
        UserDefaults.standard.removeObject(forKey: "StringType")
        if UserDefaults.standard.bool(forKey: "Loggedin") == true {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC")
            let navigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
           // navigationController.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
       // UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")
        
//        if UserDefaults.standard.string(forKey: "currentLanguage") == ENGLISH_LANGUAGE{
//            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
//        }
//        else
//        {
//            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
//        }
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey:"DzoUmphsfj04bxlBIKPOgVj8i", consumerSecret:"w3W4jxHTqfbShA0N5GDCitUbSKokfGpETVR7qBfitScQKW5lXz")
        if #available(iOS 10.0, *){
            
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }else{
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            print("User Notifications else")
        }
        IQKeyboardManager.shared.enable = true
        return true
    }
 
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
       // let handled
        print("2\(signInFor)")
        
        if signInFor == "fb"{
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        else{
            return TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        }
        // Add any custom logic here.
        //return handled
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("didRegister")
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        let defaults = UserDefaults.standard
        defaults.set(deviceTokenString, forKey: "deviceToken")
        // Persist it in your backend in case it's new
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
        
        let dic = data as NSDictionary
        print("Notification data Is: \(dic)")
        NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        if signInFor == "Fb"{
            FBSDKAppEvents.activateApp()
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
//        recordingSession = AVAudioSession.sharedInstance()
//        do{
//            try recordingSession!.setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
//            try recordingSession!.setActive(true)
//        }catch{
//            print("failed to Play")
//        }
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if UserDefaults.standard.bool(forKey: "Loggedin") == true {
            print("if truu")
        }else{
            print("if false")
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate{
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void)
    {
        let userInfo = notification.request.content.userInfo as! Dictionary <String,Any>
       
        print("Notification data is:\(userInfo)")
        let aps = userInfo["aps"] as? Dictionary<String,Any>
        let infoDict = aps!["info"] as? Dictionary<String,Any>
        
        //let msg = infoDict!["message"] as! String
        let title = infoDict!["type"] as! String
        let postId = infoDict!["id"] as! String
        print(title)
        
        //accepted and rejected
//        if title == "Likes" || title == "following" || title == "following_request" {
//            let rootViewController = self.window?.rootViewController as! UINavigationController
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let mvc = storyboard.instantiateViewController(withIdentifier: "Activities") as!
//            Activities
//            rootViewController.pushViewController(mvc, animated: false)
//            NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
//        }
    //        else if title == "accepted" || title == "rejected"{
//            let rootViewController = self.window?.rootViewController as! UINavigationController
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let mvc = storyboard.instantiateViewController(withIdentifier: "HomeVC1") as!
//            HomeVC
//            //rootViewController.pushViewController(mvc, animated: false)
//            rootViewController.present(mvc, animated: false, completion: nil)
//            //NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
//
//        }
//            else if title == "comment"{
//            let rootViewController = self.window?.rootViewController as! UINavigationController
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let mvc = storyboard.instantiateViewController(withIdentifier: "Comments") as!
//            Comments
//            //mvc.fromAppStr = "appDelegate"
//            mvc.postIdStr = postId
//            rootViewController.pushViewController(mvc, animated: false)
//            NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
//        }

        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
      //completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue)|UInt8(UNNotificationPresentationOptions.sound.rawValue))))
        
        // NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void)
    {
        let userInfo = response.notification.request.content.userInfo
        
        print("Notification data is:\(userInfo)")
        let aps = userInfo["aps"] as? Dictionary<String,Any>
        let infoDict = aps!["info"] as? Dictionary<String,Any>
        
        //let msg = infoDict!["message"] as! String
        let title = infoDict!["type"] as! String
        let postId = infoDict!["id"] as! String
        let userName = infoDict!["user_name"] as! String
//        let x3 : Int = infoDict!["id"] as! Int
//        let xNSNumber3 = x3 as NSNumber
//        let postId : String = xNSNumber3.stringValue

        print(title)
         //accepted and rejected
        if title == "Likes" || title == "following" || title == "following_request" {
            let rootViewController = self.window?.rootViewController as! UINavigationController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mvc = storyboard.instantiateViewController(withIdentifier: "Activities") as!
            Activities
            rootViewController.pushViewController(mvc, animated: false)
           // NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
        }else if title == "accepted" || title == "rejected"{
            let rootViewController = self.window?.rootViewController as! UINavigationController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mvc = storyboard.instantiateViewController(withIdentifier: "HomeVC1") as!
            HomeVC
            //rootViewController.pushViewController(mvc, animated: false)
            rootViewController.present(mvc, animated: false, completion: nil)
            //NotificationCenter.default.post(name: NSNotification.Name("reloadTable"), object: nil)
            
        }else if title == "comment"{
            let rootViewController = self.window?.rootViewController as! UINavigationController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mvc = storyboard.instantiateViewController(withIdentifier: "Comments") as!
            Comments
            //mvc.fromAppStr = "appDelegate"
            mvc.postIdStr = postId
            rootViewController.pushViewController(mvc, animated: true)
        }//private_post
        else if title == "private_post"{
            let rootViewController = self.window?.rootViewController as! UINavigationController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mvc = storyboard.instantiateViewController(withIdentifier: "Details") as!
            Details
            //mvc.fromAppStr = "appDelegate"
            mvc.postId = postId
            mvc.profileNameString = userName
            rootViewController.pushViewController(mvc, animated: true)
        }
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let Activities = storyboard.instantiateViewController(withIdentifier: "Activities") as? Activities
//        let viewcontroller = UIApplication.shared.keyWindow?.rootViewController
//        let navigation = UINavigationController.init(rootViewController: Activities!)
//        //viewcontroller?.push(navigation, animated: true, completion: nil)
//        viewcontroller.pushViewController(navigation, animated: true)
        
        completionHandler()
    }
}

