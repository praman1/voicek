//
//  FindFriends.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu

class FindFriends: UIViewController {
  
    //hide Content
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var connect: UIButton!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var facebookConnect: UILabel!
    
    @IBOutlet weak var findFriendsTable: UITableView!
    
    
    var nameArr = [String]()
    var emailArr = [String]()
    //var imageArr = [String]()
    var imageArr : [UIImage] = [#imageLiteral(resourceName: "Oval2"),#imageLiteral(resourceName: "Oval"),#imageLiteral(resourceName: "Oval3"),#imageLiteral(resourceName: "Oval6"),#imageLiteral(resourceName: "Oval22")]
    var btnNamesArr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        nameArr = ["Abdullah Ahmed","Tarek Mohamed","Read Hisham","Ibrahim Khaled","Ibrahim Khaled"]
        emailArr = ["@Abdullah","@Tarek","@Read","@Ibrahim","@Ibrahim"]
        btnNamesArr = ["Listening","Listen","Listen","Rewuested","Listen"]
        
        self.navigationItem.title = languageChangeString(a_str: "Find Friends")
        let backBtn = UIBarButtonItem(image: UIImage(named:"MenuWhite"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
    }
    
    @objc func backBtnClicked()
    {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func connectBtn(_ sender: Any) {
    }
}
extension FindFriends: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : BlockedCell 
        
        cell = findFriendsTable.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! BlockedCell
       
        cell.frndLbl.text = nameArr[indexPath.row]
        cell.frndEmailLbl.text = emailArr[indexPath.row]
        cell.frndImg.image = imageArr[indexPath.row]
        
//        switch indexPath.row {
//        case 0:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
//            cell.listenBtn.setTitle("Listening", for: UIControlState.normal)
//        case 1:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5058823529, blue: 0.9529411765, alpha: 1)
//            cell.listenBtn.setTitle("Listen", for: UIControlState.normal)
//        case 2:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5058823529, blue: 0.9529411765, alpha: 1)
//            cell.listenBtn.setTitle("Listen", for: UIControlState.normal)
//        case 3:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//            cell.listenBtn.setTitle("Requested", for: UIControlState.normal)
//            cell.listenBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
//
//        case 4:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5058823529, blue: 0.9529411765, alpha: 1)
//            cell.listenBtn.setTitle("Listen", for: UIControlState.normal)
//
//        default:
//            cell.listenBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5058823529, blue: 0.9529411765, alpha: 1)
//            cell.listenBtn.setTitle("Listen", for: UIControlState.normal)
//        }
        return cell
   
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 87
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

