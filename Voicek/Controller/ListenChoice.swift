//
//  ListenChoice.swift
//  Voicek
//
//  Created by Suman Guntuka on 24/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class ListenChoice: UIViewController {

    @IBOutlet var unlistenLabel: UILabel!
    
    @IBOutlet var unListenBtn: UIButton!
    @IBOutlet var cancelBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
        //self.unlistenLabel.text = languageChangeString(a_str: "")
        self.unListenBtn.setTitle(languageChangeString(a_str: "Unlisten"), for: UIControlState.normal)
        self.cancelBtn.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }
    @objc func hideView()
    {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unlistenBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
