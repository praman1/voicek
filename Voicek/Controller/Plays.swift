//
//  Plays.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class Plays: UIViewController {

    @IBOutlet weak var likedby: UILabel!
    @IBOutlet weak var playsLbl: UILabel!
    @IBOutlet weak var playTable: UITableView!
    
    var ownerUserIdStr : String! = ""
    var postIdString : String! = ""
    var postUserIdStr : String! = ""
    
    var userIdArray = [String]()
    var postIdArray = [String]()
    var nameArray = [String]()
    var userNameArray = [String]()
    var isPublicArray = [String]()
    var imageArray = [String]()
    var requestedArray = [String]()
    var playedUsersData : [[String : AnyObject]]!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ownerUserIdStr = UserDefaults.standard.object(forKey: "userid") as? String
       
        self.navigationItem.title = languageChangeString(a_str: "Plays")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        playedUsersListServiceCall()
        
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func followClicked(sender : UIButton){
        let buttonRow = sender.tag
        postUserIdStr = userIdArray[buttonRow]
        followOrUnfollowServiceCall()
    }
    
    //MARK:FOLLOW OR UNFOLLOW SERVICE CALL
    func followOrUnfollowServiceCall()
    {
        // http://volive.in/voicek/api/services/follower?API-KEY=225143&user_id=3783858425&follower_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)follower?"
        print("Report Post URL Is :\(deleteUserPost)")

        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "follower_id" : postUserIdStr!]

        print(parameters)

        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)

                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String

                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)

                    let x1 : Int = responseData["followed"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let followed : String = xNSNumber1.stringValue

                    UserDefaults.standard.set(followed, forKey: "follow")
                    self.playedUsersListServiceCall()
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    //MARK:PLAYED USERS LIST SERVICE CALL
    func playedUsersListServiceCall(){
        //http://volive.in/voicek/api/services/played_users?API-KEY=225143&user_id=3935547725&post_id=263
        Services.sharedInstance.loader(view: self.view)
        let played_users = "\(Base_Url)played_users?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "post_id" : postIdString!]
        
        print(parameters)
        
        Alamofire.request(played_users, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let x : Int = responseData["like_count"] as! Int
                let xNSNumber = x as NSNumber
                let like_count : String = xNSNumber.stringValue
                self.likedby.text = String(format: "%@ %@ %@", self.languageChangeString(a_str: "Liked by")!,like_count,self.languageChangeString(a_str: "People")!)
                
                if status == 1
                {
                    self.userIdArray = [String]()
                    self.postIdArray = [String]()
                    self.nameArray = [String]()
                    self.userNameArray = [String]()
                    self.isPublicArray = [String]()
                    self.imageArray = [String]()
                    self.requestedArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    self.playedUsersData = responseData["data"] as? [[String:AnyObject]]
                    
                    let arrayCount : Int = self.playedUsersData.count
                    let xNSNumber = arrayCount as NSNumber
                    let count : String = xNSNumber.stringValue
                    
                    print("Array Count Is :\(count)")
                    self.playsLbl.text = String(format: "%@ %@", count,self.languageChangeString(a_str: "Plays")!)
                    
                    for each in self.playedUsersData
                    {
                        let userId = each["user_id"] as? String
                        let post_id = each["post_id"] as? String
                        let name = each["name"] as? String
                        
                        let user_name = each["user_name"] as? String
                        let is_public = each["is_public"] as? String
                        let image = each["image"] as? String
                        
                        let x : Int = each["requested"] as! Int
                        let xNSNumber = x as NSNumber
                        let request : String = xNSNumber.stringValue
                        
                        self.userIdArray.append(userId!)
                        self.postIdArray.append(post_id!)
                        self.nameArray.append(name!)
                        self.userNameArray.append(user_name!)
                        self.isPublicArray.append(is_public!)
                        self.imageArray.append(image!)
                        self.requestedArray.append(request)
                    }
                    DispatchQueue.main.async {
                        self.playTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
        
    }
}

extension Plays: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userIdArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : BlockedCell
        
        cell = playTable.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! BlockedCell
        
        cell.frndImg.layer.cornerRadius = cell.frndImg.frame.size.height/2
        cell.frndLbl.text = nameArray[indexPath.row]
        cell.frndEmailLbl.text = String(format: "@%@", userNameArray[indexPath.row])
        cell.frndImg.sd_setImage(with: URL (string: imageArray[indexPath.row]), placeholderImage: UIImage(named: "DefaultUser"))
        cell.listenBtn.tag = indexPath.row
        
        cell.listenBtn.addTarget(self, action: #selector(followClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        if requestedArray[indexPath.row] == "1" {
            cell.listenBtn.setImage(UIImage.init(named: "Following"), for: UIControlState.normal)
        }else if requestedArray[indexPath.row] == "2"{
            cell.listenBtn.setImage(UIImage.init(named: "Follow"), for: UIControlState.normal)
        }else if requestedArray[indexPath.row] == "3"{
            cell.listenBtn.setImage(UIImage.init(named: "Request"), for: UIControlState.normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = userIdArray[indexPath.row]
        profile.fromSearchStr = "search"
        self.navigationController?.pushViewController(profile, animated: false)
    }
}
