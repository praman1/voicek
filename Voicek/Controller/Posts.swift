//
//  Posts.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class Posts: UIViewController {
    @IBOutlet weak var recordBtn: UIButton!
    
    @IBOutlet weak var postTable: UITableView!
    
    var postUserId : String! = ""
    var ownerUserId : String! = ""
    var checkStr : String! = ""
    var testString : String! = ""
    var postUserIdStr : String! = ""
    
    var fromSwipePostUserId : String! = ""
    
    var postIdArr = [String]()
    var userIdArr = [String]()
    var imageArr = [String]()
    var audioArr = [String]()
    var durationArr = [String]()
    var descArr = [String]()
    var nameArr = [String]()
    var isPublicArr = [String]()
    var playedArr = [String]()
    var likesArr = [String]()
    var notiCountArr = [String]()
    var postsData : [[String : AnyObject]]?
    var refreshControl: UIRefreshControl?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.postUserId = UserDefaults.standard.object(forKey: "postUserId") as? String
        self.testString = UserDefaults.standard.object(forKey: "testString") as? String
        self.checkStr = UserDefaults.standard.object(forKey: "checkString") as? String
        ownerUserId = UserDefaults.standard.object(forKey: "userid") as? String
        
        if testString == "popToProfile" {
            if ownerUserId == postUserId {
                ownerUserId = postUserId
                UserDefaults.standard.removeObject(forKey: "postId")
                recordBtn.isHidden = false
                recordBtn.layer.cornerRadius = recordBtn.layer.frame.size.height/2
                recordBtn.layer.shadowColor = UIColor.init(red: 0.0/255.0, green: 218.0/255.0, blue: 244.0/255.0, alpha: 1).cgColor
                //recordBtn.layer.shadowColor = UIColor.black.cgColor
                //recordBtn.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                recordBtn.layer.shadowOpacity = 1.0
                recordBtn.layer.shadowRadius = 2.0
                recordBtn.layer.masksToBounds = false
                
                //recordBtn.layer.cornerRadius = 3.0
            }else{
                recordBtn.isHidden = true
            }
        }else{
             //postUserId = UserDefaults.standard.object(forKey: "postId") as? String
            if ownerUserId == postUserId {
                ownerUserId = postUserId
                UserDefaults.standard.removeObject(forKey: "postId")
                recordBtn.isHidden = false
                recordBtn.layer.cornerRadius = recordBtn.layer.frame.size.height/2
                recordBtn.layer.shadowColor = UIColor.init(red: 0.0/255.0, green: 218.0/255.0, blue: 244.0/255.0, alpha: 1).cgColor
                //recordBtn.layer.shadowColor = UIColor.black.cgColor
                //recordBtn.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                recordBtn.layer.shadowOpacity = 1.0
                recordBtn.layer.shadowRadius = 2.0
                recordBtn.layer.masksToBounds = false
                
                //recordBtn.layer.cornerRadius = 3.0
            }else{
                recordBtn.isHidden = true
            }
        }
        
        
       // if testString == "popToProfile" {
            let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
            backBtn.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = backBtn
       // }
        
        if checkStr == "fromSide" {
            postUserId = ownerUserId
        }
        
        self.navigationItem.title = languageChangeString(a_str: "Posts")
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.down
//        self.view.addGestureRecognizer(swipeRight)
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.clear
        postTable.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(self.dismissView), for: .valueChanged)
        //getUserPostsServiceCall()
    }
    
    @objc func dismissView() {
        
        // getPostsServiceCall()
        DispatchQueue.main.async(execute: {
            //self.refreshControl?.endRefreshing()
//            self.homeTable.reloadData()
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    @objc func backBtnClicked(){
        if testString == "popToProfile" {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "fromSide") != nil{
            checkStr = (UserDefaults.standard.object(forKey: "fromSide") as! String)
            getUserPostsServiceCall()
        }else{
            getUserPostsServiceCall()
        }
    }
    
    @IBAction func menuBtn(_ sender: Any) {
    present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
//    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
//        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
//            switch swipeGesture.direction {
//            case UISwipeGestureRecognizerDirection.down:
//                self.dismiss(animated: true, completion: nil)
//            default:
//                break
//            }
//        }
//    }

    @IBAction func postBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "Record")
        self .present(home, animated: true, completion: nil)
    }
    
    //MARK: Get User Posts Servicecall
    func getUserPostsServiceCall()
    {
        // http://volive.in/voicek/api/services/get_user_posts?API-KEY=225143&user_id=4182668577&profile_user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)get_user_posts?"
        let parameters: Dictionary<String, Any>
        
        if checkStr == "fromSide" {
            parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : ownerUserId!]
        }
        else{
            parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : postUserId!]
        }
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    self.postIdArr = [String]()
                    self.userIdArr = [String]()
                    self.imageArr = [String]()
                    self.audioArr = [String]()
                    self.durationArr = [String]()
                    self.descArr = [String]()
                    self.nameArr = [String]()
                    self.isPublicArr = [String]()
                    self.playedArr = [String]()
                    self.likesArr = [String]()
                    self.notiCountArr = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.postsData = responseData["data"]  as? [[String : AnyObject]]
                    
                    for each in self.postsData!
                    {
                        let postId = each["id"] as? String
                        let userId = each["user_id"] as? String

                        let image = each["image"] as? String
                        let audio = each["audio"] as? String

                        let duration = each["duration"] as? String
                        let description = each["description"] as? String

                        let name = each["name"] as? String
                        let is_public = each["is_public"] as? String

                        let x : Int = each["played"] as! Int
                        let xNSNumber = x as NSNumber
                        let played : String = xNSNumber.stringValue

                        let x1 : Int = each["likes"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let likes : String = xNSNumber1.stringValue
                        
                        let x3 : Int = each["noti_count"] as! Int
                        let xNSNumber3 = x3 as NSNumber
                        let noti_count : String = xNSNumber3.stringValue

                        self.postIdArr.append(postId!)
                        self.userIdArr.append(userId!)

                        self.imageArr.append(image!)
                        self.audioArr.append(audio!)

                        self.durationArr.append(duration!)
                        self.descArr.append(description!)

                        self.nameArr.append(name!)
                        self.isPublicArr.append(is_public!)

                        self.playedArr.append(played)
                        self.likesArr.append(likes)
                        self.notiCountArr.append(noti_count)
                    }
                    DispatchQueue.main.async {
                        self.postTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @objc func playClicked(sender:UIButton)
    {
        let buttonRow = sender.tag
        
        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
        play.postId = postIdArr[buttonRow]
        play.postUserId = userIdArr[buttonRow]
        play.profileNameString = nameArr[buttonRow]
        self.navigationController?.pushViewController(play, animated: true)
    }
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        self.dismiss(animated: true, completion: nil)
//    }
}

extension Posts: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : HomeCell
        
        cell = postTable.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
        cell.postsBadgeLabel.layer.cornerRadius = cell.postsBadgeLabel.frame.size.height/2
        
        let x2 : Int = Int(self.notiCountArr[indexPath.row])!
        let xNSNumber2 = x2 as NSNumber
        let noti_count : String = xNSNumber2.stringValue
        
        if x2 > 0{
            cell.postsBadgeLabel.isHidden = false
        }else{
            cell.postsBadgeLabel.isHidden = true
        }
        cell.postsBadgeLabel.text = noti_count
        
        cell.nameLbl.text = nameArr[indexPath.row]
        cell.homeImg.sd_setImage(with: URL (string: imageArr[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
        cell.subNameLbl.text = descArr[indexPath.row]
        cell.likesLBl.text = String(format: "%@ %@", likesArr[indexPath.row],self.languageChangeString(a_str: "Likes")!)
        cell.playedLbl.text = String(format: "%@ %@", playedArr[indexPath.row],self.languageChangeString(a_str: "Played")!)
        cell.secLbl.text = durationArr[indexPath.row]
        cell.playBtn.tag = indexPath.row
        cell.playBtn.addTarget(self, action: #selector(playClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
        play.postId = postIdArr[indexPath.row]
        play.postUserId = userIdArr[indexPath.row]
        play.profileNameString = nameArr[indexPath.row]
        self.navigationController?.pushViewController(play, animated: true)
    }
    
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
//        
//    }
}
