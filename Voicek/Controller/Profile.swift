//
//  Profile.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import Alamofire


class Profile: UIViewController {

    @IBOutlet weak var bioTV: UITextView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var listenersBtn: UIButton!
    @IBOutlet weak var listeningBtn: UIButton!
    @IBOutlet weak var postsBtn: UIButton!
    @IBOutlet weak var posts: UILabel!
   
    @IBOutlet weak var listening: UILabel!
    
    @IBOutlet weak var listeners: UILabel!
    
    @IBOutlet weak var profileMail: UILabel!
    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet var followBtn: UIButton!
    
    @IBOutlet weak var privateImage: UIImageView!
    @IBOutlet weak var privateLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    
    var isPublicString : String!
    var blockStr : String!
    
    var checkString : String!
    var ownerUserIdStr : String!
    var postUserIdStr : String!
    
    var fromSearchStr : String!
    
    var swipe = UISwipeGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blockStr = "Block"
        ownerUserIdStr = UserDefaults.standard.object(forKey: "userid") as? String
        UserDefaults.standard.set(postUserIdStr, forKey: "postUserId")
        self.privateImage.isHidden = true
        self.privateLabel.isHidden = true
        self.hintLabel.isHidden = true
        
        if checkString == "fromSide" {
            followBtn.isHidden = true
//            let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
//            backBtn.tintColor = UIColor.white
//            self.navigationItem.leftBarButtonItem = backBtn
        }
        else{
            let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
            backBtn.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = backBtn
            if ownerUserIdStr == postUserIdStr {
                followBtn.isHidden = true
            }
            else{
                followBtn.isHidden = false
            }
        }
        
        if fromSearchStr == "search" {
            let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
            backBtn.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = backBtn
        }
        
        if checkString == "toProfile" {
            let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
            backBtn.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = backBtn
        }
        listenersBtn.layer.cornerRadius = listenersBtn.frame.size.height/2
        listenersBtn.layer.borderWidth = 2
        listenersBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        listeningBtn.layer.cornerRadius = listeningBtn.frame.size.height/2
        listeningBtn.layer.borderWidth = 2
        listeningBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        postsBtn.layer.cornerRadius = postsBtn.frame.size.height/2
        postsBtn.layer.borderWidth = 2
        postsBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        //isPublicString = UserDefaults.standard.object(forKey: "is_public") as! String
        
        self.navigationItem.title = languageChangeString(a_str: "Profile")
        swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipe.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipe)
        ownerProfileServiceCall()
        
        NotificationCenter.default.addObserver(self, selector: #selector(unblockCall), name: Notification.Name("Blocked"), object: nil)
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func unblockCall()
    {
        blockStr = UserDefaults.standard.object(forKey: "Blocked") as? String
        if self.blockStr == "Unblock"{
            self.privateImage.isHidden = false
            self.privateLabel.isHidden = false
            self.hintLabel.isHidden = false
            self.followBtn.isHidden = true
            self.privateImage.image = UIImage.init(named: "UserBlocked")
            self.privateLabel.text = languageChangeString(a_str: "This User Is Blocked")
        }else{
            self.privateImage.isHidden = true
            self.privateLabel.isHidden = true
            self.hintLabel.isHidden = true
            self.followBtn.isHidden = false
            ownerProfileServiceCall()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ownerProfileServiceCall()
    }

    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
     
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.up:
                if checkString == "fromSide"
                {
                    UserDefaults.standard.set(checkString, forKey: "checkString")
                }
                else{
                    
                    UserDefaults.standard.removeObject(forKey: "fromSide")
                }
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let service = storyBoard.instantiateViewController(withIdentifier: "Posts")
                self .present(service, animated: true, completion: nil)
                
            default:
                break
            }
        }
    }
    
    //MARK:OWNER PROFILE SERVICE CALL
    func ownerProfileServiceCall()
    {
        // http://volive.in/voicek/api/services/view_profile?API-KEY=225143&user_id=4182668577&porp_user_id=4182668577
        
        Services.sharedInstance.loader(view: self.view)
        
        let profileDetails = "\(Base_Url)view_profile?"
        
        let parameters: Dictionary<String, Any>
        
        if checkString == "fromSide" {
            parameters = [ "API-KEY" :APIKEY, "user_id":ownerUserIdStr! , "porp_user_id":ownerUserIdStr!]
        }
        else{
            parameters = [ "API-KEY" :APIKEY, "user_id":ownerUserIdStr! , "porp_user_id":postUserIdStr!]
        }
        //3783858425
        print(parameters)
        
        Alamofire.request(profileDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let responseData = response.result.value as? Dictionary<String, Any>
            print(responseData!)
            
            let status = responseData!["status"] as! Int
            // let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                    
                    let userId = responseData1["porp_user_id"] as! String
                    let username = responseData1["username"] as! String
                    let fullName = responseData1["first_name"] as! String
                    let image = responseData1["image"] as! String
                    let bio = responseData1["bio"] as! String
                    let is_public = responseData1["is_public"] as! String
                    UserDefaults.standard.set(userId, forKey: "postUserId")
                    
                    self.postUserIdStr = userId
                    
                    UserDefaults.standard.set(is_public, forKey: "is_public")
 
                    let x : Int = responseData1["posts_count"] as! Int
                    let xNSNumber = x as NSNumber
                    let posts_count : String = xNSNumber.stringValue
                    
                    let x1 : Int = responseData1["listening_count"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let listening_count : String = xNSNumber1.stringValue
                    
                    let x2 : Int = responseData1["listeners_count"] as! Int
                    let xNSNumber2 = x2 as NSNumber
                    let listeners_count : String = xNSNumber2.stringValue
                    
                    let x3 : Int = responseData1["followed"] as! Int
                    let xNSNumber3 = x3 as NSNumber
                    let followed : String = xNSNumber3.stringValue
                    UserDefaults.standard.set(followed , forKey: "follow")
                    
                    if self.ownerUserIdStr != userId{
                    if is_public != "2" && followed == "1"{
                        print("1st loop")
                        self.postsBtn.isEnabled = true
                        self.listeningBtn.isEnabled = true
                        self.listenersBtn.isEnabled = true
                        self.view.addGestureRecognizer(self.swipe)
                        
                        self.privateImage.isHidden = true
                        self.privateLabel.isHidden = true
                        self.hintLabel.isHidden = true
                        self.profileImg.isHidden = false
                        self.followBtn.setImage(UIImage (named: "Following"), for: UIControlState.normal)
                        
                    }else
                    {
                       if is_public == "2"{
                        if followed == "1"{
                            print("2nd loop")
                            self.followBtn.setImage(UIImage (named: "Following"), for: UIControlState.normal)
                            self.postsBtn.isEnabled = true
                            self.listeningBtn.isEnabled = true
                            self.listenersBtn.isEnabled = true
                            self.view.addGestureRecognizer(self.swipe)
                            
                            self.privateImage.isHidden = true
                            self.privateLabel.isHidden = true
                            self.hintLabel.isHidden = true
                            self.profileImg.isHidden = false
                        }else if followed == "2"{
                            print("3rd loop")
                            self.followBtn.setImage(UIImage (named: "Follow"), for: UIControlState.normal)
                            self.postsBtn.isEnabled = false
                            self.listeningBtn.isEnabled = false
                            self.listenersBtn.isEnabled = false
                            self.view.removeGestureRecognizer(self.swipe)
                            
                            self.privateImage.isHidden = false
                            self.privateLabel.isHidden = false
                            self.hintLabel.isHidden = false
                            self.profileImg.isHidden = true
                            self.privateImage.image = UIImage.init(named: "UserPrivate")
                            self.privateLabel.text = self.languageChangeString(a_str: "This User Is Private")
                            self.hintLabel.text = self.languageChangeString(a_str: "Request access to listen to this user's voice")
                        }else if followed == "3"{
                            print("3rd loop")
                            self.followBtn.setImage(UIImage (named: "Request"), for: UIControlState.normal)
                            self.postsBtn.isEnabled = false
                            self.listeningBtn.isEnabled = false
                            self.listenersBtn.isEnabled = false
                            self.view.removeGestureRecognizer(self.swipe)
                            
                            self.privateImage.isHidden = false
                            self.privateLabel.isHidden = false
                            self.hintLabel.isHidden = false
                            self.profileImg.isHidden = true
                            self.privateImage.image = UIImage.init(named: "UserPrivate")
                            self.privateLabel.text = self.languageChangeString(a_str: "This User Is Private")
                            self.hintLabel.text = self.languageChangeString(a_str: "Request access to listen to this user's voice")
                        }
                       
                        }else{
                        print("4th loop")
                        self.postsBtn.isEnabled = true
                        self.listeningBtn.isEnabled = true
                        self.listenersBtn.isEnabled = true
                        self.view.addGestureRecognizer(self.swipe)
                        
                        self.privateImage.isHidden = true
                        self.privateLabel.isHidden = true
                        self.hintLabel.isHidden = true
                        self.profileImg.isHidden = false
                        self.followBtn.setImage(UIImage (named: "Follow"), for: UIControlState.normal)
                        }
                       }
                    }
                    self.postsBtn.setTitle(posts_count, for: UIControlState.normal)
                    self.listeningBtn.setTitle(listening_count, for: UIControlState.normal)
                    self.listenersBtn.setTitle(listeners_count, for: UIControlState.normal)
                    
                    self.profileImg.sd_setImage(with: URL (string: image), placeholderImage: UIImage(named: "Logo"))
                    self.profileName.text = fullName
                    self.profileMail.text = String(format: "@%@", username)
                    self.bioTV.text = bio

                    print(userId)
                    print(is_public)
                    print(followed)
                }
            }
            else
            {
                let message = responseData!["message"] as! String
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    //MARK:FOLLOW OR UNFOLLOW SERVICE CALL
    func followOrUnfollowServiceCall()
    {
        // http://volive.in/voicek/api/services/follower?API-KEY=225143&user_id=3783858425&follower_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)follower?"
        print("Report Post URL Is :\(deleteUserPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "follower_id" : postUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    
                    let x1 : Int = responseData["followed"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let followed : String = xNSNumber1.stringValue
                    
                    UserDefaults.standard.set(followed, forKey: "follow")
                    self.ownerProfileServiceCall()
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @IBAction func follow(_ sender: Any) {
    
        followOrUnfollowServiceCall()
    }
    
    @IBAction func editBtnAction(_ sender: Any) {
        
        //let followOrUnfollow = UserDefaults.standard.object(forKey: "follow") as! String
       
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let editAction: UIAlertAction = UIAlertAction(title: "Edit Profile", style: .default) { action -> Void in
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "EditProfile")
            
            DispatchQueue.main.async {
                self .present(service, animated: true, completion: nil)
            }
        }
        
        let reportAction: UIAlertAction = UIAlertAction(title: "Report", style: .default) { action -> Void in
            
            let reportUser = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportUser") as! ReportUser
            reportUser.ownerUserId = self.ownerUserIdStr
            reportUser.postUserId = self.postUserIdStr
            reportUser.reportCheckStr = "fromProfile"
            self.navigationController?.pushViewController(reportUser, animated: true)
        }
        
        let blockAction: UIAlertAction = UIAlertAction(title: blockStr, style: .default) { action -> Void in
            
            UserDefaults.standard.set(self.ownerUserIdStr, forKey: "ownerUserId")
            UserDefaults.standard.set(self.postUserIdStr, forKey: "postUserId")
            UserDefaults.standard.set(self.checkString, forKey: "check")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let blockUser = storyBoard.instantiateViewController(withIdentifier: "BlockUser")

            DispatchQueue.main.async {
                self .present(blockUser, animated: true, completion: nil)
            }
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        // add actions
        
        if checkString == "fromSide"{
            actionSheetController.addAction(editAction)
        }
        else
        {
            if ownerUserIdStr != postUserIdStr
            {
                actionSheetController.addAction(reportAction)
                actionSheetController.addAction(blockAction)
            }
            else{
                actionSheetController.addAction(editAction)
            }
        }
        actionSheetController.addAction(cancelAction)
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func posts(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let posts = storyBoard.instantiateViewController(withIdentifier: "Posts")
        
//        posts.postUserId = self.postUserIdStr
//        //posts.ownerUserId = self.ownerUserIdStr
//        posts.checkStr = self.checkString
        let testString = "popToProfile"
        
        UserDefaults.standard.set(postUserIdStr, forKey: "postUserId")
        UserDefaults.standard.set(checkString, forKey: "checkString")
        UserDefaults.standard.set(testString, forKey: "testString")
        
        UserDefaults.standard.removeObject(forKey: "fromSide")
        self .present(posts, animated: true, completion: nil)
        //self.navigationController?.pushViewController(posts, animated: true)
        print("Posts Clicked")
    }
    
    @IBAction func listening(_ sender: Any) {
        let listening = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Listening") as! Listening
        listening.postUserId = self.postUserIdStr
        listening.ownerUserId = self.ownerUserIdStr
        listening.checkStr = self.checkString
        //listening.testString = "popToProfile"
        self.navigationController?.pushViewController(listening, animated: true)
        print("Listening Clicked")
    }
    
    @IBAction func listeners(_ sender: Any) {
        let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Listeners") as! Listeners
        listeners.postUserId = self.postUserIdStr
        listeners.ownerUserId = self.ownerUserIdStr
        listeners.checkStr = self.checkString
        //listeners.testString = "popToProfile"
        self.navigationController?.pushViewController(listeners, animated: true)
        print("Listeners Clicked")
    }
    
    @IBAction func sideMenuBtn(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
