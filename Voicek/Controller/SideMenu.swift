//
//  SideMenu.swift
//  Voicek
//
//  Created by volivesolutions on 19/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class SideMenu: UIViewController {

    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var sideMenuTable: UITableView!
    
    var checkArray = [String]()
    var menuNamesArr = [String]()
    
    var menuImgArr : [UIImage] = [#imageLiteral(resourceName: "HomeGray"),#imageLiteral(resourceName: "SearchGray"),#imageLiteral(resourceName: "FindFriendsGray"),#imageLiteral(resourceName: "BeatGray"),#imageLiteral(resourceName: "SettingsGray"),#imageLiteral(resourceName: "ShareGray")]
    var menuColorImgArr : [UIImage] = [#imageLiteral(resourceName: "HomeBlue"),#imageLiteral(resourceName: "SearchBlue"),#imageLiteral(resourceName: "FindFriendBlue"),#imageLiteral(resourceName: "BeatBlue"),#imageLiteral(resourceName: "SettingsBlue"),#imageLiteral(resourceName: "ShareBlue")]
    var selectArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isHidden = true
        profileImg.layer.cornerRadius = profileImg.frame.size.height/2
        menuNamesArr = [languageChangeString(a_str: "Home") , languageChangeString(a_str: "Search") , languageChangeString(a_str: "Find Friends") , languageChangeString(a_str: "Activities"), languageChangeString(a_str: "Settings"), languageChangeString(a_str: "Share the App")] as! [String]
        checkArray = ["1","0","0","0","0","0"]
        
        emailLbl.text = NSString(format: "@%@",(UserDefaults.standard.object(forKey: "userName") as! String)) as String
        nameLbl.text = (UserDefaults.standard.object(forKey: "first_name") as! String)
        DispatchQueue.main.async {
            self.profileImg.sd_setImage(with: URL (string: UserDefaults.standard.object(forKey: "image") as! String ), placeholderImage:
                UIImage(named: "DefaultUser"))
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        animateTable()
    }
    
    func animateTable() {
        DispatchQueue.main.async {
            self.menuNamesArr = [self.languageChangeString(a_str: "Home") , self.languageChangeString(a_str: "Search") , self.languageChangeString(a_str: "Find Friends") , self.languageChangeString(a_str: "Activities"), self.languageChangeString(a_str: "Settings"), self.languageChangeString(a_str: "Share the App")] as! [String]
            //self.checkArray = ["1","0","0","0","0","0"]
            self.sideMenuTable.reloadData()
            let cells = self.sideMenuTable.visibleCells
            let tableHeight: CGFloat = self.sideMenuTable.bounds.size.height
            
            for i in cells {
                let cell: UITableViewCell = i as UITableViewCell
                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            }
            
            var index = 0
            for a in cells {
                let cell: UITableViewCell = a as UITableViewCell
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
                index += 1
            }
        }
        
       
    }
    
    @IBAction func profileBtn(_ sender: Any) {
        
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.checkString = "fromSide"
        
        self.navigationController?.pushViewController(profile, animated: true)
    }
}

//let checkStr = "0"
//self.serviceCheckArr.append(checkStr)
//if serviceCheckArr[indexPath.row] == "0" {
//    cell.img.image = UIImage(named: "unCheck")
//    cell.nameLbl.textColor = UIColor.lightGray
//
//}else{
//    cell.img.image = UIImage(named: "check color")
//    cell.nameLbl.textColor = UIColor.black
//
//    idArr.append(idsting)
//
//    let IdStr = idArr.joined(separator: ",")
//
//    UserDefaults.standard.set( IdStr , forKey: "filterids") //setObject
//    print(UserDefaults.standard.object(forKey: "filterids") ?? "")
//
//}

extension SideMenu: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNamesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : SideMenuCell
        cell = sideMenuTable.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        
        //let checkStr = checkArray[indexPath.row]
        if checkArray[indexPath.row] == "0" {
            cell.menuNameLbl.textColor = UIColor.black
            cell.menuNameLbl.text = menuNamesArr[indexPath.row]
            cell.menuImg.image = menuImgArr[indexPath.row]
            cell.selectView.isHidden = true
        }
        else{
            cell.selectView.isHidden = false
            cell.menuNameLbl.textColor = UIColor.init(red: 0.0/255.0, green: 132.0/255.0, blue: 247.0/255.0, alpha: 1.0)
            cell.menuNameLbl.text = menuNamesArr[indexPath.row]
            cell.menuImg.image = menuColorImgArr[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.row == 0 {
            checkArray[0] = "1"
            sideMenuTable.reloadData()
            let HomeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(HomeVC, animated: false)
            checkArray[1] = "0"
            checkArray[2] = "0"
            checkArray[3] = "0"
            checkArray[4] = "0"
            checkArray[5] = "0"
        }
        else if indexPath.row == 1
        {
            checkArray[indexPath.row] = "1"
            sideMenuTable.reloadData()
            let Search = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(Search, animated: false)
            checkArray[0] = "0"
            checkArray[2] = "0"
            checkArray[3] = "0"
            checkArray[4] = "0"
            checkArray[5] = "0"
            
        }else if indexPath.row == 2
        {
            checkArray[indexPath.row] = "1"
            sideMenuTable.reloadData()
            //checkArray[1] = "0"
            let FindFriends = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FindFriends") as! FindFriends
            self.navigationController?.pushViewController(FindFriends, animated: false)
            checkArray[0] = "0"
            checkArray[1] = "0"
            checkArray[3] = "0"
            checkArray[4] = "0"
            checkArray[5] = "0"
        }
        else if indexPath.row == 3
        {
             checkArray[indexPath.row] = "1"
            sideMenuTable.reloadData()
            let Activities = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Activities") as! Activities
            self.navigationController?.pushViewController(Activities, animated: false)
            checkArray[0] = "0"
            checkArray[1] = "0"
            checkArray[2] = "0"
            checkArray[4] = "0"
            checkArray[5] = "0"
            
        }else if indexPath.row == 4
        {
             checkArray[indexPath.row] = "1"
            sideMenuTable.reloadData()
            let Settings = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Settings") as! Settings
            self.navigationController?.pushViewController(Settings, animated: false)
            checkArray[0] = "0"
            checkArray[1] = "0"
            checkArray[2] = "0"
            checkArray[3] = "0"
            checkArray[5] = "0"
        }
        else if indexPath.row == 5
        {
            checkArray[indexPath.row] = "1"
            sideMenuTable.reloadData()
           // https://j27z8.app.goo.gl/c91e
            let shareVC = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
            shareVC.popoverPresentationController?.sourceView = self.view
            self.present(shareVC, animated: true, completion: nil)

            checkArray[0] = "0"
            checkArray[1] = "0"
            checkArray[2] = "0"
            checkArray[3] = "0"
            checkArray[4] = "0"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 72
    }
}
