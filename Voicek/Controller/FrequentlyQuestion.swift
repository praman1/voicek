//
//  FrequentlyQuestion.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class FrequentlyQuestion: UIViewController {

    var questionsArray = [String]()
    var answersArray = [String]()
    var idArray = [String]()
    var ownerUserIdStr : String! = ""
    var frequentQnsData : [[String : AnyObject]]!
    
    @IBOutlet weak var frequentQnsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.title = languageChangeString(a_str: "Frequently Asked Questions")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        freqentQnsServiceCall()
        
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:FREQUENTLY ASKED QUESTIONS SERVICE CALL
    func freqentQnsServiceCall()
    {
        // http://volive.in/voicek/api/services/questions_answers?API-KEY=225143&user_id=3783858425
        Services.sharedInstance.loader(view: self.view)
        let questions_answers = "\(Base_Url)questions_answers?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
      
        print(parameters)
        
        Alamofire.request(questions_answers, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    self.idArray = [String]()
                    self.questionsArray = [String]()
                    self.answersArray = [String]()
                    
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.frequentQnsData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.frequentQnsData
                    {
                        let qnId = each["id"] as? String
                        let question = each["question"] as? String
                        let answer = each["answer"] as? String
                        
                        self.idArray.append(qnId!)
                        self.questionsArray.append(question!)
                        self.answersArray.append(answer!)
                        
                    }
                    DispatchQueue.main.async {
                        self.frequentQnsTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
        
    }
 
}

extension FrequentlyQuestion: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return idArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : FrequentQnsCell
        
        cell = frequentQnsTable.dequeueReusableCell(withIdentifier: "FrequentQnsCell", for: indexPath) as! FrequentQnsCell
        cell.questionLabel.text = questionsArray[indexPath.row]
        cell.answerLabel.text = answersArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
//        profile.postUserIdStr = self.userIdArr[indexPath.row]
//        self.navigationController?.pushViewController(profile, animated: true)
//
//    }
}
