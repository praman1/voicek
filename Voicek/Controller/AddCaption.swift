//
//  AddCaption.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AddCaption: UIViewController {
    
    var pickerImage  = UIImage()
    var imagePicker = UIImagePickerController()
    var pickedImage  = UIImage()
    var pickedAudio = NSURL()
    var userIdStr : String?
    var duration : Int!
    var tagsList : String! = ""
    var postTypeString : String! = ""
    
    @IBOutlet var addPhotoBtn: UIButton!
    @IBOutlet var writeCaptionLabel: UILabel!
    
    @IBOutlet var addTagsBtn: UIButton!
    
    @IBOutlet weak var captionTF: UITextField!
    
    @IBOutlet weak var captionTV: UITextView!
    @IBOutlet weak var captionImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        print("Picked Audio Is\(pickedAudio)")
        self.captionTV.returnKeyType = .done
        
        captionImage.layer.cornerRadius = captionImage.frame.size.height/2
        userIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        self.navigationItem.title = languageChangeString(a_str: "Add a photo")
        self.addPhotoBtn.setTitle(languageChangeString(a_str: "Add a photo"), for: UIControlState.normal)
        self.writeCaptionLabel.text = languageChangeString(a_str: "Write a Caption")
        self.addTagsBtn.setTitle(languageChangeString(a_str: "Add Tags"), for: UIControlState.normal)
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        let postBtn = UIBarButtonItem(title: languageChangeString(a_str: "Post"),  style: .plain, target: self, action: #selector(postBtnClicked))
        postBtn.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItem = postBtn
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        captionImage.isUserInteractionEnabled = true
        captionImage.addGestureRecognizer(tapGestureRecognizer)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
//        self.view.addGestureRecognizer(tapGesture)
        
    }
    
//    @objc func dismissView(){
//        self.dismiss(animated: true, completion: nil)
//    }
    
    @objc func imageTapped() {
        
        let view = UIAlertController(title: languageChangeString(a_str: "Pick Image"), message: "", preferredStyle: .actionSheet)
        let PhotoLibrary = UIAlertAction(title:  languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
            self.PhotoLibrary()
            view.dismiss(animated: true)
        })
        let camera = UIAlertAction(title: languageChangeString(a_str: "Take a new photo"), style: .default, handler: { action in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
            view.dismiss(animated: true)
        })
        
        let cancel = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
        })
        view.addAction(camera)
        view.addAction(PhotoLibrary)
        view.addAction(cancel)
        present(view, animated: true)
    }
    
    func PhotoLibrary()
    {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true)
    }
    
    func camera()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: languageChangeString(a_str: "No Camera"),message: languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    @IBAction func addTags(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let tags = storyBoard.instantiateViewController(withIdentifier: "TagsSelection")
        self .present(tags, animated: true, completion: nil)
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        
        let view = UIAlertController(title: languageChangeString(a_str: "Pick Image"), message: "", preferredStyle: .actionSheet)
        let PhotoLibrary = UIAlertAction(title:  languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
            self.PhotoLibrary()
            view.dismiss(animated: true)
        })
        let camera = UIAlertAction(title: languageChangeString(a_str: "Take a new photo"), style: .default, handler: { action in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
            view.dismiss(animated: true)
        })
        
        let cancel = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
        })
        view.addAction(camera)
        view.addAction(PhotoLibrary)
        view.addAction(cancel)
        present(view, animated: true)
        
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func postBtnClicked()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let publicAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Public"), style: .default) { action -> Void in
            
            let tags = UserDefaults.standard.object(forKey: "bandIdsString")
            if self.pickedImage.size.width > 0{
            self.postTypeString = "1"
            var dataDict = Dictionary<String, Any>()
                dataDict = ["image": self.pickedImage, "caption":self.captionTV.text as Any, "audio":self.pickedAudio , "duration":self.duration!, "userId":self.userIdStr as Any , "tags":tags as Any ,"postType":self.postTypeString,"listeners" :""]
            
            let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            UserDefaults.standard.set("add", forKey: "StringType")
            home.addString = "fromAdd"
            home.sendPostData = dataDict
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = .white
            self.navigationController?.pushViewController(home, animated: false)
        }
            else{
                self.showToastForError(message: self.languageChangeString(a_str: "Post Image is required")!)
            }
        }
        
        let privateAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Private"), style: .default) { action -> Void in
            
            let tags = UserDefaults.standard.object(forKey: "bandIdsString") ?? ""
            if  self.pickedImage.size.width > 0{
                var dataDict = Dictionary<String, Any>()
                self.postTypeString = "2"
                dataDict = ["image": self.pickedImage, "caption":self.captionTV.text as Any, "audio":self.pickedAudio , "duration":self.duration!, "userId":self.userIdStr as Any , "tags":tags as Any,"postType":self.postTypeString]
                let data = NSKeyedArchiver.archivedData(withRootObject: dataDict)
                let userDefaults = UserDefaults.standard
                userDefaults.set(data, forKey:"postData")
                
                let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListenersSelection") as! ListenersSelection
                self.present(listeners, animated: true, completion: nil)
            }
            else{
                self.showToastForError(message: self.languageChangeString(a_str: "Post Image is required")!)
            }
        }
        
        let directAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Direct"), style: .default) { action -> Void in
            
            let tags = UserDefaults.standard.object(forKey: "bandIdsString") ?? ""
            if  self.pickedImage.size.width > 0{
                var dataDict = Dictionary<String, Any>()
                self.postTypeString = "3"
                dataDict = ["image": self.pickedImage, "caption":self.captionTV.text as Any, "audio":self.pickedAudio , "duration":self.duration!, "userId":self.userIdStr as Any , "tags":tags as Any,"postType":self.postTypeString]
                let data = NSKeyedArchiver.archivedData(withRootObject: dataDict)
                let userDefaults = UserDefaults.standard
                userDefaults.set(data, forKey:"postData")
                
                let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListenersSelection") as! ListenersSelection
                self.present(listeners, animated: true, completion: nil)
            }
            else{
                self.showToastForError(message: self.languageChangeString(a_str: "Post Image is required")!)
            }
        }
        
        let groupAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Group"), style: .default) { action -> Void in
            
//            let tags = UserDefaults.standard.object(forKey: "bandIdsString") ?? ""
//            if  self.pickedImage.size.width > 0{
//                var dataDict = Dictionary<String, Any>()
//                self.postTypeString = "2"
//                dataDict = ["image": self.pickedImage, "caption":self.captionTV.text as Any, "audio":self.pickedAudio , "duration":self.duration!, "userId":self.userIdStr as Any , "tags":tags as Any,"postType":self.postTypeString]
//                let data = NSKeyedArchiver.archivedData(withRootObject: dataDict)
//                let userDefaults = UserDefaults.standard
//                userDefaults.set(data, forKey:"postData")
//
//                let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListenersSelection") as! ListenersSelection
//                self.present(listeners, animated: true, completion: nil)
//            }
//            else{
//                self.showToastForError(message: self.languageChangeString(a_str: "Post Image is required")!)
//            }
        } 
        
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(publicAction)
        actionSheetController.addAction(privateAction)
        actionSheetController.addAction(directAction)
        actionSheetController.addAction(groupAction)
        actionSheetController.addAction(cancelAction)
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
    return true
    }
}

extension AddCaption: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //let pickedImage2 : UIImage!
        
        if let img = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            pickedImage = img
        }
        else if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            pickedImage = img
        }
        
        picker.dismiss(animated: true, completion: nil)
        // let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        // print(pickedImage ?? "")
        
        captionImage.image = pickedImage
        pickerImage = pickedImage
        
    }
}
