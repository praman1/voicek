//
//  SocialAccounts.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class SocialAccounts: UIViewController {
    
    @IBOutlet var addPhotoBtn: UIButton!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var createAccountBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    var tokenString : String!
    
    var fbIdStr : String!
    var fbEmailIdStr : String!
    var fbNameStr : String!
    var userNameStr : String!
    var imageUrlStr : String?
    var loginTypeCheckStr : String!

    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    
    var pickerImage  = UIImage()
    //var pickerImage1  = UIImage()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.height/2
        self.navigationItem.title = languageChangeString(a_str: "Create Account")
        self.addPhotoBtn.setTitle(languageChangeString(a_str: "Add a photo"), for: UIControlState.normal)
        self.fullNameLabel.text = languageChangeString(a_str: "Full Name")
        self.userNameLabel.text = languageChangeString(a_str: "User Name")
        self.createAccountBtn.setTitle(languageChangeString(a_str: "Create account"), for: UIControlState.normal)
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        fullNameTF.text = fbNameStr
        
        let colors = [(UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)), (UIColor(red: 0.0 / 255.0, green: 223.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0))]
        self.navigationController?.navigationBar.gradeintBarTintColor2(with: colors)
        
//        let url = NSURL(string: imageUrlStr ?? "")
//        self.profileImage.image = UIImage(data: NSData(contentsOf: url ?? "" as URL)! as Data)
//        pickerImage = self.profileImage.image!
        
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        
        let view = UIAlertController(title: languageChangeString(a_str: "Pick Image"), message: "", preferredStyle: .actionSheet)
        let PhotoLibrary = UIAlertAction(title:  languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
            self.PhotoLibrary()
            view.dismiss(animated: true)
        })
        let camera = UIAlertAction(title: languageChangeString(a_str: "Take a new photo"), style: .default, handler: { action in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
            view.dismiss(animated: true)
        })
        
        let cancel = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
        })
        view.addAction(camera)
        view.addAction(PhotoLibrary)
        view.addAction(cancel)
        present(view, animated: true)
    }
    
    func PhotoLibrary()
    {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true)
    }
    
    func camera()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: languageChangeString(a_str: "No Camera"),message: languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    @IBAction func createAccount(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork(){
            
            facebookLoginServiceCall()
            
        }else
        {
            showToastForAlert (message: languageChangeString(a_str: "You must Connect Internet!")!)
        }
        
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
   
    //MARK:SOCIAL LOGIN SERVICE CALL
    func facebookLoginServiceCall()
    {
        // http://volive.in/voicek/api/services/social
        Services.sharedInstance.loader(view: self.view)
        let parameters: Dictionary<String, Any>
        tokenString = UserDefaults.standard.object(forKey: "deviceToken") as? String
        
       
        if ((tokenString?.count) != nil)  {
            if loginTypeCheckStr == "fb"{
                parameters = [ "API-KEY" :APIKEY, "email":fbEmailIdStr!, "first_name" : fbNameStr! , "username" : userNameTF.text!, "device":"iOS", "token" :  tokenString! ,"oauth_provider" :  "facebook", "oauth_uid" :  fbIdStr!]
            }else{
                parameters = [ "API-KEY" :APIKEY, "email":fbEmailIdStr!, "first_name" : fbNameStr! , "username" : userNameTF.text!, "device":"iOS", "token" :  tokenString! ,"oauth_provider" :  "twitter", "oauth_uid" :  fbIdStr!]
            }
        }
        else
        {
            if loginTypeCheckStr == "fb"{
                parameters = [ "API-KEY" :APIKEY, "email":fbEmailIdStr!, "first_name" : fbNameStr! , "username" : userNameStr!, "device":"iOS", "token" :  "fkdjh72364gfdgd72346ndri" ,"oauth_provider" :  "facebook", "oauth_uid" :  fbIdStr!]
            }else{
                parameters = [ "API-KEY" :APIKEY, "email":fbEmailIdStr!, "first_name" : fbNameStr! , "username" : userNameStr!, "device":"iOS", "token" :  "fkdjh72364gfdgd72346ndri" ,"oauth_provider" :  "twitter", "oauth_uid" :  fbIdStr!]
            }
        }
        print(parameters)
        
        if pickerImage.size.width != 0 {
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)social")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print("Facebook Login Data Is :\(response.result.value ?? "")")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                                
                                let userId = responseData1["user_id"] as! String
                                let firstName = responseData1["first_name"] as! String
                                let username = responseData1["username"] as! String
                                let email = responseData1["email"] as! String
                                let image = responseData1["image"] as! String
                                let bio = responseData1["bio"] as! String
                                let mobile = responseData1["mobile"] as! String
                                let publicOrPrivate = responseData1["is_public"] as! String
                                let new_comments = responseData1["new_comments"] as! String
                                let new_mentions = responseData1["new_mentions"] as! String
                                let new_listeners = responseData1["new_listeners"] as! String
                                let new_likes = responseData1["new_likes"] as! String
                                let social_type = responseData1["social_type"] as! String
                                let imageStr = image
                                
                                print(email)
                                
                                UserDefaults.standard.set(userId, forKey: "userid")
                                //UserDefaults.standard.set(email, forKey: "email")
                                UserDefaults.standard.set(firstName, forKey: "first_name")
                                UserDefaults.standard.set(username, forKey: "userName")
                                UserDefaults.standard.set(mobile, forKey: "mobile")
                                UserDefaults.standard.set(imageStr, forKey: "image")
                                UserDefaults.standard.set(publicOrPrivate, forKey: "is_public")
                                UserDefaults.standard.set(bio, forKey: "bio")
                                UserDefaults.standard.set(new_comments, forKey: "newComments")
                                UserDefaults.standard.set(new_mentions, forKey: "newMentions")
                                UserDefaults.standard.set(new_listeners, forKey: "newListeners")
                                UserDefaults.standard.set(new_likes, forKey: "newLikes")
                                UserDefaults.standard.set(social_type, forKey: "socialType")
                                
                                let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                 UserDefaults.standard.set(true, forKey: "Loggedin")
                                self.navigationController?.pushViewController(homeVC, animated: true)
                                
                            }
                        }
                        else
                        {
                            self.showToastForError(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
            
        }else{
            pickerImage = UIImage.init(named: "DefaultUser")!
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)social")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print("Facebook Login Data Is :\(response.result.value ?? "")")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                                
                                let userId = responseData1["user_id"] as! String
                                let firstName = responseData1["first_name"] as! String
                                let username = responseData1["username"] as! String
                                let email = responseData1["email"] as! String
                                let image = responseData1["image"] as! String
                                let bio = responseData1["bio"] as! String
                                let mobile = responseData1["mobile"] as! String
                                let publicOrPrivate = responseData1["is_public"] as! String
                                let new_comments = responseData1["new_comments"] as! String
                                let new_mentions = responseData1["new_mentions"] as! String
                                let new_listeners = responseData1["new_listeners"] as! String
                                let new_likes = responseData1["new_likes"] as! String
                                let social_type = responseData1["social_type"] as! String
                                let imageStr = image
                                print(email)
                                
                                UserDefaults.standard.set(userId, forKey: "userid")
                                //UserDefaults.standard.set(email, forKey: "email")
                                UserDefaults.standard.set(firstName, forKey: "first_name")
                                UserDefaults.standard.set(username, forKey: "userName")
                                UserDefaults.standard.set(mobile, forKey: "mobile")
                                UserDefaults.standard.set(imageStr, forKey: "image")
                                UserDefaults.standard.set(publicOrPrivate, forKey: "is_public")
                                UserDefaults.standard.set(bio, forKey: "bio")
                                UserDefaults.standard.set(new_comments, forKey: "newComments")
                                UserDefaults.standard.set(new_mentions, forKey: "newMentions")
                                UserDefaults.standard.set(new_listeners, forKey: "newListeners")
                                UserDefaults.standard.set(new_likes, forKey: "newLikes")
                                UserDefaults.standard.set(social_type, forKey: "socialType")
                                
                                let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                 UserDefaults.standard.set(true, forKey: "Loggedin")
                                self.navigationController?.pushViewController(homeVC, animated: true)
                            }
                        }
                        else
                        {
                            self.showToastForError(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
}

extension SocialAccounts: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        print(pickedImage2 ?? "")
        profileImage.image = pickedImage2  as? UIImage
        pickerImage = (pickedImage2 as? UIImage)!
    }
}

extension UINavigationBar{
    
    func gradeintBarTintColor2(with gradient:[UIColor]){
        var frameAndStatusBarSize = self.bounds
        frameAndStatusBarSize.size.height +=  20
        setBackgroundImage(UINavigationBar.gradient2(size: frameAndStatusBarSize.size, color: gradient), for: .default)
    }
    
    static func gradient2(size:CGSize,color:[UIColor]) -> UIImage?{
        //turn color into cgcolor
        let colors = color.map{$0.cgColor}
        
        //begin graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        // From now on, the context gets ended if any return happens
        defer {UIGraphicsEndImageContext()}
        
        //create core graphics context
        let locations:[CGFloat] = [0.0,1.0]
        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
            return nil
        }
        //draw the gradient
        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
