//
//  LinkedAccounts.swift
//  Voicek
//
//  Created by Suman Guntuka on 20/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class LinkedAccounts: UIViewController {

    @IBOutlet var facebookLabel: UILabel!
    @IBOutlet var twitterLabel: UILabel!
    @IBOutlet var setEmailAndPasswordLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = languageChangeString(a_str: "Linked Accounts")
        self.facebookLabel.text = languageChangeString(a_str: "Facebook")
        self.twitterLabel.text = languageChangeString(a_str: "Twitter")
        self.setEmailAndPasswordLabel.text = languageChangeString(a_str: "Set Email and Password")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func setEmailPw(_ sender: Any) {
        
        let setpw = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetPassword") as! SetPassword
        self.navigationController?.pushViewController(setpw, animated: true)
    }
}
