//
//  ForgotPasswordVC.swift
//  Voicek
//
//  Created by volivesolutions on 19/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var typeYourEmailLbl: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var resetBtn: UIButton!
    var tokenString : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(textFieldDidEndEditing(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        self.navigationItem.title = languageChangeString(a_str: "Forgot Password")
        self.typeYourEmailLbl.text = languageChangeString(a_str: "Type your email")
        self.resetBtn.setTitle(languageChangeString(a_str: "Reset Password"), for: UIControlState.normal)
        
        let button1 = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) 
        button1.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationItem.leftBarButtonItem  = button1
        
        let colors = [(UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)), (UIColor(red: 0.0 / 255.0, green: 223.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0))]
        self.navigationController?.navigationBar.gradeintBarTintColor1(with: colors)
      
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetBtn_Action(_ sender: Any) {
        
        if !((emailTF.text?.isEmpty)!) {
            forgotPasswordServiceCall()
        }
        else{
            
            print("Textfield Is Empty")
            self.showToast(message: languageChangeString(a_str: "Please Enter Email")!)
            //showToastForAlert (message: "Please Enter Email")
        }
    }
    
    //MARK:FORGOT PASSWORD SERVICE CALL
    func forgotPasswordServiceCall()
    {
       // http://volive.in/voicek/api/services/forgot?API-KEY=225143&email=testcase123@yopmail.com
        Services.sharedInstance.loader(view: self.view)
        let forgotPassword = "\(Base_Url)forgot?"
        
        tokenString = UserDefaults.standard.object(forKey: "deviceToken") as? String
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "email":self.emailTF.text!]
        
        print(parameters)
        
        Alamofire.request(forgotPassword, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                   // self.showToastForAlert(message: message)
                    let alert = UIAlertController(title: self.languageChangeString(a_str: "Done!"), message: message, preferredStyle: .alert)
                    let okButton = UIAlertAction(title: self.languageChangeString(a_str: "OK"), style: .default, handler: { action in
                    let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(login, animated: true)
                    })
                    alert.addAction(okButton)
                    self.present(alert, animated: true)
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
}

extension ForgotPasswordVC : UITextFieldDelegate{
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
            emailTF.resignFirstResponder()
            return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField){
        emailTF.resignFirstResponder()
    }
}

extension UINavigationBar{
    
    func gradeintBarTintColor1(with gradient:[UIColor]){
        var frameAndStatusBarSize = self.bounds
        frameAndStatusBarSize.size.height +=  20
        setBackgroundImage(UINavigationBar.gradient1(size: frameAndStatusBarSize.size, color: gradient), for: .default)
    }
    
    static func gradient1(size:CGSize,color:[UIColor]) -> UIImage?{
        //turn color into cgcolor
        let colors = color.map{$0.cgColor}
        
        //begin graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        // From now on, the context gets ended if any return happens
        defer {UIGraphicsEndImageContext()}
        
        //create core graphics context
        let locations:[CGFloat] = [0.0,1.0]
        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
            return nil
        }
        //draw the gradient
        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
