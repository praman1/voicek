//
//  Listening.swift
//  Voicek
//
//  Created by Suman Guntuka on 24/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class Listening: UIViewController {

    @IBOutlet weak var listeningTable: UITableView!
   
    @IBOutlet weak var headPhones: UIImageView!
    @IBOutlet weak var notListeningLabel: UILabel!
    
    var user_idArr = [String]()
    var follower_idArr = [String]()
    var created_atArr = [String]()
    var nameArr = [String]()
    var user_nameArr = [String]()
    var is_publicArr = [String]()
    var imageArr = [String]()
    var requestIdArray = [String]()
    var listeningData : [[String : AnyObject]]!
    
    var postUserId : String!
    var ownerUserId : String!
    var checkStr : String!
    var postUserIdStr : String!
    //var testString : String! = ""

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.navigationItem.title = languageChangeString(a_str: "Listening")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        getListeningServiceCall()
        
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    //MARK:GET LISTENING LIST SERVICE CALL
    func getListeningServiceCall()
    {
        //http://volive.in/voicek/api/services/get_listening?API-KEY=225143&user_id=4182668577&profile_user_id=3783858425
        Services.sharedInstance.loader(view: self.view)
        let get_listening = "\(Base_Url)get_listening?"
        let parameters: Dictionary<String, Any>
        
        if checkStr == "fromSide" {
            parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : ownerUserId!]
        }
        else{
            parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : postUserId!]
        }
        
        print(parameters)
        
        Alamofire.request(get_listening, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    self.user_idArr = [String]()
                    self.follower_idArr = [String]()
                    self.created_atArr = [String]()
                    self.nameArr = [String]()
                    self.user_nameArr = [String]()
                    self.is_publicArr = [String]()
                    self.imageArr = [String]()
                    self.requestIdArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.listeningData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.listeningData
                    {
                        let userId = each["user_id"] as? String
                        let follower_id = each["follower_id"] as? String
                        let created_at = each["created_at"] as? String
                        let name = each["name"] as? String
                        let user_name = each["user_name"] as? String
                        let is_public = each["is_public"] as? String
                        let image = each["image"] as? String
                        
                        let x1 : Int = each["request"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let request : String = xNSNumber1.stringValue
                        
                        self.user_idArr.append(userId!)
                        self.follower_idArr.append(follower_id!)
                        self.created_atArr.append(created_at!)
                        self.nameArr.append(name!)
                        self.user_nameArr.append(user_name!)
                        self.is_publicArr.append(is_public!)
                        self.imageArr.append(image!)
                        self.requestIdArray.append(request)
                    }
                    DispatchQueue.main.async {
                        self.listeningTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //let message = responseData["message"] as! String
                        //self.showToastForError(message: message)
                        self.listeningTable.isHidden = true
                        self.headPhones.isHidden = false
                        self.notListeningLabel.isHidden = false
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @objc func followClicked(sender : UIButton){
        let buttonRow = sender.tag
        postUserIdStr = user_idArr[buttonRow]
        followOrUnfollowServiceCall()
    }
    
    //MARK:FOLLOW OR UNFOLLOW SERVICE CALL
    func followOrUnfollowServiceCall()
    {
        // http://volive.in/voicek/api/services/follower?API-KEY=225143&user_id=3783858425&follower_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)follower?"
        print("Report Post URL Is :\(deleteUserPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "follower_id" : postUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    
                    let x1 : Int = responseData["followed"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let followed : String = xNSNumber1.stringValue
                    
                    UserDefaults.standard.set(followed, forKey: "follow")
                    self.getListeningServiceCall()
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension Listening: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : BlockedCell
        
        cell = listeningTable.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! BlockedCell
        cell.listenBtn.layer.cornerRadius = cell.listenBtn.frame.size.height/2
        cell.frndImg.layer.cornerRadius = cell.frndImg.frame.size.height/2
        cell.frndLbl.text = nameArr[indexPath.row]
        cell.frndEmailLbl.text = String(format: "@%@", user_nameArr[indexPath.row])
        cell.frndImg.sd_setImage(with: URL (string: imageArr[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
        cell.listenBtn.tag = indexPath.row
        cell.listenBtn.addTarget(self, action: #selector(followClicked(sender:)), for: UIControlEvents.touchUpInside)
        if requestIdArray[indexPath.row] == "1" {
            cell.listenBtn.setImage(UIImage.init(named: "Following"), for: UIControlState.normal)
        }else if requestIdArray[indexPath.row] == "0"{
            cell.listenBtn.setImage(UIImage.init(named: "Follow"), for: UIControlState.normal)
        }else if requestIdArray[indexPath.row] == "2"{
            cell.listenBtn.setImage(UIImage.init(named: "Request"), for: UIControlState.normal)
        }
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = user_idArr[indexPath.row]
        profile.fromSearchStr = "search"
        self.navigationController?.pushViewController(profile, animated: false)
    }
}
