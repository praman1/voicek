//
//  FollowersSelection.swift
//  Voicek
//
//  Created by volivesolutions on 27/11/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ListenersSelection: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var postBtn: UIButton!
    
    @IBOutlet var addListenersTableView: UITableView!
    var ownerUserId : String! = ""
    
    var listenersCheckArray = [String]()
    var IdString : String!
    var idArray = [String]()
    var checkArr = [String]()
    var saveBool : Bool!
    var user_idArr = [String]()
    var follower_idArr = [String]()
   // var created_atArr = [String]()
    var nameArr = [String]()
    var postTypeString : String! = ""
   // var user_nameArr = [String]()
    //var is_publicArr = [String]()
   // var imageArr = [String]()
    //var requestIdArray = [String]()
    
    var listeningData : [[String : AnyObject]]!
    var postDict = Dictionary<String, Any>()
   // int selectedIndex
    var selectedIndex : Int!
    var selectedIndexValues = [Int]()
   // int selectedIndex;
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.ownerUserId = UserDefaults.standard.object(forKey: "userid") as? String
        let data = UserDefaults.standard.object(forKey: "postData") as! NSData
        let postObject = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! NSDictionary
        self.postDict = postObject as! [String : Any]
        print("Post Dict Is:\(postDict)")
        self.titleLabel.text = languageChangeString(a_str: "Listeners")
        self.postBtn.setTitle(languageChangeString(a_str: "Post"), for: UIControlState.normal)
        UserDefaults.standard.removeObject(forKey: "postData")
        let postType = self.postDict["postType"] as? String
        self.postTypeString = postType
        self.getListenersServiceCall()
        

        // Do any additional setup after loading the view.
    }

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func post(_ sender: Any) {
//        if saveBool == true
//        {
            self.postDict.updateValue(IdString ?? "", forKey: "listeners")
            print("Updated Post Dict Is:\(postDict)")
            
            let data = NSKeyedArchiver.archivedData(withRootObject: self.postDict)
            let userDefaults = UserDefaults.standard
            userDefaults.set(data, forKey:"postDataFinal")
            UserDefaults.standard.set("listen", forKey: "postType")
            
            let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC1")
            self.present(homeVC, animated: false, completion: nil)
            UserDefaults.standard.set("filt", forKey: "savebS")
        //}
       // self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:GET LISTENERS SERVICE CALL
    func getListenersServiceCall()
    {
        //http://volive.in/voicek/api/services/get_listeners?API-KEY=225143&user_id=3783858425&profile_user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let get_listeners = "\(Base_Url)get_listeners?"
        let parameters: Dictionary<String, Any>
        
        //if checkStr == "fromSide" {
            parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : ownerUserId!]
       // }
        //else{
           // parameters = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : postUserId!]
        //}
        
        print(parameters)
        
        Alamofire.request(get_listeners, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                
                if status == 1
                {
                    self.user_idArr = [String]()
                    self.follower_idArr = [String]()
                    self.nameArr = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    self.listeningData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.listeningData
                    {
                        let userId = each["user_id"] as? String
                        let follower_id = each["follower_id"] as? String
                        let name = each["name"] as? String
 
                        self.user_idArr.append(userId!)
                        self.follower_idArr.append(follower_id!)
                        self.nameArr.append(name!)
                        
                        let myInt2 = (follower_id! as NSString).integerValue
                        
                        let myString = String(myInt2)
                        
                        if UserDefaults.standard.object(forKey: "savebS") != nil
                        {
                            self.saveBool = true
                            let idArr = UserDefaults.standard.object(forKey: "IdsStringArr") as! Array<Any>
                            
                            self.checkArr = idArr as! [String]
                            
                            print(self.checkArr)
                            
                            if self.checkArr .contains(myString)
                            {
                                let checkStr = "1"
                                self.listenersCheckArray.append(checkStr)
                                
                            }else
                            {
                                let checkStr = "0"
                                self.listenersCheckArray.append(checkStr)
                            }
                        }
                        else
                        {
                            let checkStr = "0"
                            self.listenersCheckArray.append(checkStr)
                        }
                    }
                    DispatchQueue.main.async {
                        self.addListenersTableView.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension ListenersSelection: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var  cell : AddListenersCell
        cell = addListenersTableView.dequeueReusableCell(withIdentifier: "AddListenersCell", for: indexPath) as! AddListenersCell
        cell.listenersNameLabel.text = nameArr[indexPath.row]
            let followerIdStr = self.follower_idArr[indexPath.row]
            if listenersCheckArray[indexPath.row] == "0" {
                cell.checkImageView.image = UIImage(named: "UnCheckBox")
            }else if listenersCheckArray[indexPath.row] == "1"{
                cell.checkImageView.image = UIImage(named: "CheckBoxGreen")
                idArray.append(followerIdStr)
                
                print("Id Array Is :\(idArray)")
                
                IdString = idArray.joined(separator: ",")
                print(IdString)
                UserDefaults.standard.set( IdString , forKey: "followerIdString") //setObject
                UserDefaults.standard.set( idArray , forKey: "IdsStringArr")
                
            }else{
                
            }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 70
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.postTypeString == "3"{
            idArray = [String]()
            let listenersCell = self.addListenersTableView.cellForRow(at: indexPath) as! AddListenersCell
            listenersCell.checkImageView.image = UIImage(named: "CheckBoxGreen")
            IdString = self.follower_idArr[indexPath.row]
           // idArray.append(followerIdStr)
            
           // print("Id Array Is :\(idArray)")
            
           // IdString = idArray.joined(separator: ",")
            print(IdString)
            UserDefaults.standard.set( IdString , forKey: "followerIdString") //setObject
           // UserDefaults.standard.set( idArray , forKey: "IdsStringArr")
        }else{
            saveBool = true
          /*
            tableView.deselectRow(at: indexPath, animated: true)
            
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark{
                    cell.accessoryType = .none
                    self.idArray.remove(at: self.idArray.index(of:self.follower_idArr[indexPath.row])!)
                    //self.idArray.remove(at: indexPath.row)
                    print("Remove Id Array Is :\(idArray)")
                    IdString = idArray.joined(separator: ",")
                    print("Removed::::\(IdString)")
                    self.selectedIndexValues.remove(at: self.idArray.index(of:self.follower_idArr[indexPath.row])!)
                   // self.selectedIndexValues.remove(at: indexPath.row)
                    print("Removed Index Values:::\(self.selectedIndexValues)")
                }
                else{
                    cell.accessoryType = .checkmark
                    let followerIdStr = self.follower_idArr[indexPath.row]
                    idArray.append(followerIdStr)
                    print("Add Id Array Is :\(idArray)")
                    IdString = idArray.joined(separator: ",")
                    print("Added::::\(IdString)")
                    UserDefaults.standard.set( IdString , forKey: "followerIdString") //setObject
                    UserDefaults.standard.set( idArray , forKey: "IdsStringArr")
                    self.selectedIndexValues.append(indexPath.row)
                    print("Selected Index Values:::\(self.selectedIndexValues)")
                }
            }
            */
            
            if listenersCheckArray[indexPath.row] == "0" {
                
                listenersCheckArray.remove(at: indexPath.row)
                listenersCheckArray.insert("1", at: indexPath.row)
                
                print(listenersCheckArray)
                
                IdString = ""
                idArray = [String]()
                
                addListenersTableView.reloadData()
            }
            else if listenersCheckArray[indexPath.row] == "1"{
                //UserDefaults.standard.set("filt", forKey: "savebS")
                //  UserDefaults.standard.removeObject(forKey: "savebS")
                listenersCheckArray.remove(at: indexPath.row)
                listenersCheckArray.insert("0", at: indexPath.row)
                print(listenersCheckArray)
                IdString = ""
                idArray = [String]()
                if !(self.listenersCheckArray.contains("1")){
                    print("There are no items added")
                    saveBool = false
                    UserDefaults.standard.removeObject(forKey: "savebS")
                }
                addListenersTableView.reloadData()
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        if self.postTypeString == "3"{
            let listenersCell = self.addListenersTableView.cellForRow(at: indexPath) as! AddListenersCell
            listenersCell.checkImageView.image = UIImage(named: "UnCheckBox")
            
        }else{
            
        }
        
        }
    }

