//
//  Comments.swift
//  Voicek
//
//  Created by Prashanth on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class Comments: UIViewController {
    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet var viewEarlierCommensLabel: UILabel!
    
    var postIdStr : String!
    var userIdStr : String!
    var ownerUserIdStr : String!
    
    var commentsListArr : [[String : AnyObject]]!
    
    var user_idArr = [String]()
    var commentsArr = [String]()
    var comment_idArr = [String]()
    var time_agoArr = [String]()
    var imageArr = [String]()
    var user_nameArr = [String]()
   
    var combineArr = [Any]()
    
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var commentTextView: UITextView!
    @IBOutlet var hintLbl: UILabel!
    var fromAppStr : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = languageChangeString(a_str: "Comments")
        self.viewEarlierCommensLabel.text = languageChangeString(a_str: "View earlier comments")
        self.hintLbl.text = languageChangeString(a_str: "Add a comment")
        ownerUserIdStr = UserDefaults.standard.object(forKey: "userid") as? String
        postImage.layer.cornerRadius = postImage.frame.size.height/2
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        getAllCommentsServiceCall()
        NotificationCenter.default.addObserver(self, selector: #selector(commentsReload), name: Notification.Name("commentsReload"), object: nil)
        
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
//        self.view.addGestureRecognizer(tap)
        
    }
    @objc func hideKeyboard()
    {
        self.commentTextView.resignFirstResponder()
        
    }
    
   @objc func commentsReload()
    {
        self.viewDidLoad()
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:GET ALL COMMENTS SERVICE CALL
    func getAllCommentsServiceCall()
    {
        // http://volive.in/voicek/api/services/get_comments?API-KEY=225143&user_id=3783858425&post_id=1
        
        Services.sharedInstance.loader(view: self.view)
        
        let postDetails = "\(Base_Url)get_comments?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id" : ownerUserIdStr! , "post_id":postIdStr!]
        
        print(parameters)
        
        Alamofire.request(postDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let responseData = response.result.value as? Dictionary<String, Any>
            print(responseData!)
            
            let status = responseData!["status"] as! Int
            // let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                    
                    let userId = responseData1["user_id"] as! String
                    let image = responseData1["image"] as! String
                    let description = responseData1["description"] as! String
                    let name = responseData1["name"] as! String
                    let is_public = responseData1["is_public"] as! String
                    
                    self.postImage.sd_setImage(with: URL (string: image), placeholderImage: UIImage(named: "Logo"))
                    self.nameLbl.text = name
                    self.descLbl.text = description
                    
                    print(userId)
                    print(is_public)
                    
                    self.commentsListArr = responseData1["comments_list"] as? [[String:AnyObject]]
                    
                    self.user_idArr = [String]()
                    self.commentsArr = [String]()
                    self.comment_idArr = [String]()
                    self.time_agoArr = [String]()
                    self.imageArr = [String]()
                    self.user_nameArr = [String]()
                    self.combineArr = [Any]()
                    
                    print(self.commentsListArr)
                    let count = self.commentsListArr.count
                    print("Array Count Is: \(count)")
                    
                    for each in self.commentsListArr
                    {
                        let user_id = each["user_id"] as? String
                        let comment = each["comment"] as? String
                        let comment_id = each["comment_id"] as? String
                        let time_ago = each["time_ago"] as? String
                        let image = each["image"] as? String
                        let user_name = each["user_name"] as? String
                        
                        let twoWords = String(format: "@%@ %@", user_name!,comment!)
                        var components = twoWords.components(separatedBy: " ")
                        let blueRange: NSRange = (twoWords as NSString).range(of: components[0])
                        let blackRange: NSRange = (twoWords as NSString).range(of: components[1])
                        
                        let attrString = NSMutableAttributedString(string: twoWords)
                        
                        attrString.beginEditing()
                        attrString.addAttribute(.foregroundColor, value: UIColor.init(red: 11.0/255.0, green: 235.0/255.0, blue: 243.0/255.0, alpha: 1.0), range: blueRange)
                        attrString.addAttribute(.foregroundColor, value: UIColor.black, range: blackRange)
                        attrString.endEditing()
                        
                        self.combineArr.append(attrString)
                        self.user_idArr.append(user_id!)
                        self.commentsArr.append(comment!)
                        self.comment_idArr.append(comment_id!)
                        self.time_agoArr.append(time_ago!)
                        self.imageArr.append(image!)
                        self.user_nameArr.append(user_name!)
                    }
                    DispatchQueue.main.async {
                        self.commentsTable.reloadData()
                    }
                }
            }
            else
            {
                let message = responseData!["message"] as! String
                self.showToastForError(message: message)
                //self.showToastForAlert(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    //MARK:ADD COMMENT SERVICE CALL
    func addCommentServiceCall()
    {
        // http://volive.in/voicek/api/services/comment
//        API-KEY:225143
//        user_id : 513987992
//        comment : test
//        post_id : 5
        Services.sharedInstance.loader(view: self.view)
        
        let signin = "\(Base_Url)comment"
       
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id" : ownerUserIdStr! ,"comment" : commentTextView.text! , "post_id":postIdStr!]
    
        print(parameters)
        
        Alamofire.request(signin, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            let responseData = response.result.value as? Dictionary<String, Any>
            
            let status = responseData!["status"] as! Int
            let message = responseData!["message"] as! String
            
            if status == 1
            {
                
                Services.sharedInstance.dissMissLoader()
                self.commentTextView.text = ""
                self.hintLbl.isHidden = false
                self.showToastForAlert(message: message)
                self.viewDidLoad()
              
            }
            else
            {
                self.showToastForError(message: message)
                //self.showToastForAlert(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    @IBAction func addPost(_ sender: Any) {
        if commentTextView.text.count > 0 {
            addCommentServiceCall()
        }
        else{
            self.showToastForError(message: languageChangeString(a_str: "Please enter comment to post")!)
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.commentTextView.resignFirstResponder()
        hintLbl.isHidden = false
    }
}

extension Comments: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return comment_idArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : BlockedCell
        
        cell = commentsTable.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! BlockedCell
        cell.commentImage.layer.cornerRadius = cell.commentImage.frame.size.height/2
        cell.commentImage.sd_setImage(with: URL (string: imageArr[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
        cell.userNameLbl.textColor = UIColor.init(red: 11.0/255.0, green: 235.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        cell.userNameLbl.text = String(format: "@%@", user_nameArr[indexPath.row])
        cell.commentLbl.text = commentsArr[indexPath.row]
        cell.timeAgoLbl.text = time_agoArr[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = user_idArr[indexPath.row]
        profile.checkString = "toProfile"
        self.navigationController?.pushViewController(profile, animated: false)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            let postUserId = user_idArr[indexPath.row]
            
            if ownerUserIdStr == postUserId
            {
                let commentId = comment_idArr[indexPath.row]
                UserDefaults.standard.set(postIdStr, forKey: "postId")
                UserDefaults.standard.set(commentId, forKey: "commentId")
                UserDefaults.standard.set(ownerUserIdStr, forKey: "ownerUserIdStr")
                
                let delete = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeleteComment")
                self .present(delete, animated: true, completion: nil)
            }
            else{
                self.showToastForError(message: languageChangeString(a_str: "You're not owner of this comment so,you're not able to delete this comment")!)
            }
//            self.catNames.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

extension Comments: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        hintLbl.isHidden = true
    }
}
