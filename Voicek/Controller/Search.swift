//
//  Search.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class Search: UIViewController {
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var tagsTable: UITableView!
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var people: UIButton!
    @IBOutlet weak var tags: UIButton!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var likeView: UIView!
    
    @IBOutlet weak var mySearch: UISearchBar!
    
    var ownerUserIdStr : String!
    
    var userIdArray = [String]()
    var usernameArray = [String]()
    var nameArray = [String]()
    var imageArray = [String]()
    var is_publicArray = [String]()
    var requestIdArray = [String]()
    
    var filteredData: [String]!
    var postUserIdStr : String! = ""
    
    var checkStr : String!
    
    var allData : [[String : AnyObject]]!
    
    var tagNamesArr = [String]()
    var tagIdArr = [String]()
    var postCountArr = [String]()
    
    var tagsDataArray = NSArray()
    var totalTagsArray = NSMutableArray()
    var tagsData : [[String : AnyObject]]!
    var tagsFiltersArray = [[String:AnyObject]]()
    
    var usersDataArray = NSArray()
    var totalUsersArray = NSMutableArray()
    var usersData : [[String : AnyObject]]!
    var usersFiltersArray = [[String:AnyObject]]()
    
    var isSearch : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tagsTable.isHidden = true
        searchTable.isHidden = false
        searchTable.reloadData()
        
        likeView.isHidden = true
        tags.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        self.navigationItem.title = languageChangeString(a_str: "Search")
        self.mySearch.placeholder = languageChangeString(a_str: "Search")
        self.people.setTitle(languageChangeString(a_str: "People"), for: UIControlState.normal)
        self.tags.setTitle(languageChangeString(a_str: "Tags"), for: UIControlState.normal)
        let backBtn = UIBarButtonItem(image: UIImage(named:"MenuWhite"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        //        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(viewTapped))
        //        self.view.addGestureRecognizer(tapGesture)
        
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideSearch))
        //        searchTable.addGestureRecognizer(tapGesture)
        usersListServiceCall()
        self.checkStr = "search"
        
        // Do any additional setup after loading the view.
    }
    //    @objc func viewTapped()
    //    {
    //        mySearch.resignFirstResponder()
    //    }
    
    @objc func backBtnClicked()
    {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    //    @objc func hideSearch()
    //    {
    //      mySearch.endEditing(true)
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func peopleBtn(_ sender: Any) {
        
        mySearch.resignFirstResponder()
        checkStr = "search"
        usersListServiceCall()
        //self.filteredData.removeAll()
        searchTable.isHidden = false
        tagsTable.isHidden = true
        likeView.isHidden = true
        activityView.isHidden = false
        tags.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        people.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
        img.image = #imageLiteral(resourceName: "FindFriendsGray")
        titleLbl.text = languageChangeString(a_str: "No Recent Searches")
        descLbl.text = languageChangeString(a_str: "Very sorry, but you haven't searched for anything yet :)")
        
    }
    
    @IBAction func tagsBtn(_ sender: Any) {
        mySearch.resignFirstResponder()
        checkStr = "tags"
        tagsServiceCall()
        tagsTable.isHidden = false
        searchTable.isHidden = true
        activityView.isHidden = true
        likeView.isHidden = false
        people.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        tags.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
        img.image = #imageLiteral(resourceName: "Search")
    }
    
    //MARK:- Users List ServiceCall
    func usersListServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/all_users?API-KEY=225143&user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)all_users?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" :ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    self.userIdArray = [String]()
                    self.usernameArray = [String]()
                    self.nameArray = [String]()
                    self.imageArray = [String]()
                    self.is_publicArray = [String]()
                    self.requestIdArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    let totalItems:NSArray = responseData["data"] as! NSArray
                    self.totalUsersArray.addObjects(from: totalItems as! [Any])
                    print("total data array",self.totalUsersArray)
                    self.usersDataArray = self.totalUsersArray
                    
                    self.allData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.allData
                    {
                        let user_id = each["user_id"] as? String
                        let username = each["username"] as? String
                        let name = each["name"] as? String
                        let image = each["image"] as? String
                        let is_public = each["is_public"] as? String
                        
                        let x1 : Int = each["request"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let request : String = xNSNumber1.stringValue
                        
                        self.userIdArray.append(user_id!)
                        self.usernameArray.append(username!)
                        self.nameArray.append(name!)
                        self.imageArray.append(image!)
                        self.is_publicArray.append(is_public!)
                        self.requestIdArray.append(request)
                    }
                   // self.filteredData = self.nameArray
                    DispatchQueue.main.async {
                        
                        if  self.allData.count > 0
                        {
                            Services.sharedInstance.dissMissLoader()
                        }
                        self.searchTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        //self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
    //MARK:- Tags ServiceCall
    func tagsServiceCall()
    {
        // http://volive.in/voicek/api/services/audio_tags?API-KEY=225143
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)audio_tags?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY]
        
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    
                    self.tagNamesArr = [String]()
                    self.tagIdArr = [String]()
                    self.postCountArr = [String]()
                    Services.sharedInstance.dissMissLoader()
                    
                    let totalItems:NSArray = responseData["data"] as! NSArray
                    self.totalTagsArray.addObjects(from: totalItems as! [Any])
                    print("total data array",self.totalTagsArray)
                    self.tagsDataArray = self.totalTagsArray
                    
                    self.allData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.allData
                    {
                        let tagId = each["id"] as? String
                        let tagName = each["name"] as? String
                        
                        let x : Int = each["posts_count"] as! Int
                        let xNSNumber = x as NSNumber
                        let postCount : String = xNSNumber.stringValue
                        
                        self.tagIdArr.append(tagId!)
                        self.tagNamesArr.append(tagName!)
                        self.postCountArr.append(postCount)
                    }
                    
                    DispatchQueue.main.async {
                        
                        if  self.allData.count > 0
                        {
                            Services.sharedInstance.dissMissLoader()
                        }
                        self.tagsTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        //self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
    
    @objc func followClicked(sender : UIButton){
        let buttonRow = sender.tag
        if isSearch == true{
            postUserIdStr = ((self.usersFiltersArray[buttonRow] as AnyObject).object(forKey: "user_id") as? String)
            followOrUnfollowServiceCall()
        }else{
            postUserIdStr = userIdArray[buttonRow]
            followOrUnfollowServiceCall()
        }
        
    }
    
    //MARK:- FollowOrUnfollow ServiceCall
    func followOrUnfollowServiceCall()
    {
        // http://volive.in/voicek/api/services/follower?API-KEY=225143&user_id=3783858425&follower_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)follower?"
        print("Report Post URL Is :\(deleteUserPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "follower_id" : postUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    
                    let x1 : Int = responseData["followed"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let followed : String = xNSNumber1.stringValue
                    
                    UserDefaults.standard.set(followed, forKey: "follow")
                    self.usersListServiceCall()
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension Search: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTable
        {
            if checkStr == "search"{
                if isSearch == true{
                    return usersFiltersArray.count
                }else{
                    return usernameArray.count
                }
            }
        }
        else{
            if checkStr == "tags"{
                if isSearch == true{
                    return tagsFiltersArray.count
                }else{
                    return tagNamesArr.count
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : BlockedCell!
        var cell1 : TagsCell!
        // code to check for a particular tableView to display the data .
        if tableView == searchTable{
            if isSearch == true{
                cell = searchTable.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath as IndexPath) as? BlockedCell
                cell.listenBtn.layer.cornerRadius = cell.listenBtn.frame.size.height/2
                cell.frndImg.layer.cornerRadius = cell.frndImg.frame.size.height/2
                //username
                cell.frndLbl.text = ((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String)
                cell.frndEmailLbl.text = "@" + (((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "username") as? String)!)
                cell.frndImg.sd_setImage(with: URL (string: (self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "image") as! String), placeholderImage: UIImage(named: "DefaultUser"))
                cell.listenBtn.tag = indexPath.row
                cell.listenBtn.addTarget(self, action: #selector(followClicked(sender:)), for: UIControlEvents.touchUpInside)
                
                if ((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "request")) as? String == "1" {
                    cell.listenBtn.setImage(UIImage.init(named: "Following"), for: UIControlState.normal)
                }else if ((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "request")) as? String == "2"{
                    cell.listenBtn.setImage(UIImage.init(named: "Follow"), for: UIControlState.normal)
                }else if ((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "request")) as? String == "3"{
                    cell.listenBtn.setImage(UIImage.init(named: "Request"), for: UIControlState.normal)
                }
                return cell
            }else{
                cell = searchTable.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath as IndexPath) as? BlockedCell
                cell.listenBtn.layer.cornerRadius = cell.listenBtn.frame.size.height/2
                cell.frndImg.layer.cornerRadius = cell.frndImg.frame.size.height/2
                cell.frndLbl.text = nameArray[indexPath.row]
                cell.frndEmailLbl.text = "@" + usernameArray[indexPath.row]
                cell.frndImg.sd_setImage(with: URL (string: imageArray[indexPath.row]), placeholderImage: UIImage(named: "DefaultUser"))
                cell.listenBtn.tag = indexPath.row
                cell.listenBtn.addTarget(self, action: #selector(followClicked(sender:)), for: UIControlEvents.touchUpInside)
                
                if requestIdArray[indexPath.row] == "1" {
                    cell.listenBtn.setImage(UIImage.init(named: "Following"), for: UIControlState.normal)
                }else if requestIdArray[indexPath.row] == "2"{
                    cell.listenBtn.setImage(UIImage.init(named: "Follow"), for: UIControlState.normal)
                }else if requestIdArray[indexPath.row] == "3"{
                    cell.listenBtn.setImage(UIImage.init(named: "Request"), for: UIControlState.normal)
                }
                
                return cell
            }
        }
        else if tableView == tagsTable{
            if isSearch == true{
                cell1 = tagsTable.dequeueReusableCell(withIdentifier: "TagsCell", for: indexPath as IndexPath) as? TagsCell
                cell1.textLabel?.textColor = UIColor.darkGray
                
                cell1.tagNameLabel.text = String(format: "%@%@", "#",((self.tagsFiltersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String)!)
                let x : Int = (self.tagsFiltersArray[indexPath.row] as AnyObject).object(forKey: "posts_count") as! Int
                let xNSNumber = x as NSNumber
                let postCount : String = xNSNumber.stringValue
                
                cell1.noOfPostsLabel.text = String(format: "%@ %@", postCount,languageChangeString(a_str: "Posts")!)
                
                return cell1
            }else{
                cell1 = tagsTable.dequeueReusableCell(withIdentifier: "TagsCell", for: indexPath as IndexPath) as? TagsCell
                cell1.textLabel?.textColor = UIColor.darkGray
                cell1.tagNameLabel.text = String(format: "%@%@", "#",tagNamesArr[indexPath.row])
                cell1.noOfPostsLabel.text = String(format: "%@ %@", postCountArr[indexPath.row],languageChangeString(a_str: "Posts")!)
                
                return cell1
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == searchTable {
            return 70
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == searchTable {
            if isSearch == true{//141842597
                let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
                profile.postUserIdStr = ((self.usersFiltersArray[indexPath.row] as AnyObject).object(forKey: "user_id") as? String)
                profile.fromSearchStr = "search"
                self.navigationController?.pushViewController(profile, animated: false)
            }else{
                let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
                profile.postUserIdStr = userIdArray[indexPath.row]
                profile.fromSearchStr = "search"
                self.navigationController?.pushViewController(profile, animated: false)
            }
            
        }else {
            if isSearch == true{
                let tagsPostList = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TagsPostsList") as! TagsPostsList
                tagsPostList.tagIdStr = ((self.tagsFiltersArray[indexPath.row] as AnyObject).object(forKey: "id") as? String)
                tagsPostList.tagNameStr = ((self.tagsFiltersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String)
                self.navigationController?.pushViewController(tagsPostList, animated: false)
            }else{
                let tagsPostList = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TagsPostsList") as! TagsPostsList
                tagsPostList.tagIdStr = tagIdArr[indexPath.row]
                tagsPostList.tagNameStr = tagNamesArr[indexPath.row]
                self.navigationController?.pushViewController(tagsPostList, animated: false)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.mySearch.resignFirstResponder()
    }
}

extension Search:  UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK:- SEARCH FILTER
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if checkStr == "tags" {
            if searchText.count == 0{
                self.isSearch = false
                self.tagsTable.isHidden = false
                tagsFiltersArray.removeAll()
                tagsTable.reloadData()
                mySearch.resignFirstResponder()
            }else{
                self.isSearch = true
                
                print("filter array total products ",(self.tagsDataArray.object(at: 0) as AnyObject).count as Any)
                
                let namePredicate = NSPredicate(format: "name CONTAINS[C] %@", searchText)
                print("name predicate ",namePredicate)
                tagsFiltersArray.removeAll()
                
                tagsFiltersArray = (self.tagsDataArray as NSArray).filtered(using: namePredicate) as! [[String : AnyObject]]
                print("names = \(tagsFiltersArray)")
                print("search text",searchText)
                tagsTable.reloadData()
                if tagsFiltersArray.count > 0{
                    self.tagsTable.isHidden = false
                    tagsTable.reloadData()
                }else{
                    print("Tags Data Is Not Available")
                    self.tagsTable.isHidden = true
                    self.img.image = UIImage.init(named: "NoHashTags")
                    self.titleLbl.text = languageChangeString(a_str: "Hashtag Not Found")
                    self.descLbl.text = languageChangeString(a_str: "Very sorry,but the hashtag you're looking for doesn't exist")
                }
            }
            
        }else{
            checkStr = "search"
            if searchText.count == 0{
                self.isSearch = false
                self.searchTable.isHidden = false
                usersFiltersArray.removeAll()
                searchTable.reloadData()
                mySearch.resignFirstResponder()
            }else{
                self.isSearch = true
            
                print("filter array total products ",(self.usersDataArray.object(at: 0) as AnyObject).count as Any)
            
                let namePredicate = NSPredicate(format: "name CONTAINS[C] %@", searchText)
                print("name predicate ",namePredicate)
                usersFiltersArray.removeAll()
            
                usersFiltersArray = (self.usersDataArray as NSArray).filtered(using: namePredicate) as! [[String : AnyObject]]
                print("names = \(usersFiltersArray)")
                print("search text",searchText)
                searchTable.reloadData()
            
                if usersFiltersArray.count > 0{
                    self.searchTable.isHidden = false
                    searchTable.reloadData()
                }else{
                    print("People Data Is Not Available")
                    self.searchTable.isHidden = true
                    self.img.image = UIImage.init(named: "NoPeople")
                    self.titleLbl.text = languageChangeString(a_str: "User Profile Not Found")
                    self.descLbl.text = languageChangeString(a_str: "Very sorry,but the user profile you're looking for doesn't exist")
            }
        }
        }
    }
}
