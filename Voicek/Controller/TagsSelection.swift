//
//  TagsSelection.swift
//  Voicek
//
//  Created by volivesolutions on 09/11/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TagsSelection: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet weak var tagsTableView: UITableView!
    var ownerUserIdStr : String!
    
    var tagIdArray = [String]()
    var tagNameArray = [String]()
    var allData : [[String : AnyObject]]!
    var tagsCheckArray = [String]()
    
    var IdString : String!
    
    var idArray = [String]()
    var checkArr = [String]()
    var selectedRows:[IndexPath] = []
    
   // var selectedRows = [String]()
    
    var cell : AddTagsCell!
    var allCheck : String!
    var saveBool : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.text = languageChangeString(a_str: "Tags")
        self.saveBtn.setTitle(languageChangeString(a_str: "Done"), for: UIControlState.normal)
        self.tagsServiceCall()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func save(_ sender: Any) {
        if saveBool == true
        {
            
           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "brands"), object: nil)
            self.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set("filt", forKey: "savebS")
            
        }
 
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        self.tagsCheckArray = [String]()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:AUDIO TAGS SERVICE CALL
    func tagsServiceCall()
    {
        // http://volive.in/voicek/api/services/audio_tags?API-KEY=225143
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)audio_tags?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY]
        
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    
                    self.tagNameArray = [String]()
                    self.tagIdArray = [String]()
                    self.idArray = [String]()
                    self.checkArr = [String]()
                    Services.sharedInstance.dissMissLoader()
                    self.allData = responseData["data"] as? [[String:AnyObject]]
                   // self.idArray = UserDefaults.standard.object(forKey: "IdsStringArr") as! [String]
                    for each in self.allData
                    {
                        let tagId = each["id"] as? String
                        let tagName = each["name"] as? String
                        self.tagIdArray.append(tagId!)
                        self.tagNameArray.append(tagName!)
                        
                        let myInt2 = (tagId! as NSString).integerValue
                        
                        let myString = String(myInt2)
                        
                        if UserDefaults.standard.object(forKey: "savebS") != nil
                        {
                            self.saveBool = true
                            let idArr = UserDefaults.standard.object(forKey: "IdsStringArr") as! Array<Any>
                            
                            self.checkArr = idArr as! [String]
                            
                            print(self.checkArr)
                            
                            if self.checkArr .contains(myString)
                            {
                                let checkStr = "1"
                                self.tagsCheckArray.append(checkStr)
                                
                            }else
                            {
                                let checkStr = "0"
                                self.tagsCheckArray.append(checkStr)
                            }
                        }
                        else
                        {
                            let checkStr = "0"
                            self.tagsCheckArray.append(checkStr)
                        }
                    }
                   
                    DispatchQueue.main.async {
                        self.tagsTableView.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //self.showToast(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension TagsSelection: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : AddTagsCell
        cell = tagsTableView.dequeueReusableCell(withIdentifier: "AddTagsCell", for: indexPath) as! AddTagsCell
        cell.tagNameLabel.text = String(format: "#%@",tagNameArray[indexPath.row])
        let userIdStr = self.tagIdArray[indexPath.row]
        
        if tagsCheckArray[indexPath.row] == "0" {
            cell.checkBox.image = UIImage(named: "UnCheckBox")
        }else if tagsCheckArray[indexPath.row] == "1"{
            cell.checkBox.image = UIImage(named: "CheckBoxGreen")
            idArray.append(userIdStr)
            
            //            let name = self.selectCountryArr[indexPath.row]
            //            selectedName.append(name)
            print("Id Array Is :\(idArray)")
            
            IdString = idArray.joined(separator: ",")
            print(IdString)
            UserDefaults.standard.set( IdString , forKey: "bandIdsString") //setObject
            UserDefaults.standard.set( idArray , forKey: "IdsStringArr")
            
        }else{
            
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 96
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        saveBool = true
        if tagsCheckArray[indexPath.row] == "0" {
            
            tagsCheckArray.remove(at: indexPath.row)
            tagsCheckArray.insert("1", at: indexPath.row)
            
            print(tagsCheckArray)
            
            IdString = ""
            idArray = [String]()
            
            tagsTableView.reloadData()
            
        }
        else if tagsCheckArray[indexPath.row] == "1"{
            //UserDefaults.standard.set("filt", forKey: "savebS")
            //  UserDefaults.standard.removeObject(forKey: "savebS")
            tagsCheckArray.remove(at: indexPath.row)
            tagsCheckArray.insert("0", at: indexPath.row)
            print(tagsCheckArray)
            IdString = ""
            idArray = [String]()
            if !(self.tagsCheckArray.contains("1")){
                print("There are no items added")
                saveBool = false
                UserDefaults.standard.removeObject(forKey: "savebS")
            }
            tagsTableView.reloadData()
            
        }
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let tagsCell = self.tagsTableView.cellForRow(at: indexPath) as! AddTagsCell
//
//        self.idArray.remove(at: self.idArray.index(of:self.tagIdArray[indexPath.row])!)
//        IdString = idArray.joined(separator: ",")
//        print("tempArray select:\(self.idArray)")
//        print("tempArray deselect:\(self.idArray)")
//        tagsCell.checkBox.image = UIImage(named: "UnCheckBox")
//        if idArray.count == 0{
//            print("There are no items added")
//            saveBool = false
//            UserDefaults.standard.removeObject(forKey: "savebS")
//        }
//    }
}
