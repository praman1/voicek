//
//  ChangePassword.swift
//  Voicek
//
//  Created by Suman Guntuka on 20/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ChangePassword: UIViewController {
    @IBOutlet var currentPasswordLabel: UILabel!
    @IBOutlet var newPasswordLabel: UILabel!
    @IBOutlet var reEnterPasswordLabel: UILabel!
    
    @IBOutlet var changeBtn: UIButton!
    
    @IBOutlet weak var currentPswdTF: UITextField!
    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var reEnterPswdTF: UITextField!
    
    var ownerUserIdStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = languageChangeString(a_str: "Change Password")
        self.currentPasswordLabel.text = languageChangeString(a_str: "Current Password")
        self.newPasswordLabel.text = languageChangeString(a_str: "New Password")
        self.reEnterPasswordLabel.text = languageChangeString(a_str: "Re-enter New Password")
        self.changeBtn.setTitle(languageChangeString(a_str: "Change"), for: UIControlState.normal)
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        // Do any additional setup after loading the view.
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:CHANGE PASSWORD SERVICE CALL
    func changePasswordServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        //http://volive.in/voicek/api/services/change_password
        Services.sharedInstance.loader(view: self.view)
        
        let signin = "\(Base_Url)change_password"

        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id":self.ownerUserIdStr!, "old_passwd":self.currentPswdTF.text!, "new_passwd" : self.newPswdTF.text! , "confirm_passwd" : self.reEnterPswdTF.text!]
        
        print(parameters)
        
        Alamofire.request(signin, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            let responseData = response.result.value as? Dictionary<String, Any>
            print("change_password Response data Is:\(responseData!)")
            
            let status = responseData!["status"] as! Int
            let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: message)
                self.navigationController?.popViewController(animated: false)
            }
            else
            {
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }

    @IBAction func update(_ sender: Any) {
        changePasswordServiceCall()
    }
}
