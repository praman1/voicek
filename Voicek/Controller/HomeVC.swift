//
//  HomeVC.swift
//  Voicek
//
//  Created by Prashanth on 19/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//
//,UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning

import UIKit
import SideMenu
import Alamofire
import BubbleTransition
import JTMaterialTransition
import FacebookLogin
import FBSDKLoginKit


class HomeVC: UIViewController,UIViewControllerTransitioningDelegate,LoginButtonDelegate {
    
    
    //hiden content
    @IBOutlet weak var nopostLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var findFrnds: UIButton!
    //main content
    @IBOutlet weak var homeTable: UITableView!
    
    @IBOutlet weak var record: UIButton!
    @IBOutlet var caption: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var postImg: UIImageView!
   
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var progressBar: CircularProgressBar!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var noPostsImage: UIImageView!
    
    @IBOutlet var mySearch: UISearchBar!
    var appDelegate = AppDelegate()
    
      var filtersArray = [[String:AnyObject]]()
    var totalProductsArray = NSMutableArray()
    var refreshControl: UIRefreshControl?
    var postIdArr = [String]()
    var userIdArr = [String]()
    var imageArr = [String]()
    var audioArr = [String]()
    var durationArr = [String]()
    var descArr = [String]()
    var nameArr = [String]()
    var isPublicArr = [String]()
    var playedArr = [String]()
    var likesArr = [String]()
    var requestedArr = [String]()
    var notiCountArr = [String]()
    
    var filteredData: [String]!
    var filteredImages : [String]!
    var filteredUserId :[String]!
    
    
   // var monthsArray = [String]()
   
    var isPresenting: Bool!
    var point: CGPoint!
    let animationDuration = 0.3
    
    var postImage = UIImage()
    var postName : String!
    var fromPost : String!
    var pickedImage  = UIImage()
    var ownerUserIdStr : String?
    var pickedAudio : NSURL?
    var durationInSeconds = Int()
    var durationTime : Int!
    var userIdStr : String!
    var tagsStr : String! = ""
    var postTypeString : String! = ""
    var followersString : String! = ""
    var postsData = NSArray()
   // var postsData : [[String : AnyObject]]!
    
    var serviceData : [[String : AnyObject]]!
    //var sendPostData = [String : Any]()
    var sendPostData = Dictionary<String, Any>()
    
    var progressCircle = CAShapeLayer()
    var transition = BubbleTransition()
    let interactiveTransition = BubbleInteractiveTransition()
    var transition1: JTMaterialTransition?
    var searchController : UISearchController!
    
    var searchText : String! = ""
    var checkString : Bool = false
    var addString : String! = ""
    
    //Mini Player
    var miniPlayerHeight: CGFloat = 80.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.searchController = UISearchController(searchResultsController:  nil)
//        self.searchController.searchResultsUpdater = self
//        self.searchController.delegate = self
//        self.searchController.searchBar.delegate = self
//        self.searchController.hidesNavigationBarDuringPresentation = false
//        //self.searchController.dimsBackgroundDuringPresentation = true
//        self.navigationItem.titleView = searchController.searchBar
//        self.definesPresentationContext = true
        
       // self.datesList()
       // self.dateCompare()
//        DispatchQueue.main.async {
        self.viewHeight.constant = 0
        self.caption.isHidden = true
        self.userName.isHidden = true
        //self.postImg.isHidden = true
        self.progressBar.isHidden = true
        
//            self.topView.isHidden = true
//        }
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        let colors = [(UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)), (UIColor(red: 0.0 / 255.0, green: 223.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0))]
        self.navigationController?.navigationBar.gradeintBarTintColor(with: colors)
        
        self.transition1 = JTMaterialTransition(animatedView: self.record!)
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor(red: 0.0 / 255.0, green: 226.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
        homeTable.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(self.refreshTable), for: .valueChanged)
        
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        
        let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuVC)
        menuLeftNavigationController.leftSide = true
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1
        
        self.navigationItem.title = languageChangeString(a_str: "Home")
        self.mySearch.placeholder = languageChangeString(a_str: "Search")
        
        record.layer.cornerRadius = record.layer.frame.size.height/2
        record.layer.shadowColor = UIColor.init(red: 0.0/255.0, green: 218.0/255.0, blue: 244.0/255.0, alpha: 1).cgColor
        //recordBtn.layer.shadowColor = UIColor.black.cgColor
        //recordBtn.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        record.layer.shadowOpacity = 1.0
        record.layer.shadowRadius = 2.0
        record.layer.masksToBounds = false
       
        
        //getPostsServiceCall()
        
        //self.record.addTarget(self, action: #selector(didPresentControllerButtonTouch), for: .touchUpInside)
//        DispatchQueue.main.async {
//            if UserDefaults.standard.string(forKey: "currentLanguage") == ENGLISH_LANGUAGE{
//                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
//            }
//            else
//            {
//                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
//            }
//        }
        
    }
    
    @objc func refreshTable() {
       // getPostsServiceCall()
        DispatchQueue.main.async(execute: {
            self.refreshControl?.endRefreshing()
            self.homeTable.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        DispatchQueue.main.async {
//            if UserDefaults.standard.string(forKey: "currentLanguage") == ENGLISH_LANGUAGE{
//                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
//            }
//            else
//            {
//                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
//            }
//        }
       // DispatchQueue.main.async {
            self.getPostsServiceCall()
       // }
        
        if UserDefaults.standard.object(forKey: "StringType") != nil {
            print(self.sendPostData)
            let typeStrHome = UserDefaults.standard.object(forKey: "StringType") as! String
            
            if typeStrHome == "add"
            {
                self.postImg.layer.cornerRadius = self.postImg.frame.size.height/2
                self.viewHeight.constant = 60
                self.caption.isHidden = false
                self.userName.isHidden = false
                //self.postImg.isHidden = false
                self.progressBar.isHidden = false
                
                self.sendAudioServiceCall()
            }
            else{
                
            }
        }else if UserDefaults.standard.object(forKey: "postType") != nil{
            
            let typeStrHome = UserDefaults.standard.object(forKey: "postType") as! String
            
            if typeStrHome == "listen"
            {
                let data = UserDefaults.standard.object(forKey: "postDataFinal") as! NSData
                let postObject = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! NSDictionary
                self.sendPostData = postObject as! [String : Any]
                
                self.postImg.layer.cornerRadius = self.postImg.frame.size.height/2
                self.viewHeight.constant = 60
                self.caption.isHidden = false
                self.userName.isHidden = false
                //self.postImg.isHidden = false
                self.progressBar.isHidden = false
                
                self.sendAudioServiceCall()
            }
            else{
            }
        }else{
        }
    }
    
    func progressBarAnimation(){
        progressBar.labelSize = 14
        progressBar.safePercent = 100
        progressBar.setProgress(to: 1, withAnimation: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.mySearch.resignFirstResponder()
    }
    
    //MARK:SEND AUDIO SERVICE CALL
     func sendAudioServiceCall()
        {
        print(sendPostData)
        
        let image = sendPostData["image"] as? UIImage
        let captionMsg = sendPostData["caption"] as? String
        let audio = sendPostData["audio"] as? NSURL
        let duration = sendPostData["duration"] as? Int
        let userid = sendPostData["userId"] as? String
        let tags = sendPostData["tags"] as? String
        let postType = sendPostData["postType"] as? String
        let followers = sendPostData["listeners"] as? String
        
        postName = captionMsg!
        pickedAudio = audio!
        pickedImage = image!
        durationTime = duration!
        userIdStr = userid!
        tagsStr = tags ?? ""
        postTypeString = postType
        followersString = followers ?? ""
        
        self.postImg.image = self.pickedImage
        self.caption.text = self.postName
        self.userName.text = UserDefaults.standard.object(forKey: "first_name") as? String
        // sendRecordServiceCall()
        // self.animationCall()
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id":userIdStr! , "description":postName! , "duration" :String (durationTime) , "tags" :tagsStr ?? "","post_type" :postTypeString!, "followers" : followersString ?? ""]
        
        print("Record Is",parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            //For Audio
            if self.pickedAudio != nil{
                do {
                    let audioData = try Data(contentsOf: (self.pickedAudio as URL?)!)
                    multipartFormData.append(audioData , withName: "audio_file", fileName: "audio.wav", mimeType: "audio/wav")
                } catch {
                    print(error)
                    return
                }
            }
            //For Image
            if self.pickedImage.size.width != 0{
                let image = self.pickedImage
                let imgData = UIImageJPEGRepresentation(image, 0.2)!
                multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:"\(Base_Url)record")
        { (result) in
            print(result)
            switch result {
            case .success(let upload, _, _):
                self.progressBarAnimation()
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    print(response.result.value as Any)
                    let responseData = response.result.value as? Dictionary<String, Any>
                    print("Post ResponseData Is ***:\(responseData as Any)")
                    self.getPostsServiceCall()
                    UIView.transition(with: self.topView, duration: 0.2, options: .transitionCrossDissolve, animations: {
                        self.viewHeight.constant = 0
                        self.topView.isHidden = true
                        self.caption.isHidden = true
                        self.userName.isHidden = true
                        self.progressBar.isHidden = true
                    })
                   // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postDataType"), object: nil)
                    UserDefaults.standard.removeObject(forKey: "StringType")
                    UserDefaults.standard.removeObject(forKey: "postData")
                    UserDefaults.standard.removeObject(forKey: "postType")
                    UserDefaults.standard.removeObject(forKey: "postDataFinal")
                    self.sendPostData.removeAll()
                    print("Removed Send Post Data Is :\(self.sendPostData)")
                }
                
            case .failure(let encodingError):
                print(encodingError)
                self.showToast(message: self.languageChangeString(a_str: "Failure...")!)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }

    @IBAction func recordBtn(_ sender: Any) {

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let record = storyBoard.instantiateViewController(withIdentifier: "Record")
        
        record.modalPresentationStyle = .custom
        record.transitioningDelegate = self.transition1
        self.record.backgroundColor = UIColor.init(red: 0/255.0, green: 230.0/255.0, blue: 177.0/255.0, alpha: 1)
        self.present(record, animated: true, completion: nil)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveTransition
    }
    
    
    //MARK:- GET POST SERIVICE CALL
    func getPostsServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/get_posts?API-KEY=225143&user_id=3783858425
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)get_posts?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    //NotificationCenter.default.removeObserver(self)
                    self.postIdArr = [String]()
                    self.userIdArr = [String]()
                    self.imageArr = [String]()
                    self.audioArr = [String]()
                    self.durationArr = [String]()
                    self.descArr = [String]()
                    self.nameArr = [String]()
                    self.isPublicArr = [String]()
                    self.playedArr = [String]()
                    self.likesArr = [String]()
                    self.requestedArr = [String]()
                    self.filteredData = [String]()
                    self.filteredImages = [String]()
                    self.filteredUserId = [String]()
                    self.notiCountArr = [String]()
                    Services.sharedInstance.dissMissLoader()
                    let totalItems:NSArray = responseData["data"] as! NSArray
                    
                    self.serviceData = responseData["data"] as? [[String : AnyObject]]
                    self.totalProductsArray.addObjects(from: totalItems as! [Any])
                      print("total data array",self.totalProductsArray)
                    self.postsData = self.totalProductsArray
                    print("post data array",self.postsData)
                    
                    for each in self.serviceData
                    {
                        let postId = each["id"] as? String
                        //self.nullToNil(value: postId as AnyObject)
                        let userId = each["user_id"] as? String

                        let image = each["image"] as? String
                        let audio = each["audio"] as? String

                        let duration = each["duration"] as? String
                        let description = each["description"] as? String

                        let name = each["name"] as? String
                        let is_public = each["is_public"] as? String

                        let x : Int = each["played"] as! Int
                        let xNSNumber = x as NSNumber
                        let played : String = xNSNumber.stringValue

                        let x1 : Int = each["likes"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let likes : String = xNSNumber1.stringValue

                        let x2 : Int = each["requested"] as! Int
                        let xNSNumber2 = x2 as NSNumber
                        let requested : String = xNSNumber2.stringValue
                        //noti_count
                        let x3 : Int = each["noti_count"] as! Int
                        let xNSNumber3 = x3 as NSNumber
                        let noti_count : String = xNSNumber3.stringValue
                        
                        self.postIdArr.append(postId!)
                        self.userIdArr.append(userId!)

                        self.imageArr.append(image!)
                        self.audioArr.append(audio!)

                        self.durationArr.append(duration!)
                        self.descArr.append(description!)

                        self.nameArr.append(name!)
                        self.isPublicArr.append(is_public!)

                        self.playedArr.append(played)
                        self.likesArr.append(likes)
                        self.requestedArr.append(requested)
                        self.notiCountArr.append(noti_count)
                    }
                    DispatchQueue.main.async {
                        self.homeTable.reloadData()
                        self.refreshControl?.endRefreshing()
                        
                        if  self.postsData.count > 0
                        {
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.noPostsImage.isHidden = false
                        self.nopostLbl.isHidden = false
                        self.descLbl.isHidden = false
                        self.findFrnds.isHidden = false
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        self.homeTable.isHidden = true
                        self.record.isHidden = false
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @IBAction func sideMenuBtn(_ sender: Any) {
        
         present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        print("logged in")
       // self.getFBUserData()
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }
    
    //MARK:GET FB USER DATA
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name,email,first_name,last_name,picture.width(100).height(100)"]).start(completionHandler: { (connection, result, error) -> Void in
                print("Error Is :\(String(describing: error))")
                if (error == nil){

                }else{
                    print("facebook Result Is :\(result as Any)")
                }
            })
        }else{
            print("pp")
        }
    }
    
    @IBAction func findFriendsBtn(_ sender: Any) {
        
        appDelegate.signInFor = "fb"
        let loginButton = LoginButton(readPermissions: [ .publicProfile ])
        loginButton.center = view.center
        loginButton.delegate = self
        loginButton.isHidden = true
        
        view.addSubview(loginButton)
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        print("e2")
                        self.getFBUserData()
                        //fbLoginManager.logOut()
                    }
                }
            }else{
                self.getFBUserData()
                print("ee1\(error!)")
            }
        }

    }
}

extension HomeVC: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if checkString == true {
            return filtersArray.count
        }else{
            return postsData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : HomeCell
        
        if checkString == true {
            cell = homeTable.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
            cell.badgeLabel.layer.cornerRadius = cell.badgeLabel.frame.size.height/2
            
            let x2 : Int = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "noti_count") as! Int
            let xNSNumber2 = x2 as NSNumber
            let noti_count : String = xNSNumber2.stringValue
            
            if x2 > 0{
                cell.badgeLabel.isHidden = false
            }else{
                cell.badgeLabel.isHidden = true
            }
            cell.badgeLabel.text = noti_count
            cell.nameLbl.text = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String
            cell.homeImg.sd_setImage(with: URL (string: (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "image") as! String), placeholderImage: UIImage(named: "Logo"))
            cell.subNameLbl.text = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "description") as? String
            
            let x1 : Int = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "likes") as! Int
            let xNSNumber1 = x1 as NSNumber
            let likes : String = xNSNumber1.stringValue
            
            cell.likesLBl.text = String(format: "%@ %@", likes,self.languageChangeString(a_str: "Likes")!)
            
            let x : Int = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "played") as! Int
            let xNSNumber = x as NSNumber
            let played : String = xNSNumber.stringValue
            
            cell.playedLbl.text = String(format: "%@ %@", played,self.languageChangeString(a_str: "Played")!)
            
            cell.secLbl.text = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "duration") as? String
            cell.playBtn.tag = indexPath.row
            cell.playBtn.addTarget(self, action: #selector(playClicked(sender:)), for: UIControlEvents.touchUpInside)
        }else{
            cell = homeTable.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
            cell.badgeLabel.layer.cornerRadius = cell.badgeLabel.frame.size.height/2
            let x2 : Int = (self.postsData[indexPath.row] as AnyObject).object(forKey: "noti_count") as! Int
            let xNSNumber2 = x2 as NSNumber
            let noti_count : String = xNSNumber2.stringValue
            
            if x2 > 0{
                print("noti count greater")
                cell.badgeLabel.isHidden = false
            }else{
                 print("noti count less")
                cell.badgeLabel.isHidden = true
            }
            cell.badgeLabel.text = noti_count
            
            cell.nameLbl.text = (self.postsData[indexPath.row] as AnyObject).object(forKey: "name") as? String
            cell.homeImg.sd_setImage(with: URL (string: (self.postsData[indexPath.row] as AnyObject).object(forKey: "image") as! String), placeholderImage: UIImage(named: "Logo"))
            cell.subNameLbl.text = (self.postsData[indexPath.row] as AnyObject).object(forKey: "description") as? String
            
            let x1 : Int = (self.postsData[indexPath.row] as AnyObject).object(forKey: "likes") as! Int
            let xNSNumber1 = x1 as NSNumber
            let likes : String = xNSNumber1.stringValue
            
            cell.likesLBl.text = String(format: "%@ %@", likes,self.languageChangeString(a_str: "Likes")!)
            
            let x : Int = (self.postsData[indexPath.row] as AnyObject).object(forKey: "played") as! Int
            let xNSNumber = x as NSNumber
            let played : String = xNSNumber.stringValue
            
            cell.playedLbl.text = String(format: "%@ %@", played,self.languageChangeString(a_str: "Played")!)
            cell.secLbl.text = (self.postsData[indexPath.row] as AnyObject).object(forKey: "duration") as? String
            cell.playBtn.tag = indexPath.row
            cell.playBtn.addTarget(self, action: #selector(playClicked(sender:)), for: UIControlEvents.touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if checkString == true{
            if ((self.filtersArray[indexPath.row] as AnyObject).object(forKey: "name")) as? String  == "2"
            {
                if ((self.filtersArray[indexPath.row] as AnyObject).object(forKey: "user_id") as? String) != ownerUserIdStr{
                    if ((self.filtersArray[indexPath.row] as AnyObject).object(forKey: "requested")) as? String == "1"{
                        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                        play.postId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "id") as? String
                        play.postUserId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "user_id") as? String
                        play.audioDuration = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "duration") as? String
                        play.audioUrlString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "audio") as? String
                        play.profileNameString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String
                        //DispatchQueue.main.async {
                        self.navigationController?.pushViewController(play, animated: false)
                        // }
                        
                    }else{
                        self.showToastForError(message: languageChangeString(a_str:"Please follow this user to play this post")!)
                    }
                }else{
                    let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                    play.postId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "id") as? String
                    play.postUserId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "user_id") as? String
                    play.audioDuration = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "duration") as? String
                    play.audioUrlString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "audio") as? String
                    play.profileNameString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String
                    //DispatchQueue.main.async {
                    self.navigationController?.pushViewController(play, animated: false)
                    //}
                }
            }else{
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "id") as? String
                play.postUserId = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "user_id") as? String
                play.audioDuration = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "duration") as? String
                play.audioUrlString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "audio") as? String
                play.profileNameString = (self.filtersArray[indexPath.row] as AnyObject).object(forKey: "name") as? String
                //DispatchQueue.main.async {
                self.navigationController?.pushViewController(play, animated: false)
                // }
            }
        }else{
            if isPublicArr[indexPath.row] == "2"
            {
                if userIdArr[indexPath.row] != ownerUserIdStr{
                    if requestedArr[indexPath.row] == "1"{
                        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                        play.postId = postIdArr[indexPath.row]
                        play.postUserId = userIdArr[indexPath.row]
                        play.audioDuration = durationArr[indexPath.row]
                        play.audioUrlString = audioArr[indexPath.row]
                        play.profileNameString = nameArr[indexPath.row]
                        //DispatchQueue.main.async {
                        self.navigationController?.pushViewController(play, animated: false)
                        // }
                        
                    }else{
                        self.showToastForError(message: languageChangeString(a_str:"Please follow this user to play this post")!)
                    }
                }else{
                    let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                    play.postId = postIdArr[indexPath.row]
                    play.postUserId = userIdArr[indexPath.row]
                    play.audioDuration = durationArr[indexPath.row]
                    play.audioUrlString = audioArr[indexPath.row]
                    play.profileNameString = nameArr[indexPath.row]
                    //DispatchQueue.main.async {
                    self.navigationController?.pushViewController(play, animated: false)
                    //}
                }
            }else{
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = postIdArr[indexPath.row]
                play.postUserId = userIdArr[indexPath.row]
                play.audioDuration = durationArr[indexPath.row]
                play.audioUrlString = audioArr[indexPath.row]
                play.profileNameString = nameArr[indexPath.row]
                //DispatchQueue.main.async {
                self.navigationController?.pushViewController(play, animated: false)
                // }
            }
            
        }
    }
    
    
    //MARK:PLAY CLICKED
    @objc func playClicked(sender:UIButton)
    {
        let buttonRow = sender.tag
        if checkString == true{
        if ((self.filtersArray[buttonRow] as AnyObject).object(forKey: "name")) as? String  == "2"
        {
            if ((self.filtersArray[buttonRow] as AnyObject).object(forKey: "user_id") as? String) != ownerUserIdStr{
                if ((self.filtersArray[buttonRow] as AnyObject).object(forKey: "requested")) as? String == "1"{
                        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                        play.postId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "id") as? String
                        play.postUserId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "user_id") as? String
                        play.audioDuration = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "duration") as? String
                        play.audioUrlString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "audio") as? String
                        play.profileNameString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "name") as? String
                    //DispatchQueue.main.async {
                        self.navigationController?.pushViewController(play, animated: false)
                   // }
                    
                    }else{
                        self.showToastForError(message: languageChangeString(a_str:"Please follow this user to play this post")!)
                    }
            }else{
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "id") as? String
                play.postUserId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "user_id") as? String
                play.audioDuration = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "duration") as? String
                play.audioUrlString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "audio") as? String
                play.profileNameString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "name") as? String
                //DispatchQueue.main.async {
                    self.navigationController?.pushViewController(play, animated: false)
                //}
            }
    }else{
            let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
            play.postId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "id") as? String
            play.postUserId = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "user_id") as? String
            play.audioDuration = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "duration") as? String
            play.audioUrlString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "audio") as? String
            play.profileNameString = (self.filtersArray[buttonRow] as AnyObject).object(forKey: "name") as? String
            //DispatchQueue.main.async {
                self.navigationController?.pushViewController(play, animated: false)
           // }
        }
    }else{
            if isPublicArr[buttonRow] == "2"
            {
                if userIdArr[buttonRow] != ownerUserIdStr{
                    if requestedArr[buttonRow] == "1"{
                        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                        play.postId = postIdArr[buttonRow]
                        play.postUserId = userIdArr[buttonRow]
                        play.audioDuration = durationArr[buttonRow]
                        play.audioUrlString = audioArr[buttonRow]
                        play.profileNameString = nameArr[buttonRow]
                        //DispatchQueue.main.async {
                        self.navigationController?.pushViewController(play, animated: false)
                        // }
                        
                    }else{
                        self.showToastForError(message: languageChangeString(a_str:"Please follow this user to play this post")!)
                    }
                }else{
                    let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                    play.postId = postIdArr[buttonRow]
                    play.postUserId = userIdArr[buttonRow]
                    play.audioDuration = durationArr[buttonRow]
                    play.audioUrlString = audioArr[buttonRow]
                    play.profileNameString = nameArr[buttonRow]
                    //DispatchQueue.main.async {
                    self.navigationController?.pushViewController(play, animated: false)
                    //}
                }
            }else{
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = postIdArr[buttonRow]
                play.postUserId = userIdArr[buttonRow]
                play.audioDuration = durationArr[buttonRow]
                play.audioUrlString = audioArr[buttonRow]
                play.profileNameString = nameArr[buttonRow]
                //DispatchQueue.main.async {
                self.navigationController?.pushViewController(play, animated: false)
                // }
            }
    }
    }
}
    
extension UINavigationBar{
    func gradeintBarTintColor(with gradient:[UIColor]){
        var frameAndStatusBarSize = self.bounds
        frameAndStatusBarSize.size.height +=  20
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBarSize.size, color: gradient), for: .default)
    }
    
    static func gradient(size:CGSize,color:[UIColor]) -> UIImage?{
        //turn color into cgcolor
        let colors = color.map{$0.cgColor}
        
        //begin graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        // From now on, the context gets ended if any return happens
        defer {UIGraphicsEndImageContext()}
        
        //create core graphics context
        let locations:[CGFloat] = [0.0,1.0]
        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
            return nil
        }
        //draw the gradient
        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension HomeVC:  UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK:- SEARCH FILTER
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count == 0{
            self.checkString = false
            filtersArray.removeAll()
            homeTable.reloadData()
            mySearch.resignFirstResponder()
        }else{
              self.checkString = true
            
            print("filter array total products ",(self.postsData.object(at: 0) as AnyObject).count as Any)

            let namePredicate = NSPredicate(format: "name CONTAINS[C] %@", searchText)
            print("name predicate ",namePredicate)
            filtersArray.removeAll()

            filtersArray = (self.postsData as NSArray).filtered(using: namePredicate) as! [[String : AnyObject]]
            print("names = \(filtersArray)")
            print("search text",searchText)
            homeTable.reloadData()
        }
    }
}
