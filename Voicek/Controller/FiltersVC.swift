//
//  FiltersVC.swift
//  Voicek
//
//  Created by volivesolutions on 16/01/19.
//  Copyright © 2019 Volivesolutions. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import AVFoundation

class FiltersVC: UIViewController,AVAudioPlayerDelegate {
    
    @IBOutlet var nextBtn: UIBarButtonItem!
    @IBOutlet var filtersCollectionView: UICollectionView!
    @IBOutlet var timerLabel: UILabel!
    var recordingSession: AVAudioSession!
    var currentPage: Int = 0
    var pickedAudio = NSURL()
    var duration : Int!
    var soundPlayer : AVAudioPlayer?
    var soundUrl : URL?
    var timer = Timer()
    var seconds = Int()
    var nameArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameArray = [languageChangeString(a_str: "It's Just Me"),languageChangeString(a_str: "Hear Me Out"),languageChangeString(a_str: "Robots Take Over"),languageChangeString(a_str: "Shake"),languageChangeString(a_str: "Rattle & Roll") ,languageChangeString(a_str: "Good Ol' Days")] as! [String]
        self.nextBtn.title = languageChangeString(a_str: "Next")
        self.navigationItem.title = languageChangeString(a_str: "Choose a filter")
        seconds = duration
        self.preparePlayer()
        self.runTimer()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        // Do any additional setup after loading the view.
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.filtersCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .vertical {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    fileprivate func setupLayout() {
        let layout = self.filtersCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 80)
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(FiltersVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func stopTimer()
    {
        if timer != nil {
            timer.invalidate()
            timer = Timer()
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1
        timerLabel.text = timeString(time: TimeInterval(seconds)) //This will update the label.
        if seconds == 00 {
            stopTimer()
            
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        // let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func preparePlayer() {
        //DispatchQueue.main.async {
        let data = NSData(contentsOf: self.pickedAudio as URL)
        self.soundPlayer = try? AVAudioPlayer(data: data! as Data)
        self.soundPlayer?.prepareToPlay()
        self.soundPlayer?.delegate = self
        self.soundPlayer?.play()
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch {
            print("failed to record!")
        }
       // self.soundPlayer?.
        //}
    }
    
    @IBAction func backBtn(_ sender: Any) {
      //  self.navigationController?.popViewController(animated: true)
        if pickedAudio.absoluteString != "" {
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: languageChangeString(a_str: "Do you want to discard the recording?"), preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "NO"), style: .cancel) { action -> Void in }
            let yesAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "YES"), style: .default) { action -> Void in
                print("Timer Has Stopped")
                self.navigationController?.popViewController(animated: true)
            }
            // add actions
            actionSheetController.addAction(yesAction)
            actionSheetController.addAction(cancelAction)

            // present an actionSheet...
            present(actionSheetController, animated: true, completion: nil)

        }else{
            self .dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCaption") as! AddCaption
        play.pickedAudio = pickedAudio
        play.duration = duration
        self.navigationController?.pushViewController(play, animated: true)
    }
}

extension FiltersVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = FiltersCell()
        cell = filtersCollectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCell", for: indexPath) as! FiltersCell
        
        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.height/2
        cell.filterNameLabel.text = nameArray[indexPath.row]
        //cell.imageView.image = imgArr[indexPath.row]
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.filtersCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .vertical) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .vertical) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        print(currentPage)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 180 , height:180)
    }
}
