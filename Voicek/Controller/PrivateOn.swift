
//
//  PrivateOn.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class PrivateOn: UIViewController {

    @IBOutlet var privateMessageLabel: UILabel!
    
    @IBOutlet var yesBtn: UIButton!
    
    @IBOutlet var cancelBtn: UIButton!
    
    var isPublicString : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
        self.privateMessageLabel.text = languageChangeString(a_str: "Making your account private will hide your posts from users who are not listening to you. are you sure you want to set your account private?")
        self.yesBtn.setTitle(languageChangeString(a_str: "Yes"), for: UIControlState.normal)
        self.cancelBtn.setTitle(languageChangeString(a_str: "Cancel"), for: UIControlState.normal)
        
        // Do any additional setup after loading the view.
    }
    @objc func hideView()
    {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func yesBtn(_ sender: Any) {
        
        isPublicString = "2"
        UserDefaults.standard.set(isPublicString, forKey: "is_public")
        NotificationCenter.default.post(name: Notification.Name("isPublic"), object: nil)
        self.dismiss(animated: true, completion: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
