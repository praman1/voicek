//
//  PostDetails.swift
//  Voicek
//
//  Created by Suman Guntuka on 24/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu

class PostDetails: UIViewController {

    @IBOutlet weak var posts: UIButton!
    @IBOutlet weak var listenrs: UIButton!
    @IBOutlet weak var listenings: UIButton!
    
    @IBOutlet var postsLabel: UILabel!
    @IBOutlet var listeningLabel: UILabel!
    
    @IBOutlet var listenersLabel: UILabel!
    
    @IBOutlet var listeningBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listenings.layer.cornerRadius = listenings.frame.size.height/2
        listenings.layer.borderWidth = 2
        listenings.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        listenrs.layer.cornerRadius = listenrs.frame.size.height/2
        listenrs.layer.borderWidth = 2
        listenrs.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        posts.layer.cornerRadius = posts.frame.size.height/2
        posts.layer.borderWidth = 2
        posts.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.postsLabel.text = languageChangeString(a_str: "Posts")
        self.listeningLabel.text = languageChangeString(a_str: "Listening")
        self.listenersLabel.text = languageChangeString(a_str: "Listerners")
        self.listeningBtn.setTitle(languageChangeString(a_str: "Listening"), for: UIControlState.normal)
   
    }

    
    @IBAction func postBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    
    }
    @IBAction func listenersBtn(_ sender: Any) {
        
        let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Listeners") as! Listeners
        self.navigationController?.pushViewController(listeners, animated: true)
   
    }
    
    @IBAction func listeningBtn(_ sender: Any) {
        
        let listeners = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Listening") as! Listening
        self.navigationController?.pushViewController(listeners, animated: true)
   
    }
   
    @IBAction func listenBtn(_ sender: Any) {
     
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let service = storyBoard.instantiateViewController(withIdentifier: "ListenChoice")
        self .present(service, animated: true, completion: nil)
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func moreBtn(_ sender: Any) {
     
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let reportAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Report"), style: .default) { action -> Void in
            
           let report = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportUser") as! ReportUser
            self.navigationController?.pushViewController(report, animated: true)
            
        }
        let blockAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Block"), style: .default) { action -> Void in
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let service = storyBoard.instantiateViewController(withIdentifier: "BlockUser")
            self .present(service, animated: true, completion: nil)
            
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(reportAction)
        actionSheetController.addAction(blockAction)
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
        
    }
}
