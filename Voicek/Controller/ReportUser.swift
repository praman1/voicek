//
//  ReportUser.swift
//  Voicek
//
//  Created by Suman Guntuka on 24/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class ReportUser: UIViewController {
    
    @IBOutlet var spamImage: UIImageView!
    @IBOutlet var stolenImage: UIImageView!
    @IBOutlet var notAppropriateImage: UIImageView!
    @IBOutlet var spamBtn: UIButton!
    @IBOutlet var stolenBtn: UIButton!
    @IBOutlet var appropriateBtn: UIButton!
    
    @IBOutlet var reportBtn: UIButton!
    @IBOutlet var reportsListTable: UITableView!
    var idArray = [String]()
    var nameArray = [String]()
    var listArray : [[String : AnyObject]]!
    var checkArr = [String]()
    
    var reportIdStr : String! = ""
    var ownerUserId :String!
    var postIdStr : String!
    var postUserId : String!
    var reportCheckStr : String!
    
    var rowsChecked = [IndexPath]()
    var isChecked : Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        spamBtn.tag = 1
//        stolenBtn.tag = 1
//        appropriateBtn.tag = 1
        
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        if reportCheckStr == "fromDetails" {
            self.navigationItem.title = languageChangeString(a_str: "Report a Post")
            reportPostListServiceCall()
        }
        else{
            self.navigationItem.title = languageChangeString(a_str: "Report a User")
            reportUserListServiceCall()
        }
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func spam(_ sender: Any) {
//
//        if spamBtn.tag == 1 {
//            reportIdStr = "1"
//            spamImage.image = UIImage.init(named: "CheckBoxGreen")
//            stolenImage.image = UIImage.init(named: "UnCheckBox")
//            notAppropriateImage.image = UIImage.init(named: "UnCheckBox")
//            spamBtn.tag = 0
//            stolenBtn.tag = 1
//            appropriateBtn.tag = 1
//
//        }else{
//            reportIdStr = ""
//            spamImage.image = UIImage.init(named: "UnCheckBox")
//            spamBtn.tag = 1
//        }
//    }
//
//    @IBAction func stolen(_ sender: Any) {
//
//        if stolenBtn.tag == 1 {
//            reportIdStr = "2"
//            stolenImage.image = UIImage.init(named: "CheckBoxGreen")
//            spamImage.image = UIImage.init(named: "UnCheckBox")
//            notAppropriateImage.image = UIImage.init(named: "UnCheckBox")
//            stolenBtn.tag = 0
//            spamBtn.tag = 1
//            appropriateBtn.tag = 1
//
//        }else{
//            reportIdStr = ""
//            stolenImage.image = UIImage.init(named: "UnCheckBox")
//            stolenBtn.tag = 1
//        }
//    }
//
//    @IBAction func notAppropriate(_ sender: Any) {
//
//        if appropriateBtn.tag == 1 {
//            reportIdStr = "3"
//            notAppropriateImage.image = UIImage.init(named: "CheckBoxGreen")
//            stolenImage.image = UIImage.init(named: "UnCheckBox")
//            spamImage.image = UIImage.init(named: "UnCheckBox")
//            appropriateBtn.tag = 0
//            spamBtn.tag = 1
//            stolenBtn.tag = 1
//
//        }else{
//            reportIdStr = ""
//            notAppropriateImage.image = UIImage.init(named: "UnCheckBox")
//            appropriateBtn.tag = 1
//        }
//    }
    
    @IBAction func report(_ sender: Any) {
        
        if reportIdStr.isEmpty {
            self.showToastForError(message: languageChangeString(a_str: "Please select report type")!)
        }
        else{
             if reportCheckStr == "fromDetails"
             {
                reportPostServiceCall()
            }
            else
             {
                reportUserServiceCall()
            }
        }
    }
    
    //MARK:REPORT USER SERVICE CALL
    func reportPostServiceCall()
    {
        // http://volive.in/voicek/api/services/report?API-KEY=225143&user_id=3783858425&post_id=7&report_id=1
        Services.sharedInstance.loader(view: self.view)
        let reportPost = "\(Base_Url)report?"
        print("Report Post URL Is :\(reportPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "post_id" : postIdStr! , "report_id" : reportIdStr!]
        
        print(parameters)
        
        Alamofire.request(reportPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    //MARK:REPORTS POSTS LIST SERVICE CALL
    func reportPostListServiceCall()
    {
        // http://volive.in/voicek/api/services/report_list?API-KEY=225143&user_id=3783858425&post_id=7
        
        Services.sharedInstance.loader(view: self.view)
        
        let postDetails = "\(Base_Url)report_list?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id" : ownerUserId! , "post_id":postIdStr!]
        
        print(parameters)
        
        Alamofire.request(postDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let responseData = response.result.value as? Dictionary<String, Any>
            print(responseData!)
            
            let status = responseData!["status"] as! Int
            // let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                    
                    let user_report_id = responseData1["user_report_id"] as! String
                    
                    print(user_report_id)
                    
                    self.listArray = responseData1["report_list"] as? [[String:AnyObject]]
                    
                    self.idArray = [String]()
                    self.nameArray = [String]()
                    self.checkArr = [String]()
                    
                    print(self.listArray)
                    let count = self.listArray.count
                    print("Array Count Is: \(count)")
                    
                    for each in self.listArray
                    {
                        let reportId = each["id"] as? String
                        let name = each["name"] as? String
                        let str = "0"
                        
                        self.idArray.append(reportId!)
                        self.nameArray.append(name!)
                        self.checkArr.append(str)
                    }
                    print("Check Array Is :\(self.checkArr)")
                    DispatchQueue.main.async {
                        self.reportsListTable.reloadData()
                    }
                }
            }
            else
            {
                let message = responseData!["message"] as! String
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    //MARK:REPORT USERS LIST SERVICE CALL
    func reportUserListServiceCall()
    {
        // http://volive.in/voicek/api/services/report_user_list?API-KEY=225143&user_id=3783858425&profile_user_id=4182668577
        
        Services.sharedInstance.loader(view: self.view)
        
        let postDetails = "\(Base_Url)report_user_list?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id" : ownerUserId! , "profile_user_id":postUserId!]
        
        print(parameters)
        
        Alamofire.request(postDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let responseData = response.result.value as? Dictionary<String, Any>
            print("Response data Is:\(responseData!)")
            
            let status = responseData!["status"] as! Int
            // let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                    
                    let user_report_id = responseData1["user_report_id"] as! String
                    
                    print(user_report_id)
                    
                    self.listArray = responseData1["report_list"] as? [[String:AnyObject]]
                    
                    self.idArray = [String]()
                    self.nameArray = [String]()
                    self.checkArr = [String]()
                    
                    print(self.listArray)
                    let count = self.listArray.count
                    print("Array Count Is: \(count)")
                    
                    for each in self.listArray
                    {
                        let reportId = each["id"] as? String
                        let name = each["name"] as? String
                        let str = "0"
                        
                        self.idArray.append(reportId!)
                        self.nameArray.append(name!)
                        self.checkArr.append(str)
                    }
                    DispatchQueue.main.async {
                        self.reportsListTable.reloadData()
                    }
                }
            }
            else
            {
                let message = responseData!["message"] as! String
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    //MARK:REPORT USER ACCOUNT SERVICE CALL
    func reportUserServiceCall()
    {
        // http://volive.in/voicek/api/services/report_account?API-KEY=225143&user_id=3783858425&profile_user_id=4182668577&report_id=2
        Services.sharedInstance.loader(view: self.view)
        let reportPost = "\(Base_Url)report_account?"
        print("Report Post URL Is :\(reportPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : postUserId! , "report_id" : reportIdStr!]
        
        print(parameters)
        
        Alamofire.request(reportPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension ReportUser : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return idArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : ReportCell
        cell = reportsListTable.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        cell.textLabel?.text = nameArray[indexPath.row]
        
//        let isRowChecked = rowsChecked.contains(indexPath)
//
//        if(isRowChecked == true)
//        {
//            cell.checkImage.image = UIImage.init(named: "CheckBoxGreen")
//            isChecked = true
//
//        }else{
//            cell.checkImage.image = UIImage.init(named: "UnCheckBox")
//            isChecked = false
//
//        }
        
        if checkArr[indexPath.row] == "0" {
            cell.checkImage.image = UIImage.init(named: "UnCheckBox")
        }
        else
        {
            cell.checkImage.image = UIImage.init(named: "CheckBoxGreen")
            reportIdStr = idArray[indexPath.row]
        }
        
        //cell.checkBtn.tag = indexPath.row
       // cell.checkBtn.addTarget(self, action: #selector(checkBtnClicked(sender:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let cell:ReportCell = reportsListTable.cellForRow(at: indexPath) as! ReportCell
        // cross checking for checked rows
//        if(rowsChecked.contains(indexPath) == false){
//            isChecked = true
//            rowsChecked.append(indexPath)
//        }else{
//            isChecked = false
//            // remove the indexPath from rowsWhichAreCheckedArray
//            if let checkedItemIndex = rowsChecked.index(of: indexPath){
//                rowsChecked.remove(at: checkedItemIndex)
//            }
//        }
        
        
        for i in 0..<self.checkArr.count
        {
            if i == indexPath.row
            {
                self.checkArr[i] = "1"

            } else {
                self.checkArr[i] = "0"
            }
        }

        DispatchQueue.main.async {
            self.reportsListTable.reloadData()
        }
        
    }
    
//    @objc func checkBtnClicked(sender : UIButton)
//    {
//        //let buttonRow = sender.tag
//
//    }

}
