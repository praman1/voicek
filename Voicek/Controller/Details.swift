//
//  Details.swift
//  Voicek
//
//  Created by Prashanth on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import KDCircularProgress
import AVFoundation

class Details: UIViewController ,AVAudioPlayerDelegate {

    @IBOutlet var playedLbl: UILabel!
    @IBOutlet var likesLbl: UILabel!
    @IBOutlet var commentsLbl: UILabel!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var timeAgoLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    
    @IBOutlet var showAllCommentsBtn: UIButton!
    @IBOutlet var detailsTable: UITableView!
    
    @IBOutlet var playPauseBtn: UIButton!
    @IBOutlet var playPauseView: KDCircularProgress!
    
    @IBOutlet var profileNameLabel: UILabel!
    @IBOutlet var likeUnlikeImage: UIImageView!
     var recordingSession: AVAudioSession!
    var postId : String!
    var postUserId : String!
    var ownerUserIdStr : String!
    var autoPlayString : String! = ""
    var audioDuration : String! = ""
    var audioUrlString : String! = ""
    var profileNameString : String! = ""
    
    var commentsListArr : [[String : AnyObject]]!
    var comment_idArr = [String]()
    var post_user_idArr = [String]()
    var user_nameArr = [String]()
    var commentsArr = [String]()
    var combineArr = [Any]()
    var timeString : String!
    var soundData : NSData?
 
    var circularProgressView: KDCircularProgress!
    var soundPlayer : AVAudioPlayer?
    var soundUrl : URL?
    
    var time : TimeInterval!
    var appDelegate : AppDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        profileNameLabel.text = profileNameString
        playPauseBtn.tag = 1
        circularProgressView = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        circularProgressView.trackColor = #colorLiteral(red: 0, green: 0.8980392157, blue: 0.6980392157, alpha: 1)
        circularProgressView.progressThickness = 0.2
        circularProgressView.trackThickness = 0.2
        
        playPauseView.addSubview(circularProgressView)
        
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        
        print("Auto Play Check String Is :\(autoPlayString!)")
        
        postImage.layer.cornerRadius = postImage.frame.size.height/2
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        let moreBtn = UIBarButtonItem(image: UIImage(named:"more2"), style: .plain, target: self, action: #selector(postBtnClicked))
        moreBtn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = moreBtn
        autoPlayString = UserDefaults.standard.object(forKey: "autoPlay") as? String
        // postDetailsServiceCall()
        if autoPlayString == "1" {
            var components = audioDuration.components(separatedBy: " ")
            self.timeString = components.removeFirst()
            self.time = (self.timeString as NSString).doubleValue
            print("Pushed Time Interval Is :\(self.time!)")
            self.soundUrl = URL(string: audioUrlString)
            print("Pushed Sound URL Is :\(self.soundUrl!)")
            self.AutoPlaySound()
        }else{
            print("Auto Play Is Not Enabled")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        postDetailsServiceCall()
        //viewDidLoad()
        descLbl.text = (UserDefaults.standard.object(forKey: "editDesc") as? String)
    }
    
    @objc func backBtnClicked()
    {
//        do{
//            recordingSession = AVAudioSession.sharedInstance()
//            try recordingSession.setCategory(AVAudioSessionCategoryPlayback)
//            try recordingSession.setActive(true)
//
//        }catch{
//            print("failed to Play")
//        }
        self.navigationController?.popViewController(animated: true)
    }
   
    func AutoPlaySound(){
        playPauseBtn.setImage(UIImage.init(named: "PauseWhite"), for: UIControlState.normal)
        preparePlayer()
        userPlayPostServiceCall()
        playPauseBtn.tag = 0
        circularProgressView.animate(fromAngle: 0, toAngle: 360, duration: time) { completed in
            if completed {
                self.playPauseBtn.tag = 1
                self.soundPlayer?.pause()
                self.playPauseBtn.setImage(UIImage.init(named: "PlayWhite"), for: UIControlState.normal)
                print("animation stopped, completed")
            } else {
                self.playPauseBtn.tag = 1
                self.circularProgressView.pauseAnimation()
                //self.soundPlayer?.pause()
                self.playPauseBtn.setImage(UIImage.init(named: "PlayWhite"), for: UIControlState.normal)
                print("animation stopped, was interrupted")
            }
        }
    }
    
    @IBAction func profileBtn(_ sender: Any) {
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = postUserId
        profile.fromSearchStr = "search"
        self.navigationController?.pushViewController(profile, animated: false)
    }
    
    @IBAction func playPause(_ sender: Any) {
        if playPauseBtn.tag == 1 {
            userPlayPostServiceCall()
            playPauseBtn.setImage(UIImage.init(named: "PauseWhite"), for: UIControlState.normal)
            preparePlayer()
            
            playPauseBtn.tag = 0
            circularProgressView.animate(fromAngle: 0, toAngle: 360, duration: time) { completed in
                if completed {
                    self.playPauseBtn.tag = 1
                    if (self.appDelegate?.soundPlayer?.isPlaying)!{
                        self.appDelegate?.soundPlayer?.pause()
                        do {
                            try self.appDelegate?.recordingSession!.setActive(false)
                        }catch{
                            
                        }
                    }
                    self.playPauseBtn.setImage(UIImage.init(named: "PlayWhite"), for: UIControlState.normal)
                    print("animation stopped, completed")
                } else {
                    self.playPauseBtn.tag = 1
                    self.circularProgressView.pauseAnimation()
                   // self.soundPlayer?.pause()
                    do {
                        try self.appDelegate?.recordingSession!.setActive(true)
                    }catch{
                    }
                    self.playPauseBtn.setImage(UIImage.init(named: "PlayWhite"), for: UIControlState.normal)
                    print("animation stopped, was interrupted")
                }
            }
        }
        else{
            circularProgressView.pauseAnimation()
            appDelegate?.soundPlayer?.pause()
            do {
            try appDelegate?.recordingSession!.setActive(false)
            }catch{
            }
            self.playPauseBtn.setImage(UIImage.init(named: "PlayWhite"), for: UIControlState.normal)
        }
    }
    
    func preparePlayer() {
        //DispatchQueue.main.async {
            let data = NSData(contentsOf: self.soundUrl!)
        appDelegate?.soundPlayer = try? AVAudioPlayer(data: data! as Data)
        self.appDelegate?.soundPlayer = try? AVAudioPlayer(data: data! as Data)
        self.appDelegate?.soundPlayer?.prepareToPlay()
        self.appDelegate?.soundPlayer?.delegate = self
        self.appDelegate?.soundPlayer?.play()
//            self.soundPlayer = try? AVAudioPlayer(data: data! as Data)
//            self.soundPlayer?.prepareToPlay()
//            self.soundPlayer?.delegate = self
//            self.soundPlayer?.play()
        
        appDelegate?.recordingSession = AVAudioSession.sharedInstance()
//        do {
//            try recordingSession.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            do{
               // let playbackSession = AVAudioSession.sharedInstance()
                try appDelegate?.recordingSession!.setCategory(AVAudioSessionCategoryPlayback)
                
                try appDelegate?.recordingSession!.setActive(true)
            
            }catch{
                print("failed to Play")
            }
//        } catch {
//            print("failed to record!")
//        }
        //}
    }
    
    //MARK:POST BUTTON CLICKED
    @objc func postBtnClicked()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let shareAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Share"), style: .default) { action -> Void in
        }
        let reportAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Report"), style: .default) { action -> Void in
            let reportUser = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportUser") as! ReportUser
            reportUser.ownerUserId = self.ownerUserIdStr
            reportUser.postIdStr = self.postId
            reportUser.reportCheckStr = "fromDetails"
            self.navigationController?.pushViewController(reportUser, animated: true)
        }
        let deleteAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Delete"), style: .default) { action -> Void in
            
            UserDefaults.standard.set(self.ownerUserIdStr, forKey: "userIdStr")
            UserDefaults.standard.set(self.postId, forKey: "postIdStr")
            
            let delete = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Delete")
            self .present(delete, animated: true, completion: nil)
         }
        let editAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Edit Description"), style: .default) { action -> Void in
            
          let edit = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditDescription") as! EditDescription
            edit.descString = self.descLbl.text
            edit.postIdStr = self.postId
            edit.userIdStr = self.postUserId
            self.navigationController?.pushViewController(edit, animated: true)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        // add actions
        if postUserId == ownerUserIdStr {
            actionSheetController.addAction(shareAction)
            actionSheetController.addAction(deleteAction)
            actionSheetController.addAction(editAction)
        }
        else{
            actionSheetController.addAction(shareAction)
            actionSheetController.addAction(reportAction)
        }
        actionSheetController.addAction(cancelAction)
      
        present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK:USER PLAY POST SERVICE CALL
    func userPlayPostServiceCall(){
        
            //http://volive.in/voicek/api/services/play_post?API-KEY=225143&user_id=3770410060&post_id=4
            //Services.sharedInstance.loader(view: self.view)
            let play_post = "\(Base_Url)play_post?"
            print("play_post URL Is :\(play_post)")
            
            let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "post_id" : postId!]
            
            print(parameters)
            
            Alamofire.request(play_post, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                if let responseData = response.result.value as? Dictionary<String, Any>{
                    print(responseData)
                    
                    let status = responseData["status"] as! Int
                    
                    if status == 1
                    {
                        //Services.sharedInstance.dissMissLoader()
                        let count : Int = responseData["played_count"] as! Int
                        let xNSNumber1 = count as NSNumber
                        let countNo : String = xNSNumber1.stringValue
                        
                        print(countNo)
                        
                        self.playedLbl.text = countNo
                        //self.postDetailsServiceCall()
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let message = responseData["message"] as! String
                            self.showToastForError(message: message)
                            //Services.sharedInstance.dissMissLoader()
                        }
                    }
                }
            }
    }
    
    //MARK:POST DETAILS SERVICE CALL
    func postDetailsServiceCall()
    {
        // http://volive.in/voicek/api/services/post_details?API-KEY=225143&user_id=3783858425&id=1
        
        Services.sharedInstance.loader(view: self.view)
        
        let postDetails = "\(Base_Url)post_details?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id":ownerUserIdStr! , "id":postId!]
        
        print(parameters)
        
        Alamofire.request(postDetails, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let responseData = response.result.value as? Dictionary<String, Any>
            print(responseData!)
            
            let status = responseData!["status"] as! Int
            
            if status == 1
            {
                    Services.sharedInstance.dissMissLoader()
               
                if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                    
                    let postId = responseData1["id"] as! String
                    let userId = responseData1["user_id"] as! String
                    let image = responseData1["image"] as! String
                    let audio = responseData1["audio"] as! String
                    self.soundUrl = URL(string: audio)
                    print("Sound URL Is :\(self.soundUrl!)")
//                    self.soundData =  NSData(contentsOf: self.soundUrl!)
//                    print("soundData Is :\(self.soundData!)")
                    
                    let duration = responseData1["duration"] as! String
                    
                    var components = duration.components(separatedBy: " ")
                    self.timeString = components.removeFirst()
                    self.time = (self.timeString as NSString).doubleValue
                    print("Time Interval Is :\(self.time!)")
                    //self.PlaySound()
                    
                    let description = responseData1["description"] as! String
                    let name = responseData1["name"] as! String
                    let is_public = responseData1["is_public"] as! String
                    
                    let x : Int = responseData1["played"] as! Int
                    let xNSNumber = x as NSNumber
                    let played : String = xNSNumber.stringValue
                    
                    let x1 : Int = responseData1["likes"] as! Int
                    let xNSNumber1 = x1 as NSNumber
                    let likes : String = xNSNumber1.stringValue
                    
                    if likes == "0"{
                        self.likeUnlikeImage.image = UIImage.init(named: "UnLike")
                    }else{
                        self.likeUnlikeImage.image = UIImage.init(named: "Like")
                    }
                    
                    let x2 : Int = responseData1["comments"] as! Int
                    let xNSNumber2 = x2 as NSNumber
                    let comments : String = xNSNumber2.stringValue
                    
                    if x2 > 3
                    {
                        self.showAllCommentsBtn.isHidden = false
                        self.showAllCommentsBtn.setTitle(String(format: "%@ %@ %@",self.languageChangeString(a_str: "Show all")!, comments,self.languageChangeString(a_str: "comments")!), for: UIControlState.normal)
                    }
                    else{
                        self.showAllCommentsBtn.isHidden = true
                    }
                    
                    //let comments = responseData1["comments"] as! String
                    let time_ago = responseData1["time_ago"] as! String
                    
                    self.postImage.sd_setImage(with: URL (string: image), placeholderImage: UIImage(named: "Logo"))
                    self.nameLbl.text = name
                    self.timeAgoLbl.text = time_ago
                    self.playedLbl.text = played
                    self.likesLbl.text = likes
                    self.commentsLbl.text = comments
                    self.descLbl.text = description
                    
                    print(postId)
                    print(userId)
                    print(audio)
                    print(duration)
                    print(is_public)
                    
                    self.commentsListArr = responseData1["comments_list"] as? [[String:AnyObject]]
                    
                    self.comment_idArr = [String]()
                    self.post_user_idArr = [String]()
                    self.user_nameArr = [String]()
                    self.commentsArr = [String]()
                    self.combineArr = [Any]()
                    
                    print(self.commentsListArr)
                    let count = self.commentsListArr.count
                    print("Array Count Is: \(count)")
                    
                    for each in self.commentsListArr
                    {
                        let comment_id = each["comment_id"] as? String
                        let post_user_id = each["post_user_id"] as? String
                        let user_name = each["user_name"] as? String
                        let comment = each["comment"] as? String
                        
                        let twoWords = String(format: "@%@ %@", user_name!,comment!)
                        var components = twoWords.components(separatedBy: " ")
                        let blueRange: NSRange = (twoWords as NSString).range(of: components[0])
                        let blackRange: NSRange = (twoWords as NSString).range(of: components[1])
                        
                        let attrString = NSMutableAttributedString(string: twoWords)
                        
                        attrString.beginEditing()
                        attrString.addAttribute(.foregroundColor, value: UIColor.init(red: 11.0/255.0, green: 235.0/255.0, blue: 243.0/255.0, alpha: 1.0), range: blueRange)
                        
                        attrString.addAttribute(.foregroundColor, value: UIColor.black, range: blackRange)
                        
                        attrString.endEditing()
                        
                        self.combineArr.append(attrString)
                        self.comment_idArr.append(comment_id!)
                        self.post_user_idArr.append(post_user_id!)
                        self.user_nameArr.append(user_name!)
                        self.commentsArr.append(comment!)
                    }
                    DispatchQueue.main.async {
                        Services.sharedInstance.dissMissLoader()
                        self.detailsTable.reloadData()
                        
                        //self.preparePlayer()
                    }
                    //self.PlaySound()
                }
            }
            else
            {
                let message = responseData!["message"] as! String
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    @IBAction func showAllComments(_ sender: Any) {
        let comments = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Comments") as! Comments
        comments.postIdStr = self.postId
        comments.userIdStr = self.postUserId
        self.navigationController?.pushViewController(comments, animated: true)
    }
    
    @IBAction func playListBtn(_ sender: Any) {
        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Plays") as! Plays
        play.postIdString = self.postId
        self.navigationController?.pushViewController(play, animated: true)
    }
    
    @IBAction func likesBtn(_ sender: Any) {
        if ownerUserIdStr != postUserId {
            likeOrUnlikeServiceCall()
        }else{
            self.showToastForError(message: languageChangeString(a_str: "You're not allowed to like your own post")!)
        }
    }
    
    //MARK:LIKE OR UNLIKE SERVICE CALL
    func likeOrUnlikeServiceCall()
    {
        // http://volive.in/voicek/api/services/like_unlike?API-KEY=225143&user_id=3783858425&post_id=5
        Services.sharedInstance.loader(view: self.view)
        let like_unlike = "\(Base_Url)like_unlike?"
        print("like_unlike URL Is :\(like_unlike)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "post_id" : postId!]
        
        print(parameters)
        
        Alamofire.request(like_unlike, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    let count : Int = responseData["count"] as! Int
                    let xNSNumber1 = count as NSNumber
                    let countNo : String = xNSNumber1.stringValue
                    self.likesLbl.text = countNo
                    
                    let like_unlike : Int = responseData["like_unlike"] as! Int
                    let xNSNumber2 = like_unlike as NSNumber
                    let likeUnlike : String = xNSNumber2.stringValue
                    
                    if likeUnlike == "1"{
                        if countNo == "0"{
                            self.likeUnlikeImage.image = UIImage.init(named: "UnLike")
                        }else{
                            self.likeUnlikeImage.image = UIImage.init(named: "Like")
                        }
                    }
                    else if likeUnlike == "2"{
                        if countNo == "0"{
                            self.likeUnlikeImage.image = UIImage.init(named: "UnLike")
                        }else{
                            self.likeUnlikeImage.image = UIImage.init(named: "Like")
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @IBAction func commentsBtn(_ sender: Any) {
        let comments = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Comments") as! Comments
        comments.postIdStr = self.postId
        comments.userIdStr = self.postUserId
        self.navigationController?.pushViewController(comments, animated: true)
    }
}

extension Details: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return comment_idArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : DetailsCell
        
        cell = detailsTable.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
        cell.textLabel?.attributedText = combineArr[indexPath.row] as? NSAttributedString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.detailsTable.frame.size.height/3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = post_user_idArr[indexPath.row]
        profile.checkString = "toProfile"
        self.navigationController?.pushViewController(profile, animated: false)
    }
}
