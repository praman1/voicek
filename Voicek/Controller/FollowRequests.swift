//
//  FollowRequests.swift
//  Voicek
//
//  Created by volivesolutions on 24/09/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class FollowRequests: UIViewController {

    @IBOutlet weak var followReqTable: UITableView!
    var testStr : String!
    
    var ownerUserIdStr : String!
    
    var userIdStr : String!
    var followerIdStr : String!
    var requestTypeStr : String!
    
    var user_idArray = [String]()
    var follower_idArray = [String]()
    var nameArray = [String]()
    var user_nameArray = [String]()
    var is_publicArray = [String]()
    var imageArray = [String]()
    
    var follow_requestsData : [[String : AnyObject]]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = languageChangeString(a_str: "Follow Requests")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        followRequestsServiceCall()

        // Do any additional setup after loading the view.
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:FOLLOW REQUESTS SERVICE CALL
    func followRequestsServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/follow_requests?API-KEY=225143&user_id=3783858425
        Services.sharedInstance.loader(view: self.view)
        let follow_requests = "\(Base_Url)follow_requests?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(follow_requests, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    
                    self.user_idArray = [String]()
                    self.follower_idArray = [String]()
                    self.nameArray = [String]()
                    self.user_nameArray = [String]()
                    self.is_publicArray = [String]()
                    self.imageArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.follow_requestsData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.follow_requestsData
                    {
                        let user_id = each["user_id"] as? String
                        let follower_id = each["follower_id"] as? String
                        let name = each["name"] as? String
                        let user_name = each["user_name"] as? String
                        let is_public = each["is_public"] as? String
                        let image = each["image"] as? String
 
                        self.user_idArray.append(user_id!)
                        self.follower_idArray.append(follower_id!)
                        self.nameArray.append(name!)
                        self.user_nameArray.append(user_name!)
                        self.is_publicArray.append(is_public!)
                        self.imageArray.append(image!)
                    }
                    print("Image Array Is:\(self.imageArray)")
                    DispatchQueue.main.async {
                        self.followReqTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.user_idArray = [String]()
                        self.followReqTable.reloadData()
                        let message = responseData["message"] as! String
                        if self.testStr == "1"{
                            self.showToastForError(message: message)
                        }
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }

    @objc func acceptClicked(sender:UIButton)
    {
        let buttonRow = sender.tag
        userIdStr = user_idArray[buttonRow]
        followerIdStr = follower_idArray[buttonRow]
        requestTypeStr = "1"
        self.acceptOrRejectServiceCall()
    }
    
    @objc func rejectClicked(sender:UIButton)
    {
        let buttonRow = sender.tag
        userIdStr = user_idArray[buttonRow]
        followerIdStr = follower_idArray[buttonRow]
        requestTypeStr = "2"
        self.acceptOrRejectServiceCall()
    }
    
    //MARK:ACCEPT OR REJECT SERVICE CALL
    func acceptOrRejectServiceCall()
    {
        // http://volive.in/voicek/api/services/accept_reject?API-KEY=225143&user_id=3770410060&follower_id=4182668577&request_type=1
        Services.sharedInstance.loader(view: self.view)
        let reportPost = "\(Base_Url)accept_reject?"
        print("accept_reject URL Is :\(reportPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : userIdStr! , "follower_id" : followerIdStr! , "request_type" : requestTypeStr!]
        
        print(parameters)
        
        Alamofire.request(reportPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    
                    self.showToastForAlert(message: message)
                    self.testStr = "0"
                    self.followRequestsServiceCall()
                    //self.viewDidLoad()
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension FollowRequests: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return user_idArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : FollowReq
    
            cell = followReqTable.dequeueReusableCell(withIdentifier: "FollowReq", for: indexPath) as! FollowReq
            cell.reqImage.layer.cornerRadius = cell.reqImage.frame.size.height/2
            cell.usernameLbl.text = user_nameArray[indexPath.row]
            cell.nameLbl.text = nameArray[indexPath.row]
            cell.reqImage.sd_setImage(with: URL (string: imageArray[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
            cell.acceptBtn.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
            cell.rejectBtn.setTitle(languageChangeString(a_str: "Reject"), for: UIControlState.normal)
            cell.acceptBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.acceptBtn.addTarget(self, action: #selector(acceptClicked(sender:)), for: UIControlEvents.touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(rejectClicked(sender:)), for: UIControlEvents.touchUpInside)
        
            return cell
        
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}


//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
//        self.navigationController?.pushViewController(play, animated: true)
//
//    }

