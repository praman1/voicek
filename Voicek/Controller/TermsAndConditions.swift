//
//  TermsAndConditions.swift
//  Voicek
//
//  Created by Suman Guntuka on 20/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class TermsAndConditions: UIViewController {

    @IBOutlet weak var termsWeb: UIWebView!
    
    var myUrl : NSURL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = languageChangeString(a_str: "Terms and Conditions")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        let url = URL (string: "http://voicek-480f4.firebaseapp.com/termsAndConditions.html")
        let requestObj = URLRequest(url: url!)
        termsWeb.loadRequest(requestObj)
        
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
