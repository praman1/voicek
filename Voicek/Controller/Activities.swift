//
//  Activities.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire

class Activities: UIViewController {

    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet var follow: UIButton!
    @IBOutlet weak var activity: UIButton!
    @IBOutlet weak var likes: UIButton!
    
    @IBOutlet var comments: UIButton!
    @IBOutlet var followView: UIView!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet var commentsView: UIView!
    
    @IBOutlet weak var reviewBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var btnHeight: NSLayoutConstraint!
    var sepStr : String!
    var ownerUserIdStr : String!
    
    //Followers Arrays
    var followUserImageArray = [String]()
    var followUserNameArray = [String]()
    var followNameArray = [String]()
    var followImageArray = [String]()
    var followTimeAgoArray = [String]()
    var followIsPublicArray = [String]()
    var followTypeArray = [String]()
    var followNotiCountArray = [String]()
    var followUserIdArray = [String]()
    
    //Likes Arrays
    var likeUserImageArray = [String]()
    var likeUserNameArray = [String]()
    var likeNameArray = [String]()
    var likeImageArray = [String]()
    var likeTimeAgoArray = [String]()
    var likeIsPublicArray = [String]()
    var likeTypeArray = [String]()
    var likeNotiCountArray = [String]()
    var likePostIdArray = [String]()
    var likeUserIdArray = [String]()
    
    //Comments Arrays
    var commentsUserImageArray = [String]()
    var commentsUserNameArray = [String]()
    var commentsNameArray = [String]()
    var commentsImageArray = [String]()
    var commentsTimeAgoArray = [String]()
    var commentsIsPublicArray = [String]()
    var commentsTypeArray = [String]()
    var commentsNotiCountArray = [String]()
    var commentsPostIdArray = [String]()
    var commentsUserIdArray = [String]()
    var commentArray = [String]()
    
    
//    var time_agoArray = [String]()
//    var activityNameArray = [String]()
//    var user_nameArray = [String]()
//    var is_publicArray = [String]()
//    var userImageArray = [String]()
//    var imageArray = [String]()
//    var typeArray = [String]()
//    var notiCountArr = [String]()
//
//    var likesNameArray = [String]()
//    var postIdArr = [String]()
//    var userIdArr = [String]()
//    var audioArr = [String]()
//    var durationArr = [String]()
//    var descArr = [String]()
//    var playedArr = [String]()
//    var likesArr = [String]()
    
    var isPublicStr : String!
    var refreshControl: UIRefreshControl?
    
    var followersData : [[String : AnyObject]]!
    var commentsData : [[String : AnyObject]]!
    var likesData : [[String : AnyObject]]!
    
    @IBOutlet weak var activityTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isPublicStr = (UserDefaults.standard.object(forKey: "is_public") as! String)
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        likeView.isHidden = true
        commentsView.isHidden = true
        likes.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        comments.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        
        self.navigationItem.title = languageChangeString(a_str: "Activities")
        let backBtn = UIBarButtonItem(image: UIImage(named:"MenuWhite"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor(red: 0.0 / 255.0, green: 226.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
        activityTable.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(self.refreshTable), for: .valueChanged)
       
        sepStr = "follow"
        MyFollowersServiceCall()
        //activityTable.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadtabledata), name: NSNotification.Name("reloadTable"), object: nil)
        self.follow.setTitle(languageChangeString(a_str: "Followers"), for: UIControlState.normal)
        self.likes.setTitle(languageChangeString(a_str: "Likes"), for: UIControlState.normal)
        self.comments.setTitle(languageChangeString(a_str: "Comments"), for: UIControlState.normal)
        self.reviewBtn.setTitle(languageChangeString(a_str: "Please review follow requests"), for: UIControlState.normal)
   
    }
    
    @objc func refreshTable() {
        DispatchQueue.main.async(execute: {
            self.refreshControl?.endRefreshing()
            self.activityTable.reloadData()
        })
    }
    
    @objc func reloadtabledata()
    {
        MyFollowersServiceCall()
    }
    
    @objc func backBtnClicked()
    {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    @IBAction func review(_ sender: Any) {
        
        let reviewReq = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FollowRequests") as! FollowRequests
        reviewReq.testStr = "1"
        self.navigationController?.pushViewController(reviewReq, animated: true)
    }
    
    @IBAction func followingsBtn(_ sender: Any) {
        sepStr = "follow"
        
        followView.isHidden = false
        likeView.isHidden = true
        commentsView.isHidden = true
        
        activityTable.isHidden = false
        
        follow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
        likes.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        comments.setTitleColor(#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1), for: UIControlState.normal)
        MyFollowersServiceCall()
    }
    
    
    @IBAction func likesBtn(_ sender: Any) {
        
        sepStr = "likes"
        
        likeView.isHidden = false
        followView.isHidden = true
        commentsView.isHidden = true
        activityTable.isHidden = false
        
        likes.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
        follow.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        comments.setTitleColor(#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1), for: UIControlState.normal)
        mylikesServiceCall()
        
//        img.image = #imageLiteral(resourceName: "TestImages")
//
//        titleLbl.text = "No Likes Yet"
//        descLbl.text = "Wait? You havent liked any posts yet? Make someone's day happy!"
    }
    
    @IBAction func commentsBtn(_ sender: Any) {
        
        sepStr = "comments"
        commentsView.isHidden = false
        likeView.isHidden = true
        followView.isHidden = true
        activityTable.isHidden = false
        comments.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
        likes.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        follow.setTitleColor(#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1), for: UIControlState.normal)
        myCommentsServiceCall()
    }
    
    //MARK:MY FOLLOWERS SERVICE CALL
    func MyFollowersServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/my_activities?API-KEY=225143&user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let my_activities = "\(Base_Url)my_activities?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(my_activities, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                let x : Int = responseData["count"] as! Int
                let xNSNumber = x as NSNumber
                let countNo : String = xNSNumber.stringValue
                print("Count Is***:\(countNo)")
                if countNo.count < 1{
                    self.activityTable.isHidden = true
                    self.img.image = #imageLiteral(resourceName: "BeatGray")
                    self.titleLbl.text = "No current Activities"
                    self.descLbl.text = "You don't have any activities yet. Let the world know what you're up to!"
                }
                
                if self.isPublicStr == "2" && (countNo.count) > 0{
                    self.reviewBtn.isHidden = false
                    self.btnHeight.constant = 60
                    self.profileImage.isHidden = false
                    self.profileImage.sd_setImage(with: URL (string: UserDefaults.standard.object(forKey: "image") as! String ), placeholderImage:
                        UIImage(named: "Logo"))
                }
                else{
                    self.reviewBtn.isHidden = true
                    self.btnHeight.constant = 0
                    self.profileImage.isHidden = true
                }
                if status == 1
                {
                    self.followUserImageArray = [String]()
                    self.followUserNameArray = [String]()
                    self.followNameArray = [String]()
                    self.followImageArray = [String]()
                    self.followTimeAgoArray = [String]()
                    self.followTypeArray = [String]()
                    self.followIsPublicArray = [String]()
                    self.followNotiCountArray = [String]()
                    self.followUserIdArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.followersData = responseData["followers"] as? [[String:AnyObject]]
                    
                    for each in self.followersData
                    {
                        let time_ago = each["time_ago"] as? String
                        let name = each["name"] as? String
                        let user_name = each["user_name"] as? String
                        let user_image = each["user_image"] as? String
                        let image = each["image"] as? String
                        let is_public = each["is_public"] as? String
                        let user_id = each["user_id"] as? String
                        let x1 : Int = each["type"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let type : String = xNSNumber1.stringValue
                        
                        let x3 : Int = each["noti_count"] as! Int
                        let xNSNumber3 = x3 as NSNumber
                        let noti_count : String = xNSNumber3.stringValue
                        
                        self.followUserImageArray.append(user_image!)
                        self.followUserNameArray.append(user_name!)
                        self.followUserIdArray.append(user_id!)
                        self.followNameArray.append(name!)
                        self.followImageArray.append(image!)
                        self.followTimeAgoArray.append(time_ago!)
                        self.followIsPublicArray.append(is_public!)
                        self.followTypeArray.append(type)
                        self.followNotiCountArray.append(noti_count)
                        
                    }
                    DispatchQueue.main.async {
                        
                        self.activityTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        //self.activityTable.reloadData()
                        self.activityTable.isHidden = true
                        self.img.image = #imageLiteral(resourceName: "BeatGray")
                        self.titleLbl.text = "No current Activities"
                        self.descLbl.text = "You don't have any activities yet. Let the world know what you're up to!"
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    //MARK:MY LIKES SERVICE CALL
    func mylikesServiceCall()
    {
        
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/my_activities?API-KEY=225143&user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let my_likes = "\(Base_Url)my_activities?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(my_likes, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print("my_likes data Is***:\(responseData)")
                self.reviewBtn.isHidden = true
                self.btnHeight.constant = 0
                self.profileImage.isHidden = true
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {

                    self.likeUserImageArray = [String]()
                    self.likeUserNameArray = [String]()
                    self.likeNameArray = [String]()
                    self.likeImageArray = [String]()
                    self.likeTimeAgoArray = [String]()
                    self.likeIsPublicArray = [String]()
                    self.likeTypeArray = [String]()
                    self.likeNotiCountArray = [String]()
                    self.likePostIdArray = [String]()
                    self.likeUserIdArray = [String]()
                   
                    Services.sharedInstance.dissMissLoader()
                    
                    self.likesData = responseData["likes"] as? [[String:AnyObject]]
                    
                    if self.likesData.count < 0{
                        self.activityTable.isHidden = true
                        self.img.image = #imageLiteral(resourceName: "TestImages")
                        self.titleLbl.text = "No Likes Yet"
                        self.descLbl.text = "Wait? You havent liked any posts yet? Make someone's day happy!"
                    }
 
                    for each in self.likesData
                    {
                        let postId = each["post_id"] as? String
                        let user_id = each["user_id"] as? String
                        let image = each["image"] as? String
                        let user_image = each["user_image"] as? String
                        let name = each["name"] as? String
                        let is_public = each["is_public"] as? String
                        let time_ago = each["time_ago"] as? String
                        
                        let x3 : Int = each["noti_count"] as! Int
                        let xNSNumber3 = x3 as NSNumber
                        let noti_count : String = xNSNumber3.stringValue
                        
                        self.likePostIdArray.append(postId!)
                        self.likeUserIdArray.append(user_id!)
                        self.likeImageArray.append(image!)
                        self.likeUserImageArray.append(user_image!)
                        self.likeNameArray.append(name!)
                        self.likeIsPublicArray.append(is_public!)
                        self.likeTimeAgoArray.append(time_ago!)
                        self.likeNotiCountArray.append(noti_count)
  
                    }
                        self.activityTable.reloadData()
                }
                else
                {
                    DispatchQueue.main.async {
                        //self.activityTable.reloadData()
                        self.activityTable.isHidden = true
                        self.img.image = #imageLiteral(resourceName: "hearttt3")
                        self.titleLbl.text = "No Likes Yet"
                        self.descLbl.text = "Wait? You haven't liked any posts yet? Make someone's day happy!"
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    //MARK:MY COMMENTS SERVICE CALL
    func myCommentsServiceCall()
    {
        
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/my_activities?API-KEY=225143&user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let my_likes = "\(Base_Url)my_activities?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(my_likes, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print("my_likes data Is***:\(responseData)")
                self.reviewBtn.isHidden = true
                self.btnHeight.constant = 0
                self.profileImage.isHidden = true
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    
                    self.likeUserImageArray = [String]()
                    self.likeUserNameArray = [String]()
                    self.likeNameArray = [String]()
                    self.likeImageArray = [String]()
                    self.likeTimeAgoArray = [String]()
                    self.likeIsPublicArray = [String]()
                    self.likeTypeArray = [String]()
                    self.likeNotiCountArray = [String]()
                    self.likePostIdArray = [String]()
                    self.likeUserIdArray = [String]()
                    self.commentArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.commentsData = responseData["comments"] as? [[String:AnyObject]]
                    
                    if self.commentsData.count < 0{
                        self.activityTable.isHidden = true
                        self.img.image = #imageLiteral(resourceName: "TestImages")
                        self.titleLbl.text = "No Likes Yet"
                        self.descLbl.text = "Wait? You havent liked any posts yet? Make someone's day happy!"
                    }
                    
                    for each in self.commentsData
                    {
                        let postId = each["post_id"] as? String
                        let user_id = each["user_id"] as? String
                        let image = each["image"] as? String
                        let user_image = each["user_image"] as? String
                        let name = each["name"] as? String
                        let is_public = each["is_public"] as? String
                        let time_ago = each["time_ago"] as? String
                        let comment = each["comment"] as? String
                        let x3 : Int = each["noti_count"] as! Int
                        let xNSNumber3 = x3 as NSNumber
                        let noti_count : String = xNSNumber3.stringValue
                        
                        self.commentsPostIdArray.append(postId!)
                        self.commentsUserIdArray.append(user_id!)
                        self.commentsImageArray.append(image!)
                        self.commentsUserImageArray.append(user_image!)
                        self.commentsNameArray.append(name!)
                        self.commentsIsPublicArray.append(is_public!)
                        self.commentsTimeAgoArray.append(time_ago!)
                        self.commentArray.append(comment!)
                        self.commentsNotiCountArray.append(noti_count)
                        
                    }
                    self.activityTable.reloadData()
                }
                else
                {
                    DispatchQueue.main.async {
                        //self.activityTable.reloadData()
                        self.activityTable.isHidden = true
                        self.img.image = #imageLiteral(resourceName: "hearttt3")
                        self.titleLbl.text = "No Likes Yet"
                        self.descLbl.text = "Wait? You haven't liked any posts yet? Make someone's day happy!"
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}

extension Activities: UITableViewDataSource , UITableViewDelegate{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if sepStr == "follow" {
                return followNameArray.count
            }else if sepStr == "likes"{
                return likeNameArray.count
            }else{
                return commentsNameArray.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            var  cell : HomeCell
            //var cell1 : HomeCell
            
            if sepStr == "follow" {
               
                cell = activityTable.dequeueReusableCell(withIdentifier: "ActivityCell", for: indexPath) as! HomeCell
                cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
                cell.activityBadgeLabel.layer.cornerRadius = cell.activityBadgeLabel.frame.size.height/2
                
                let x2 : Int = Int(self.followNotiCountArray[indexPath.row])!
                let xNSNumber2 = x2 as NSNumber
                let noti_count : String = xNSNumber2.stringValue
                
                if x2 > 0{
                    cell.activityBadgeLabel.isHidden = false
                }else{
                    cell.activityBadgeLabel.isHidden = true
                }
                cell.activityBadgeLabel.text = noti_count
                
                cell.nameLbl.text = followNameArray[indexPath.row]
                cell.homeImg.sd_setImage(with: URL (string: followUserImageArray[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
                let imageStr = followImageArray[indexPath.row]
                cell.postImage.sd_setImage(with: URL (string: imageStr), placeholderImage: UIImage(named: "Logo"))
                cell.subNameLbl.text = followTimeAgoArray[indexPath.row]
                cell.typeLabel.text = "following you"
//                if followTypeArray[indexPath.row] == "1"
//                {
//                    cell.typeLabel.text = "following you"
//                }else if followTypeArray[indexPath.row] == "2"{
//                    cell.typeLabel.text = "liked your post"
//                }else if followTypeArray[indexPath.row] == "3"{
//                    cell.typeLabel.text = "commented on your post"
//                }
                
                return cell
               
            }else if sepStr == "likes"
            {
                cell = activityTable.dequeueReusableCell(withIdentifier: "ActivityCell", for: indexPath) as! HomeCell
                cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
                cell.activityBadgeLabel.layer.cornerRadius = cell.activityBadgeLabel.frame.size.height/2
                
                let x2 : Int = Int(self.likeNotiCountArray[indexPath.row])!
                let xNSNumber2 = x2 as NSNumber
                let noti_count : String = xNSNumber2.stringValue
                
                if x2 > 0{
                    cell.activityBadgeLabel.isHidden = false
                }else{
                    cell.activityBadgeLabel.isHidden = true
                }
                cell.activityBadgeLabel.text = noti_count
                
                cell.nameLbl.text = likeNameArray[indexPath.row]
                cell.homeImg.sd_setImage(with: URL (string: likeUserImageArray[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
                let imageStr = likeImageArray[indexPath.row]
                cell.postImage.sd_setImage(with: URL (string: imageStr), placeholderImage: UIImage(named: "Logo"))
                cell.subNameLbl.text = likeTimeAgoArray[indexPath.row]
                cell.typeLabel.text = "liked your post"
//                if typeArray[indexPath.row] == "1"
//                {
//                    cell.typeLabel.text = "following you"
//                }else if typeArray[indexPath.row] == "2"{
//                    cell.typeLabel.text = "liked your post"
//                }else if typeArray[indexPath.row] == "3"{
//                    cell.typeLabel.text = "commented on your post"
//                }
                return cell
            }else{
                cell = activityTable.dequeueReusableCell(withIdentifier: "ActivityCell", for: indexPath) as! HomeCell
                cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
                cell.activityBadgeLabel.layer.cornerRadius = cell.activityBadgeLabel.frame.size.height/2
                
                let x2 : Int = Int(self.commentsNotiCountArray[indexPath.row])!
                let xNSNumber2 = x2 as NSNumber
                let noti_count : String = xNSNumber2.stringValue
                
                if x2 > 0{
                    cell.activityBadgeLabel.isHidden = false
                }else{
                    cell.activityBadgeLabel.isHidden = true
                }
                cell.activityBadgeLabel.text = noti_count
                
                cell.nameLbl.text = commentsNameArray[indexPath.row]
                cell.homeImg.sd_setImage(with: URL (string: commentsUserImageArray[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
                let imageStr = commentsImageArray[indexPath.row]
                cell.postImage.sd_setImage(with: URL (string: imageStr), placeholderImage: UIImage(named: "Logo"))
                cell.subNameLbl.text = commentsTimeAgoArray[indexPath.row]
                cell.typeLabel.text = "commented on your post"
//                if typeArray[indexPath.row] == "1"
//                {
//                    cell.typeLabel.text = "following you"
//                }else if typeArray[indexPath.row] == "2"{
//                    cell.typeLabel.text = "liked your post"
//                }else if typeArray[indexPath.row] == "3"{
//                    cell.typeLabel.text = "commented on your post"
//                }
                return cell
            }
        }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if sepStr == "follow" {
                let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
                profile.postUserIdStr =  followUserIdArray[indexPath.row]
                self.navigationController?.pushViewController(profile, animated: true)
            }
            else if sepStr == "comments" {
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = commentsPostIdArray[indexPath.row]
                self.navigationController?.pushViewController(play, animated: true)
            }
            else if sepStr == "likes" {
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = likePostIdArray[indexPath.row]
                self.navigationController?.pushViewController(play, animated: true)
            }
        }
    }
    

