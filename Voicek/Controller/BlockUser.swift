//
//  BlockUser.swift
//  Voicek
//
//  Created by Suman Guntuka on 24/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class BlockUser: UIViewController {

    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var accept: UIButton!
    @IBOutlet weak var deny: UIButton!
    
    var ownerUserId : String! = ""
    var postUserId : String! = ""
    var checkStr : String! = ""
    var blockStr : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        ownerUserId = UserDefaults.standard.object(forKey: "ownerUserId") as? String
        postUserId = UserDefaults.standard.object(forKey: "postUserId") as? String
        self.textLbl.text = languageChangeString(a_str: "Are you sure you want to block this user?")
        self.accept.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
        self.deny.setTitle(languageChangeString(a_str: "Reject"), for: UIControlState.normal)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptBtn(_ sender: Any) {
        blockUserServiceCall()
    }
    
    @IBAction func denyBtn(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:BLOCK USER SERVICE CALL
    func blockUserServiceCall()
    {
        // http://volive.in/voicek/api/services/block_user?API-KEY=225143&user_id=3783858425&profile_user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let block_user = "\(Base_Url)block_user?"
        print("block_user URL Is :\(block_user)")
       
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! , "profile_user_id" : postUserId!]
        
        print(parameters)
        
        Alamofire.request(block_user, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
               
                
                if status == 1
                {
                   // let blocked = responseData["blocked"] as! Int
                    
                    let x : Int = responseData["blocked"] as! Int
                    let xNSNumber = x as NSNumber
                    self.blockStr  = xNSNumber.stringValue
                    
                    if self.blockStr == "1"
                    {
                        self.blockStr = "Unblock"
                        
                    }else if self.blockStr == "2"
                    {
                         self.blockStr = "Block"
                    }
                    
                    UserDefaults.standard.set(self.blockStr, forKey: "Blocked")
                    NotificationCenter.default.post(name: Notification.Name("Blocked"), object: nil)
                    
                    Services.sharedInstance.dissMissLoader()
                    self.dismiss(animated: true, completion: nil)
                    self.showToastForAlert(message: message)
                }
                else
                {
                    DispatchQueue.main.async {
                         self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}
