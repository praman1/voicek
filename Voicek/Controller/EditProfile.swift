//
//  EditProfile.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class EditProfile: UIViewController {
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var setPrivateLabel: UILabel!
    @IBOutlet var setPrivateTextView: UITextView!
    @IBOutlet var mobileNumberLabel: UILabel!
    @IBOutlet var bioLabel: UILabel!
    @IBOutlet var saveBtn: UIButton!
    
    
    
    @IBOutlet weak var firstnameTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    
    @IBOutlet weak var biodata: UITextView!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var privateSwitch: UISwitch!
    var tokenString : String!
    var userIdStr : String?
    
    var pickerImage  = UIImage()
    var pickerImage1  = UIImage()
    var pickedImage  = UIImage()
    var isPublicString : String!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        self.navigationItem.title = languageChangeString(a_str: "Edit Profile")

        let colors = [(UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)), (UIColor(red: 0.0 / 255.0, green: 223.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0))]
        self.navigationController?.navigationBar.gradeintBarTintColor(with: colors)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        userIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        
        firstnameTF.text = (UserDefaults.standard.object(forKey: "first_name") as! String)
        userNameTF.text = (UserDefaults.standard.object(forKey: "userName") as! String)
        mobileNumberTF.text = (UserDefaults.standard.object(forKey: "mobile") as! String)
        self.biodata.text = (UserDefaults.standard.object(forKey: "bio") as! String)
 
        DispatchQueue.main.async {
            self.profileImage.sd_setImage(with: URL (string: UserDefaults.standard.object(forKey: "image") as! String ), placeholderImage:
                UIImage(named: "userrr"))
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        
        isPublicString = UserDefaults.standard.object(forKey: "is_public") as! String
        if isPublicString == "2" {
            privateSwitch.isOn = true
        }
        else{
            privateSwitch.isOn = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillAppear(_:)), name: Notification.Name("isPublic"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        isPublicString = UserDefaults.standard.object(forKey: "is_public") as! String
        if isPublicString == "2" {
            privateSwitch.isOn = true
        }
        else{
             privateSwitch.isOn = false
        }
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        
        editProfileServiceCall()
    }
    
    func editProfileServiceCall()
    {
       
        //tokenString = UserDefaults.standard.object(forKey: "deviceToken") as! String
        Services.sharedInstance.loader(view: self.view)
        //http://volive.in/voicek/api/services/update_profile

        let parameters: Dictionary<String, Any>
        if privateSwitch.isOn == true {
             parameters = [ "API-KEY" :APIKEY, "user_id":userIdStr! , "first_name":self.firstnameTF.text!, "username":self.userNameTF.text!,"is_public":"2",   "mobile":self.mobileNumberTF.text!, "bio":self.biodata.text!]
        }
        else{
           parameters = [ "API-KEY" :APIKEY, "user_id":userIdStr! , "first_name":self.firstnameTF.text!, "username":self.userNameTF.text!,"is_public":"1",   "mobile":self.mobileNumberTF.text!, "bio":self.biodata.text!]
        }
        
        print("Edit Profile Is",parameters)
        
        if pickerImage.size.width != 0 {
            
             let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)update_profile")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                                
                                print(responseData1)
                                
                                let username = responseData1["username"] as! String
                                let firstName = responseData1["first_name"] as! String
                                let mobile = responseData1["mobile"] as! String
                                let bio = responseData1["bio"] as! String
                                let image = responseData1["image"] as! String
                                let publicOrPrivate = responseData1["is_public"] as! String
                                let imageStr = image
                                
                                self.firstnameTF.text = firstName
                                self.userNameTF.text = username
                                self.mobileNumberTF.text = mobile

                                DispatchQueue.main.async {
                                    self.profileImage.sd_setImage(with: URL (string: imageStr), placeholderImage:
                                        UIImage(named: "userrr"))
                                }
                                self.biodata.text = bio
                                
                                UserDefaults.standard.set(username, forKey: "username")
                                UserDefaults.standard.set(firstName, forKey: "first_name")
                                UserDefaults.standard.set(mobile, forKey: "mobile")
                                UserDefaults.standard.set(imageStr, forKey: "image")
                                UserDefaults.standard.set(publicOrPrivate, forKey: "is_public")
                                UserDefaults.standard.set(bio, forKey: "bio")
                                self .dismiss(animated: true, completion: nil)
                                self.showToastForAlert(message: message)
                               // self.viewDidLoad()
                            }
                        }
                        else
                        {
                            self.showToastForError(message: message)
                            //self.showToastForAlert(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
        else{
            
            pickerImage1 = profileImage.image!
            let imagedata = UIImageJPEGRepresentation(pickerImage1, 0.2)!
            print("ImageData Original",imagedata)
           // let imagedata = profileImage.image
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imagedata, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)update_profile")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            Services.sharedInstance.dissMissLoader()
                            
                            if let responseData1 = responseData!["data"] as? Dictionary<String, AnyObject> {
                                print(responseData1)
                                
                                let username = responseData1["username"] as! String
                                let firstName = responseData1["first_name"] as! String
                                let mobile = responseData1["mobile"] as! String
                                let bio = responseData1["bio"] as! String
                                let image = responseData1["image"] as! String
                                let publicOrPrivate = responseData1["is_public"] as! String
                                let imageStr = image
                                
                                self.firstnameTF.text = firstName
                                self.userNameTF.text = username
                                self.mobileNumberTF.text = mobile

                                DispatchQueue.main.async {
                                    self.profileImage.sd_setImage(with: URL (string: imageStr), placeholderImage:
                                        UIImage(named: "userrr"))
                                }
                                self.biodata.text = bio
                                
                                UserDefaults.standard.set(username, forKey: "username")
                                UserDefaults.standard.set(firstName, forKey: "first_name")
                                UserDefaults.standard.set(mobile, forKey: "mobile")
                                UserDefaults.standard.set(imageStr, forKey: "image")
                                UserDefaults.standard.set(publicOrPrivate, forKey: "is_public")
                                UserDefaults.standard.set(bio, forKey: "bio")
                                self .dismiss(animated: true, completion: nil)
                                self.showToastForAlert(message: message)
                            }
                        }
                        else
                        {
                            Services.sharedInstance.dissMissLoader()
                            self.showToastForError(message: message)
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
    }
    
    @objc func imageTapped() {
        
        let view = UIAlertController(title: languageChangeString(a_str: "Pick Image"), message: "", preferredStyle: .actionSheet)
        let PhotoLibrary = UIAlertAction(title:  languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
            self.PhotoLibrary()
            view.dismiss(animated: true)
        })
        let camera = UIAlertAction(title: languageChangeString(a_str: "Take a new photo"), style: .default, handler: { action in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
            view.dismiss(animated: true)
        })

        let cancel = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
        })
            view.addAction(camera)
            view.addAction(PhotoLibrary)
            view.addAction(cancel)
            present(view, animated: true)
        }
    
    func PhotoLibrary()
    {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true)
    }
    
    func camera()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: languageChangeString(a_str: "No Camera"),message: languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    @IBAction func privateOnBtn(_ sender: Any) {
        if privateSwitch.isOn == false
        {
            let privateOn = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PrivateOn")
            self .present(privateOn, animated: true, completion: nil)
        }
        else{
            privateSwitch.isOn = false
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
         self .dismiss(animated: true, completion: nil)
    }
}

extension EditProfile: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //let pickedImage2 : UIImage!
        
        if let img = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            pickedImage = img
        }
        else if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            pickedImage = img
        }

        picker.dismiss(animated: true, completion: nil)
       // let pickedImage2 = info[UIImagePickerControllerOriginalImage]
       // print(pickedImage ?? "")
        
        profileImage.image = pickedImage
        pickerImage = pickedImage
       
    }
}
