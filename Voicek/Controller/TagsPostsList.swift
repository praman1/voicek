//
//  TagsPostsList.swift
//  Voicek
//
//  Created by volivesolutions on 09/11/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class TagsPostsList: UIViewController {
    @IBOutlet weak var tagsPostListTableView: UITableView!
    
    var refreshControl: UIRefreshControl?
    var postIdArr = [String]()
    var userIdArr = [String]()
    var imageArr = [String]()
    var audioArr = [String]()
    var durationArr = [String]()
    var descArr = [String]()
    var nameArr = [String]()
    var isPublicArr = [String]()
    var playedArr = [String]()
    var likesArr = [String]()
    var requestedArr = [String]()
    
    var tagPostsData : [[String : AnyObject]]!
    
    var ownerUserIdStr : String! = ""
    var tagIdStr : String! = ""
    var tagNameStr : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button1 = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(backButtonTapped(sender:)))
        button1.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationItem.leftBarButtonItem  = button1
        
        self.navigationItem.title = String(format: "#%@", tagNameStr!)
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor(red: 0.0 / 255.0, green: 226.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
        tagsPostListTableView.addSubview(refreshControl!)
        refreshControl?.addTarget(self, action: #selector(self.refreshTable), for: .valueChanged)
        
        let colors = [(UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)), (UIColor(red: 0.0 / 255.0, green: 223.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0))]
        self.navigationController?.navigationBar.gradeintBarTintColors(with: colors)
        
        self.tagPostsListServiceCall()

        // Do any additional setup after loading the view.
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func refreshTable() {
        DispatchQueue.main.async(execute: {
            self.refreshControl?.endRefreshing()
            self.tagsPostListTableView.reloadData()
        })
    }
    
    //MARK:TAG POSTS LIST SERVICE CALL
    func tagPostsListServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/tag_posts?API-KEY=225143&user_id=141842597&tag_id=1
        Services.sharedInstance.loader(view: self.view)
        let tags = "\(Base_Url)tag_posts?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "tag_id" : tagIdStr!]
        
        print(parameters)
        
        Alamofire.request(tags, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    //NotificationCenter.default.removeObserver(self)
                    self.postIdArr = [String]()
                    self.userIdArr = [String]()
                    self.imageArr = [String]()
                    self.audioArr = [String]()
                    self.durationArr = [String]()
                    self.descArr = [String]()
                    self.nameArr = [String]()
                    self.isPublicArr = [String]()
                    self.playedArr = [String]()
                    self.likesArr = [String]()
                    self.requestedArr = [String]()
                    Services.sharedInstance.dissMissLoader()
                    
                    self.tagPostsData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.tagPostsData
                    {
                        
                        let postId = each["id"] as? String
                        //self.nullToNil(value: postId as AnyObject)
                        let userId = each["user_id"] as? String
                        
                        let image = each["image"] as? String
                        let audio = each["audio"] as? String
                        
                        let duration = each["duration"] as? String
                        let description = each["description"] as? String
                        
                        let name = each["name"] as? String
                        let is_public = each["is_public"] as? String
                        
                        let x : Int = each["played"] as! Int
                        let xNSNumber = x as NSNumber
                        let played : String = xNSNumber.stringValue
                        
                        let x1 : Int = each["likes"] as! Int
                        let xNSNumber1 = x1 as NSNumber
                        let likes : String = xNSNumber1.stringValue
                        
                        let x2 : Int = each["requested"] as! Int
                        let xNSNumber2 = x2 as NSNumber
                        let requested : String = xNSNumber2.stringValue
                        
                        self.postIdArr.append(postId!)
                        self.userIdArr.append(userId!)
                        
                        self.imageArr.append(image!)
                        self.audioArr.append(audio!)
                        
                        self.durationArr.append(duration!)
                        self.descArr.append(description!)
                        
                        self.nameArr.append(name!)
                        self.isPublicArr.append(is_public!)
                        
                        self.playedArr.append(played)
                        self.likesArr.append(likes)
                        self.requestedArr.append(requested)
                    }
                    
                    DispatchQueue.main.async {
                        self.tagsPostListTableView.reloadData()
                        self.refreshControl?.endRefreshing()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }

}

extension TagsPostsList: UITableViewDataSource , UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var  cell : HomeCell

        cell = tagsPostListTableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.homeImg.layer.cornerRadius = cell.homeImg.frame.size.height/2
        cell.nameLbl.text = nameArr[indexPath.row]
        cell.homeImg.sd_setImage(with: URL (string: imageArr[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
        cell.subNameLbl.text = descArr[indexPath.row]
        cell.likesLBl.text = String(format: "%@ %@", likesArr[indexPath.row],self.languageChangeString(a_str: "Likes")!)
        cell.playedLbl.text = String(format: "%@ %@", playedArr[indexPath.row],self.languageChangeString(a_str: "Played")!)
        cell.secLbl.text = durationArr[indexPath.row]
        cell.playBtn.tag = indexPath.row
        cell.playBtn.addTarget(self, action: #selector(playClicked(sender:)), for: UIControlEvents.touchUpInside)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let profile = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
        profile.postUserIdStr = self.userIdArr[indexPath.row]
        self.navigationController?.pushViewController(profile, animated: true)
    }

    @objc func playClicked(sender:UIButton)
    {
        let buttonRow = sender.tag
        if isPublicArr[buttonRow] == "2"
        {
            if userIdArr[buttonRow] != ownerUserIdStr{
                if requestedArr[buttonRow] == "1"{
                    let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                    play.postId = postIdArr[buttonRow]
                    play.postUserId = userIdArr[buttonRow]
                    play.audioDuration = durationArr[buttonRow]
                    play.audioUrlString = audioArr[buttonRow]
                    //DispatchQueue.main.async {
                    self.navigationController?.pushViewController(play, animated: false)
                    // }

                }else{
                    self.showToastForError(message: languageChangeString(a_str: "Please follow this user to play this post")!)
                }
            }else{
                let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
                play.postId = postIdArr[buttonRow]
                play.postUserId = userIdArr[buttonRow]
                play.audioDuration = durationArr[buttonRow]
                play.audioUrlString = audioArr[buttonRow]
                //DispatchQueue.main.async {
                self.navigationController?.pushViewController(play, animated: false)
                //}
            }
        }else{
            let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Details") as! Details
            play.postId = postIdArr[buttonRow]
            play.postUserId = userIdArr[buttonRow]
            play.audioDuration = durationArr[buttonRow]
            play.audioUrlString = audioArr[buttonRow]
            //DispatchQueue.main.async {
            self.navigationController?.pushViewController(play, animated: false)
            // }
        }
    }
}

extension UINavigationBar{
    func gradeintBarTintColors(with gradient:[UIColor]){
        var frameAndStatusBarSize = self.bounds
        frameAndStatusBarSize.size.height +=  20
        setBackgroundImage(UINavigationBar.gradients(size: frameAndStatusBarSize.size, color: gradient), for: .default)
    }
    
    static func gradients(size:CGSize,color:[UIColor]) -> UIImage?{
        //turn color into cgcolor
        let colors = color.map{$0.cgColor}
        
        //begin graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        // From now on, the context gets ended if any return happens
        defer {UIGraphicsEndImageContext()}
        
        //create core graphics context
        let locations:[CGFloat] = [0.0,1.0]
        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
            return nil
        }
        //draw the gradient
        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

