//
//  SignUpVC.swift
//  Voicek
//
//  Created by volivesolutions on 18/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import MOLH

class SignUpVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addPhotoBtn: UIButton!
    
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var mobileNumberLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var conformPasswordLbl: UILabel!
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    
    var tokenString : String?
    var imgStr : String = ""
    var profileBool : Bool!
    var pickerImage  = UIImage()
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var createBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        profileBool = false
        
//        pickerImage = profileImage.image!
//        let imagedata = UIImageJPEGRepresentation(pickerImage, 0.2)!
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.0/255.0, green: 227.0/255.0, blue: 239.0/255.0, alpha: 1)
        
        tokenString = UserDefaults.standard.object(forKey: "deviceToken") as? String
        imagePicker.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        
        self.view.addGestureRecognizer(tapGesture)
        
        let button1 = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationItem.leftBarButtonItem  = button1
        
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 2
        createBtn.layer.cornerRadius = 25
        
        self.addPhotoBtn.setTitle(languageChangeString(a_str: "Add a photo"), for: UIControlState.normal)
        self.firstNameLbl.text = languageChangeString(a_str: "Full Name")
        self.userNameLbl.text = languageChangeString(a_str: "Username")
        self.emailLbl.text = languageChangeString(a_str: "Email")
        self.mobileNumberLbl.text = languageChangeString(a_str: "Mobile Number")
        self.passwordLbl.text = languageChangeString(a_str: "Password")
        self.conformPasswordLbl.text = languageChangeString(a_str: "Confirm Password")
        self.createBtn.setTitle(languageChangeString(a_str: "Create Account"), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }
    
    func assignbackground(){
      
        let background = UIImage(named: "Background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }

    @objc func backButtonTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if MOLHLanguage.isRTLLanguage() {
            self.firstNameTF.textAlignment = NSTextAlignment.right
            self.userNameTF.textAlignment = NSTextAlignment.right
            self.emailTF.textAlignment = NSTextAlignment.right
            self.mobileNumberTF.textAlignment = NSTextAlignment.right
            self.passwordTF.textAlignment = NSTextAlignment.right
            self.confirmPasswordTF.textAlignment = NSTextAlignment.right
            
        }else{
            self.firstNameTF.textAlignment = NSTextAlignment.left
            self.userNameTF.textAlignment = NSTextAlignment.left
            self.emailTF.textAlignment = NSTextAlignment.left
            self.mobileNumberTF.textAlignment = NSTextAlignment.left
            self.passwordTF.textAlignment = NSTextAlignment.left
            self.confirmPasswordTF.textAlignment = NSTextAlignment.left
        }
    }

    @IBAction func createBtn_Action(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork(){
            if firstNameTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter your full name")!)
            }else if userNameTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter your username")!)
            }else if emailTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter your email")!)
            }else if mobileNumberTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter your mobilenumber")!)
            }else if passwordTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter password")!   )
            }else if confirmPasswordTF.text == ""{
                self.showToastForError(message: languageChangeString(a_str: "Please enter confirm password")!)
            }else{
                signUpServiceCall()
            }
        }else{
            self.showToastForError(message: languageChangeString(a_str: "Sorry, you need an internet connection. Please connect to a Wi-Fi or mobile network and try again.")!)
        }
    }
    
    //MARK:SIGNUP SERVICE CALL
    func signUpServiceCall()
    {
        //let token = UserDefaults.standard.object(forKey: "deviceToken")
        
        //let signUpData = "\(Base_Url)registration"
        
        Services.sharedInstance.loader(view: self.view)
        //http://volive.in/voicek/api/services/registration
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "first_name":self.firstNameTF.text!, "username":self.userNameTF.text!, "email":self.emailTF.text!,  "mobile":self.mobileNumberTF.text!, "password":self.passwordTF.text!,"confirm_passwd":self.confirmPasswordTF.text!, "device":"iOS", "token" : tokenString ?? "" ]
        
        print("registration Is",parameters)
    
        print(parameters)
        if pickerImage.size.width != 0 {
            
            let imgData = UIImageJPEGRepresentation(pickerImage, 0.2)!
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)registration")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print(response.result.value ?? "")
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            Services.sharedInstance.dissMissLoader()
                            // self.showToastForAlert(message: message)
                            let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(login, animated: true)
                        }
                        else
                        {
                            self.showToastForError(message: message)
                           // self.showToastForAlert(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
        }
        else{
            //profileImage.image = UIImage.init(named: "Logo")
            pickerImage = UIImage.init(named: "DefaultUser")!
            let imagedata = UIImageJPEGRepresentation(pickerImage, 0.2)!
            print("ImageData Original",imagedata)
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imagedata, withName: "file",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:"\(Base_Url)registration")
            { (result) in
                
                print(result)
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        
                        print(response.result.value ?? "")
                        
                        let responseData = response.result.value as? Dictionary<String, Any>
                        
                        let status = responseData!["status"] as! Int
                        let message = responseData!["message"] as! String
                        
                        if status == 1
                        {
                            Services.sharedInstance.dissMissLoader()
                            // self.showToastForAlert(message: message)
                            let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(login, animated: true)
                            
                        }
                        else
                        {
                            self.showToastForError(message: message)
                            Services.sharedInstance.dissMissLoader()
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    Services.sharedInstance.dissMissLoader()
                }
            }
    }

}

    @IBAction func addPhotoBtn_Action(_ sender: Any) {
        
        let view = UIAlertController(title: languageChangeString(a_str:"Pick Image"), message: "", preferredStyle: .actionSheet)
        let PhotoLibrary = UIAlertAction(title: languageChangeString(a_str: "Choose a photo from your library") , style: .default, handler: { action in
           self.PhotoLibrary()
            view.dismiss(animated: true)
        })
        let camera = UIAlertAction(title: languageChangeString(a_str: "Take a new photo"), style: .default, handler: { action in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
            view.dismiss(animated: true)
        })
        let remove = UIAlertAction(title: languageChangeString(a_str: "Remove this photo"), style: .default, handler: { action in
            
//            self.imagePicker = UIImagePickerController()
//            self.imagePicker.delegate = self
//            self.imagePicker.sourceType = .camera
//            self.present(self.imagePicker, animated: true)
//            view.dismiss(animated: true)
        })
        remove.setValue(UIColor.red, forKey: "titleTextColor")
        
        let cancel = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
        })
        
        if pickerImage.size.width != 0 {
            view.addAction(camera)
            view.addAction(PhotoLibrary)
            view.addAction(remove)
        }
        else{
            view.addAction(camera)
            view.addAction(PhotoLibrary)
        }
        
        view.addAction(cancel)
        present(view, animated: true)

    }
    
    func PhotoLibrary()
    {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true)
    }
    
    func camera()
    {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.present(self.imagePicker,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: languageChangeString(a_str: "No Camera"),message: languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    @objc func hideKeyboard()
    {
            firstNameTF.resignFirstResponder()
            userNameTF.resignFirstResponder()
            emailTF.resignFirstResponder()
            mobileNumberTF.resignFirstResponder()
            passwordTF.resignFirstResponder()
            confirmPasswordTF.resignFirstResponder()
    }
    
}

extension SignUpVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        let pickedImage2 = info[UIImagePickerControllerOriginalImage]
        print(pickedImage2 ?? "")
        
        profileImage.image = pickedImage2  as? UIImage
        
        pickerImage = (pickedImage2 as? UIImage)!
       
        //self.navigationController?.isNavigationBarHidden = true
        
    }
}
//extension UINavigationBar{
//
//    func gradientBarTintColor(with gradient:[UIColor]){
//        var frameAndStatusBarSize = self.bounds
//        frameAndStatusBarSize.size.height +=  20
//        setBackgroundImage(UINavigationBar.gradients(size: frameAndStatusBarSize.size, color: gradient), for: .default)
//    }
//
//    static func gradients(size:CGSize,color:[UIColor]) -> UIImage?{
//        //turn color into cgcolor
//        let colors = color.map{$0.cgColor}
//
//        //begin graphics context
//        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
//        guard let context = UIGraphicsGetCurrentContext() else {
//            return nil
//        }
//
//        // From now on, the context gets ended if any return happens
//        defer {UIGraphicsEndImageContext()}
//
//        //create core graphics context
//        let locations:[CGFloat] = [0.0,1.0]
//        guard let gredient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as NSArray as CFArray, locations: locations) else {
//            return nil
//        }
//        //draw the gradient
//        context.drawLinearGradient(gredient, start: CGPoint(x:0.0,y:0.0), end: CGPoint(x:0.0,y:size.height), options: [])
//
//        // Generate the image (the defer takes care of closing the context)
//        return UIGraphicsGetImageFromCurrentImageContext()
//    }
//}
