//
//  Onboard.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class Onboard: UIViewController {

    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var onboardCollection: UICollectionView!
    
    var titlesArr = [String]()
    var descArr = [String]()
    var diffArr = [String]()
    
    var imagesArray : [UIImage] = [#imageLiteral(resourceName: "OnBoarding1"),#imageLiteral(resourceName: "OnBoarding4"),#imageLiteral(resourceName: "OnBoarding2"),#imageLiteral(resourceName: "OnBoarding5"),#imageLiteral(resourceName: "OnBoarding3")]
   
    override func viewDidLoad() {
        super.viewDidLoad()

        diffArr = ["1","2","3","4","5"]
        onboardCollection.isPagingEnabled = true
        self.titlesArr = [languageChangeString(a_str: "Explore"),languageChangeString(a_str: "Record"),languageChangeString(a_str: "Features"),languageChangeString(a_str: "Share"),languageChangeString(a_str: "Listen")] as! [String]
        self.descArr = [languageChangeString(a_str: "Explore our world of sounds,listen to stories from around the world and enjoy heightening your surroundings"),languageChangeString(a_str: "Hold + Record + Broadcast simple as 1,2,3"),languageChangeString(a_str: "Enhance,Edit and mask your voice,make it truly your own"),languageChangeString(a_str: "Share your sounds with the world(Use social media icons(FB,TWT,INST))"),languageChangeString(a_str: "Listen to the world around you and color it with your imagination as you see")] as! [String]
 
       // self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
    }
  
    override func  viewWillAppear(_ animated: Bool) {
        
//        if UserDefaults.standard.bool(forKey: "Loggedin") == true {
//            let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC1")
//            self.present(homeVC, animated: false, completion: nil)
//        }
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = false
    }

    @IBAction func skipBtn(_ sender: Any) {
        let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(login, animated: false)
    }
}

extension Onboard:  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = onboardCollection.dequeueReusableCell(withReuseIdentifier: "OnboardCell", for: indexPath) as? OnboardCell
        cell?.onboardImg.image = imagesArray[indexPath.row]
        cell?.titleLbl.text = titlesArr[indexPath.row]
        cell?.decsLbl.text = descArr[indexPath.row]
        cell?.titleBtn.setTitle(languageChangeString(a_str: "Skip"), for: UIControlState.normal)
        
        if diffArr[indexPath.row] == "5" {
       
            cell?.titleBtn.setTitle(languageChangeString(a_str: "Start"), for: UIControlState.normal)
            cell?.titleBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5058823529, blue: 0.9529411765, alpha: 1)
            cell?.titleBtn.layer.cornerRadius = (cell?.titleBtn.frame.size.height)!/2
        }
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.onboardCollection.frame.size.width,height:  self.onboardCollection.frame.size.height)
    }
   
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == self.onboardCollection {
            
            self.pageController.currentPage = indexPath.row
        }
    }
    
}
