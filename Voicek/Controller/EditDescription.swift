//
//  EditDescription.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class EditDescription: UIViewController {
    var descString : String!
    var postIdStr : String!
    var userIdStr : String!

    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var descTextView: UITextView!
    override func viewDidLoad() {
        
        super.viewDidLoad()

        descTextView.text = descString
        self.navigationItem.title = languageChangeString(a_str: "Edit Description")
       self.saveBtn.setTitle(languageChangeString(a_str: "Save"), for: UIControlState.normal)
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
    }
        
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func save(_ sender: Any) {
        
        editDescServiceCall()
    }
    
    //MARK:EDIT DESCRIPTION SERVICE CALL
    func editDescServiceCall()
    {
        // http://volive.in/voicek/api/services/edit_post_desc
        Services.sharedInstance.loader(view: self.view)
        
        let editDesc = "\(Base_Url)edit_post_desc"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY, "user_id":userIdStr!, "description" : descTextView.text!, "post_id":postIdStr!]
        
        print(parameters)
        
        Alamofire.request(editDesc, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            let responseData = response.result.value as? Dictionary<String, Any>
            
            let status = responseData!["status"] as! Int
            let message = responseData!["message"] as! String
            
            if status == 1
            {
                Services.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: message)
                
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editDesc"), object: self.descTextView.text)
                UserDefaults.standard.set(self.descTextView.text, forKey: "editDesc")
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.showToastForError(message: message)
                //self.showToastForAlert(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }

}
