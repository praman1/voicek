//
//  Record.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import AVFoundation


class Record: UIViewController,AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var player:AVAudioPlayer!
    var settings = [String : Int]()
    var soundURL = NSURL()
    var seconds = 31
    var timer = Timer()
    var isTimerRunning = false
    var durationInSeconds = Int()
    
    @IBOutlet var nextBarBtn: UIBarButtonItem!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var taoView: UIView!
    @IBOutlet weak var play: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        play.tag = 1
        nextBarBtn.isEnabled = false
        nextBarBtn.tintColor = UIColor.clear
        taoView.layer.cornerRadius = taoView.frame.size.height/2
        self.nextBarBtn.title = languageChangeString(a_str: "Next")
        self.navigationItem.title = languageChangeString(a_str: "Record")
        self.timeLabel.text = languageChangeString(a_str: "Tap to record")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        taoView.addGestureRecognizer(tapGesture)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() {  allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                       // self.runTimer()
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        settings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderBitRateKey:12800,
            AVLinearPCMBitDepthKey:16,
        ]
    }

    @objc func viewTapped()
    {
        if play.tag == 1{
            if audioRecorder == nil {
                self.runTimer()
                play.setImage(UIImage (named: "StopRecord"), for: UIControlState.normal)
                play.tag = 0
                self.startRecording()
            }
        }else{
            
            self.finishRecording(success: true)
            play.setImage(UIImage (named: "StartRecord"), for: UIControlState.normal)
            play.tag = 1
            
            let filters = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FiltersVC") as! FiltersVC
            filters.pickedAudio = soundURL
            filters.duration = durationInSeconds
            self.navigationController?.pushViewController(filters, animated: true)
        }
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(Record.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func stopTimer()
    {
        if timer != nil {
            timer.invalidate()
            timer = Timer()
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1
        timeLabel.text = timeString(time: TimeInterval(seconds)) //This will update the label.
        if seconds == 00 {
            stopTimer()
            finishRecording(success: true)
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        // let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    //MARK:START RECORDING
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    
    //MARK:FINISH RECORDING
    func finishRecording(success: Bool) {
        nextBarBtn.isEnabled = true
        nextBarBtn.tintColor = UIColor.white
        audioRecorder.stop()
        self.stopTimer()
        
        if success {
            print(success)
            
            let audioAsset = AVURLAsset.init(url: soundURL as URL, options: nil)
            let duration = audioAsset.duration
            durationInSeconds = Int(CMTimeGetSeconds(duration))
            print("Sound Duration Is",durationInSeconds)
            
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        print("documentDirectory",documentDirectory)
        soundURL = (documentDirectory.appendingPathComponent("sound.wav")! as NSURL?)!
        print("soundURL",soundURL)
        //self.convertmp3()
        
        return soundURL as NSURL?
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
//             let date : [String : NSURL] = ["audioPath" : soundURL ]
//            print("Sound URL Is",date)
        }
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        
//        let play = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCaption") as! AddCaption
//        play.pickedAudio = soundURL
//        play.duration = durationInSeconds
//        self.navigationController?.pushViewController(play, animated: true)
   
    }
    
    @IBAction func playBtn(_ sender: Any) {
        
        if play.tag == 1{
            if audioRecorder == nil {
                self.runTimer()
                play.setImage(UIImage (named: "StopRecord"), for: UIControlState.normal)
                play.tag = 0
                self.startRecording()
            }
        }else{
            //audioRecorder = nil
            
            
            self.finishRecording(success: true)
            play.setImage(UIImage (named: "StartRecord"), for: UIControlState.normal)
            play.tag = 1
            let filters = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FiltersVC") as! FiltersVC
            filters.pickedAudio = soundURL
            filters.duration = durationInSeconds
            self.navigationController?.pushViewController(filters, animated: true)
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        if audioRecorder != nil {
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: "Do you want to discard the recording?", preferredStyle: .alert)
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "NO", style: .cancel) { action -> Void in }
            let yesAction: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
                print("Timer Has Stopped")
                self.stopTimer()
                self.finishRecording(success: true)
                self .dismiss(animated: true, completion: nil)
            }
            // add actions
            actionSheetController.addAction(yesAction)
            actionSheetController.addAction(cancelAction)
        
            // present an actionSheet...
            present(actionSheetController, animated: true, completion: nil)
            
        }else{
            self .dismiss(animated: true, completion: nil)
        }
    }
}
