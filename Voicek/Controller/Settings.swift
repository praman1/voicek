//
//  Settings.swift
//  Voicek
//
//  Created by Suman Guntuka on 20/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import MOLH

class Settings: UIViewController{
//    func reset() {
//
//    }
    var window : UIWindow?
    
    @IBOutlet var changePasswordLabel: UILabel!
    @IBOutlet var blockedAccountsLabel: UILabel!
    @IBOutlet var linkedAccountsLabel: UILabel!
    @IBOutlet var changeLanguageLabel: UILabel!
    
    @IBOutlet var autoPlayVoiceLabel: UILabel!
    @IBOutlet var autoPlayNextLabel: UILabel!
    @IBOutlet var notifyLikesLabel: UILabel!
    @IBOutlet var notifyCommentsLabel: UILabel!
    @IBOutlet var notifyListenersLabel: UILabel!
    @IBOutlet var notifyMentionsLabel: UILabel!
    
    @IBOutlet var aboutUsLabel: UILabel!
    @IBOutlet var frequentQuestionsLabel: UILabel!
    @IBOutlet var privacyPolicyLabel: UILabel!
    @IBOutlet var termsLabel: UILabel!
    
    
    @IBOutlet weak var myaccount: UILabel!
    @IBOutlet weak var autoplaVoice: UIButton!
    @IBOutlet weak var audioSettings: UILabel!
    @IBOutlet weak var linkedAccounts: UIButton!
    @IBOutlet weak var blockedUsers: UIButton!
    @IBOutlet weak var changePassword: UIButton!
    
    @IBOutlet var changeLanguage: UIButton!
    @IBOutlet weak var autoplayNextVoice: UIButton!
    @IBOutlet weak var notifications: UILabel!
    @IBOutlet weak var likedNotifications: UIButton!
    @IBOutlet weak var commentNotifications: UIButton!
    @IBOutlet weak var newListeners: UIButton!
    @IBOutlet weak var mentions: UIButton!
    @IBOutlet weak var aboutUs: UIButton!
    @IBOutlet weak var frequentlyAsk: UIButton!
    @IBOutlet weak var privacyPolicy: UIButton!
    @IBOutlet weak var termsandcondi: UIButton!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var autoPlaySwitch: UISwitch!
    
    @IBOutlet weak var likesSwitch: UISwitch!
    @IBOutlet weak var commentsSwitch: UISwitch!
    @IBOutlet weak var listenersSwitch: UISwitch!
    @IBOutlet weak var mentionsSwitch: UISwitch!
    
    @IBOutlet weak var changePasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var changePswdBottomView: UIView!
    @IBOutlet weak var changePswdNextImage: UIImageView!
    
    var newCommentsString : String! = ""
    var newMentionsString : String! = ""
    var newListenersString : String! = ""
    var newLikesString : String! = ""
    var ownerUserIdStr : String! = ""
    var typeString : String! = ""
    var isEnableString : String! = ""
    var commentCheckString : String! = ""
    var mentionCheckString : String! = ""
    var listenersCheckString : String! = ""
    var likesCheckString : String! = ""
    var socialTypeStr : String! = ""
    var autoPlayString : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = languageChangeString(a_str: "Settings")
       
        self.myaccount.text = languageChangeString(a_str: "MY ACCOUNT")
       
        self.changePasswordLabel.text = languageChangeString(a_str: "Change Password")
        self.blockedAccountsLabel.text = languageChangeString(a_str: "Blocked Accounts")
        self.linkedAccountsLabel.text = languageChangeString(a_str: "Linked Accounts")
        self.changeLanguageLabel.text = languageChangeString(a_str: "Change Language")
        
        self.autoPlayVoiceLabel.text = languageChangeString(a_str: "Autoplay voice?")
        self.autoPlayNextLabel.text = languageChangeString(a_str: "Autoplay next voice?")
        self.notifyLikesLabel.text = languageChangeString(a_str: "Notify me of new likes for my posts")
        self.notifyCommentsLabel.text = languageChangeString(a_str: "Notify me of new comments on my posts")
        self.notifyListenersLabel.text = languageChangeString(a_str: "Notify me of new listeners")
        self.notifyMentionsLabel.text = languageChangeString(a_str: "Notify me of new mentions")
        
        self.audioSettings.text = languageChangeString(a_str: "AUDIO SETTINGS")
       
         self.logout.setTitle(languageChangeString(a_str: "Log Out"), for: UIControlState.normal)
        self.aboutUsLabel.text = languageChangeString(a_str: "About Us")
        self.frequentQuestionsLabel.text = languageChangeString(a_str: "Frequently Asked Questions")
        self.privacyPolicyLabel.text = languageChangeString(a_str: "Privacy Policy")
        self.termsLabel.text = languageChangeString(a_str: "Terms and Conditions")
        self.notifications.text = languageChangeString(a_str: "NOTIFICATIONS")
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        newCommentsString = UserDefaults.standard.object(forKey: "newComments") as? String
        newMentionsString = UserDefaults.standard.object(forKey: "newMentions") as? String
        newListenersString = UserDefaults.standard.object(forKey: "newListeners") as? String
        newLikesString = UserDefaults.standard.object(forKey: "newLikes") as? String
        socialTypeStr = UserDefaults.standard.object(forKey: "socialType") as? String
        
        if socialTypeStr == "1" || socialTypeStr == "2" {
            DispatchQueue.main.async {
                self.changePasswordHeight.constant = 0
                self.changePswdBottomView.isHidden = true
                self.changePswdNextImage.isHidden = true
                self.changePassword.isHidden = true
            }
        }else{
            DispatchQueue.main.async {
                self.changePasswordHeight.constant = 35
                self.changePswdBottomView.isHidden = false
                self.changePswdNextImage.isHidden = false
                self.changePassword.isHidden = false
            }
        }
        
        if newCommentsString == "1" {
            commentsSwitch.isOn = true
            commentCheckString = "1"
        }else{
            commentsSwitch.isOn = false
            commentCheckString = "0"
        }
        if newMentionsString == "1" {
            mentionsSwitch.isOn = true
            mentionCheckString = "1"
        }else{
            mentionsSwitch.isOn = false
            mentionCheckString = "0"
        }
        if newListenersString == "1" {
            listenersSwitch.isOn = true
            listenersCheckString = "1"
        }else{
            listenersSwitch.isOn = false
            listenersCheckString = "0"
        }
        if newLikesString == "1" {
            likesSwitch.isOn = true
            likesCheckString = "1"
        }else{
            likesSwitch.isOn = false
            likesCheckString = "0"
        }
        
        if UserDefaults.standard.object(forKey: "autoPlay") as? String == "1"{
            self.autoPlaySwitch.isOn = true
        }else{
            self.autoPlaySwitch.isOn = false
        }
        if UserDefaults.standard.string(forKey: "currentLanguage") == "ar"{
            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            }
        }
        else
        {
            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            }
        }
        //        let img = UIImage(named: "Background")
//        navigationController?.navigationBar.setBackgroundImage(img, for: .default)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func menuBtn(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
     
        logoutFromApp()
    }
    
    //MARK:LOGOUT
    func logoutFromApp()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: languageChangeString(a_str: "Are you sure you want to log out?"), preferredStyle: .alert)
        
        let yesAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Yes"), style: .default) { action -> Void in
            
            UserDefaults.standard.set(false, forKey: "Loggedin")
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
            let navigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
            navigationController.navigationBar.tintColor = UIColor.white
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
            UserDefaults.standard.removeObject(forKey: "socialType")
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject(forKey: "autoPlay")
            UserDefaults.standard.removeObject(forKey: "newComments")
            UserDefaults.standard.removeObject(forKey: "newMentions")
            UserDefaults.standard.removeObject(forKey: "newListeners")
            
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        // add actions
        actionSheetController.addAction(yesAction)
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
        
    }
    
    @IBAction func autoPlay(_ sender: Any) {
        if autoPlaySwitch.isOn == true {
            autoPlaySwitch.isOn = false
            print("Switch is off")
            autoPlayString = "0"
            UserDefaults.standard.set(autoPlayString, forKey: "autoPlay")
        }else{
            autoPlaySwitch.isOn = true
            print("Switch is on")
            autoPlayString = "1"
            UserDefaults.standard.set(autoPlayString, forKey: "autoPlay")
        }
        print("Auto Play String Is :\(autoPlayString!)")
    }
    
    @IBAction func likes(_ sender: Any) {
        if likesCheckString == "1" {
            likesSwitch.isOn = false
            typeString = "new_likes"
            isEnableString = "0"
            UserDefaults.standard.set(isEnableString, forKey: "newLikes")
            userNotifyServiceCall()
        }else{
            likesSwitch.isOn = true
            typeString = "new_likes"
            isEnableString = "1"
            UserDefaults.standard.set(isEnableString, forKey: "newLikes")
            userNotifyServiceCall()
        }
    }
    
    @IBAction func comments(_ sender: Any) {
        if commentCheckString == "1" {
            commentsSwitch.isOn = false
            typeString = "new_comments"
            isEnableString = "0"
            UserDefaults.standard.set(isEnableString, forKey: "newComments")
            userNotifyServiceCall()
        }else{
            commentsSwitch.isOn = true
            typeString = "new_comments"
            isEnableString = "1"
            UserDefaults.standard.set(isEnableString, forKey: "newComments")
            userNotifyServiceCall()
        }
    }
    
    @IBAction func listeners(_ sender: Any) {
        if listenersCheckString == "1"{
            listenersSwitch.isOn = false
            typeString = "new_listeners"
            isEnableString = "0"
            UserDefaults.standard.set(isEnableString, forKey: "newListeners")
            userNotifyServiceCall()
        }else{
            listenersSwitch.isOn = true
            typeString = "new_listeners"
            isEnableString = "1"
            UserDefaults.standard.set(isEnableString, forKey: "newListeners")
            userNotifyServiceCall()
        }
    }
    
    @IBAction func mentions(_ sender: Any) {
        if mentionCheckString == "1" {
            mentionsSwitch.isOn = false
            typeString = "new_mentions"
            isEnableString = "0"
            UserDefaults.standard.set(isEnableString, forKey: "newMentions")
            userNotifyServiceCall()
        }else{
            mentionsSwitch.isOn = true
            typeString = "new_mentions"
            isEnableString = "1"
            UserDefaults.standard.set(isEnableString, forKey: "newMentions")
            userNotifyServiceCall()
        }
    }
    
    //MARK:USER NOTIFY SERVICE CALL
    func userNotifyServiceCall()
    {
        // http://volive.in/voicek/api/services/user_update_notify?API-KEY=225143&user_id=3783858425&is_enable=1&type=new_likes
        //Services.sharedInstance.loader(view: self.view)
        let user_update_notify = "\(Base_Url)user_update_notify?"
        print("user_update_notify URL Is :\(user_update_notify)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "is_enable" : isEnableString! , "type" : typeString!]
        
        print(parameters)
        
        Alamofire.request(user_update_notify, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    // let blocked = responseData["blocked"] as! Int
                    
                    Services.sharedInstance.dissMissLoader()
                    self.viewDidLoad()
                    //self.showToastForAlert(message: message)
                }
                else{
                    DispatchQueue.main.async {
                        //self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @IBAction func aboutUsBtn(_ sender: Any) {
        
        let frequent = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutUs") as! AboutUs
        self.navigationController?.pushViewController(frequent, animated: false)
    }
    
    @IBAction func privacyPolocyBtn(_ sender: Any) {
        
        let privacy = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicy") as! PrivacyPolicy
        self.navigationController?.pushViewController(privacy, animated: false)
    }
    
    @IBAction func frequentlyQuestionBtn(_ sender: Any) {
    
        let frequent = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FrequentlyQuestion") as! FrequentlyQuestion
        self.navigationController?.pushViewController(frequent, animated: false)
    }
    
    @IBAction func termsAndConditionsBtn(_ sender: Any) {
        
        let terms = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditions
        self.navigationController?.pushViewController(terms, animated: false)
    }
    
    @IBAction func blockedUsersBtn(_ sender: Any) {
        
        let blocked = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BlockedAccounts") as! BlockedAccounts
        blocked.testStr = "1"
        self.navigationController?.pushViewController(blocked, animated: false)
    }
   
    @IBAction func changePasswordBtn(_ sender: Any) {

        let changePW = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePassword") as! ChangePassword
        self.navigationController?.pushViewController(changePW, animated: false)
    }
    
    
    @IBAction func linkedAccountsBtn(_ sender: Any) {
        
        let linked = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LinkedAccounts") as! LinkedAccounts
        self.navigationController?.pushViewController(linked, animated: false)
    }
    
    func changeLanguageBlock(){
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
                // MOLHLanguage.setAppleLAnguageTo("ar")
        MOLH.reset(transition: .transitionCrossDissolve)
        self.viewDidLoad()
    }
    
    @IBAction func changeLanguageBtn(_ sender: Any) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let englishAction: UIAlertAction = UIAlertAction(title: "English", style: .default) { action -> Void in
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "en" : "en")
            MOLH.reset(transition: .transitionCrossDissolve)
            UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")

            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC")
            let navigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
            // navigationController.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        let arabicAction: UIAlertAction = UIAlertAction(title: "عربى", style: .default) { action -> Void in
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "ar" ? "ar" : "ar")
            
            MOLH.reset(transition: .transitionCrossDissolve)
            UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
         
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC")
            let navigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.8862745098, blue: 0.6705882353, alpha: 1)
            // navigationController.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: languageChangeString(a_str: "Cancel"), style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(englishAction)
        actionSheetController.addAction(arabicAction)
        actionSheetController.addAction(cancelAction)
        // present an actionSheet...
        present(actionSheetController, animated: true, completion: nil)
    }
}
