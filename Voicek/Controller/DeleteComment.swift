//
//  DeleteComment.swift
//  Voicek
//
//  Created by volivesolutions on 06/09/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class DeleteComment: UIViewController {
    
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var rejectBtn: UIButton!
    var commentIdStr : String!
    var postIdStr : String!
    var ownerUserIdStr : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentIdStr = UserDefaults.standard.object(forKey: "commentId") as? String
        postIdStr = UserDefaults.standard.object(forKey: "postId") as? String
        ownerUserIdStr = UserDefaults.standard.object(forKey: "ownerUserIdStr") as? String
        self.messageLabel.text = languageChangeString(a_str: "Are you sure you want to delete this comment?")
        self.acceptBtn.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
        self.rejectBtn.setTitle(languageChangeString(a_str: "Reject"), for: UIControlState.normal)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
        

        // Do any additional setup after loading the view.
    }
    
    @objc func hideView()
    {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func accept(_ sender: Any) {
        
        deletecCommentServiceCall()
        
    }
    
    @IBAction func reject(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:DELETE COMMENT SERVICE CALL
    func deletecCommentServiceCall()
    {
        // http://volive.in/voicek/api/services/delete_comment?API-KEY=225143&user_id=3783858425&post_id=7&comment_id=21
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)delete_comment?"
        print("Report Post URL Is :\(deleteUserPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "post_id" : postIdStr! , "comment_id" : commentIdStr!]
        
        print(parameters)
        
        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("commentsReload"), object: nil)
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
    }
}
