//
//  SetPassword.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class SetPassword: UIViewController {

    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var newPasswordLabel: UILabel!
    @IBOutlet var reEnterPasswordLabel: UILabel!
    @IBOutlet var setBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = languageChangeString(a_str: "Set Email and Password")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        self.messageLabel.text = languageChangeString(a_str: "Create an email and password to login to your account along with your socoal media login")
        self.emailLabel.text = languageChangeString(a_str: "Email")
        self.newPasswordLabel.text = languageChangeString(a_str: "New Password")
        self.reEnterPasswordLabel.text = languageChangeString(a_str: "Re-enter Password")
        self.setBtn.setTitle(languageChangeString(a_str: "Set"), for: UIControlState.normal)
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
