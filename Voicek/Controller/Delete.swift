//
//  Delete.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class Delete: UIViewController {

    @IBOutlet weak var reject: UIButton!
    @IBOutlet weak var accept: UIButton!
    @IBOutlet weak var textLbl: UILabel!
    var userIdStr : String!
    var postIdStr : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        userIdStr = UserDefaults.standard.object(forKey: "userIdStr") as? String
        postIdStr = UserDefaults.standard.object(forKey: "postIdStr") as? String
        self.textLbl.text = languageChangeString(a_str: "Are you sure you want to delete this post?")
        self.accept.setTitle(languageChangeString(a_str: "Accept"), for: UIControlState.normal)
        self.reject.setTitle(languageChangeString(a_str: "Reject"), for: UIControlState.normal)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        self.view.addGestureRecognizer(tapGesture)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func hideView()
    {
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rejectBtn(_ sender: Any) {
   
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func acceptBtn(_ sender: Any) {
        
        
        deleteUserPostServiceCall()
    
       // self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:DELETE USER POST SERVICE CALL
    func deleteUserPostServiceCall()
    {
        // http://volive.in/voicek/api/services/delete_post?API-KEY=225143&user_id=3770410060&post_id=5
        Services.sharedInstance.loader(view: self.view)
        let deleteUserPost = "\(Base_Url)delete_post?"
        print("Report Post URL Is :\(deleteUserPost)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : userIdStr! , "post_id" : postIdStr!]
        
        print(parameters)
        
        Alamofire.request(deleteUserPost, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                if status == 1
                {
                    Services.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                    let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC1")
                    //self.navigationController?.pushViewController(home, animated: true)
                    self.present(home, animated: true, completion: nil)
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                        
                    }
                }
            }
        }
        
    }
}
