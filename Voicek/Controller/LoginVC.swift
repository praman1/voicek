//
//  ViewController.swift
//  Voicek
//
//  Created by volivesolutions on 18/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire
import FacebookLogin
import FBSDKLoginKit
import TwitterKit
import Contacts
import MOLH
class LoginVC: UIViewController ,LoginButtonDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailInvalid: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var signUpWithLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var twittrrBtn_Outlet: UIButton!
    @IBOutlet weak var facebookBtn_Outlet: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var showHideBtn: UIButton!
    
    var tokenString : String!
    var checkString:String!
    var fbIdString : String!
    var fbEmailIdString : String!
    var pictureUrlString:String!
    var fbNameString : String!
    var userNameString : String!
    
    
    var firstName: String!
    var lastName: String!
    var email: String!
    var userId:String!
    
  
    var dict : [String : AnyObject]!
    var appDelegate = AppDelegate()
    var contactList: [CNContact]!
    var nameArray = [String]()
    var nameIdArray = [String]()
    var numberArray = [String]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showHideBtn.tag = 1
        //showHideBtn.setTitle("Show", for: UIControlState.normal)
        emailInvalid.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        emailTF.text = (UserDefaults.standard.object(forKey: "email") as? String)
        passwordTF.text = (UserDefaults.standard.object(forKey: "password") as? String)
        
        facebookBtn_Outlet.layer.cornerRadius = facebookBtn_Outlet.frame.width/2
        twittrrBtn_Outlet.layer.cornerRadius = twittrrBtn_Outlet.frame.width/2
        signInBtn.layer.cornerRadius = 25
        
        let backBtn = UIBarButtonItem(image: UIImage(named:""), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        self.signUpWithLabel.text = languageChangeString(a_str: "Connect With")
        self.emailLbl.text = languageChangeString(a_str: "Email")
        self.emailInvalid.text = languageChangeString(a_str: "The email you entered is invalid")
        self.passwordLbl.text = languageChangeString(a_str: "Password")
        self.showHideBtn.setTitle(languageChangeString(a_str: "Show"), for: UIControlState.normal)
        self.signInBtn.setTitle(languageChangeString(a_str: "Login"), for: UIControlState.normal)
        self.forgotPasswordBtn.setTitle(languageChangeString(a_str: "Forgot Password?"), for: UIControlState.normal)
        self.signUpBtn.setTitle(languageChangeString(a_str: "Create New Account"), for: UIControlState.normal)
        
        self.fetchContacts()
    }
    
    @objc func backBtnClicked(){
       // self.navigationController?.popViewController(animated: true)
    }
    
    override func  viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if MOLHLanguage.isRTLLanguage() {
            self.emailTF.textAlignment = NSTextAlignment.right
            self.passwordTF.textAlignment = NSTextAlignment.right

        }else{
            self.emailTF.textAlignment = NSTextAlignment.left
            self.passwordTF.textAlignment = NSTextAlignment.left
        }
    }
    
    @IBAction func showHideBtn(_ sender: Any) {
        if showHideBtn.tag == 1 {
            passwordTF.isSecureTextEntry = false
            showHideBtn.setTitle(languageChangeString(a_str: "Hide"), for: UIControlState.normal)
            showHideBtn.tag = 2
        }
        else{
            passwordTF.isSecureTextEntry = true
            showHideBtn.setTitle(languageChangeString(a_str: "Show"), for: UIControlState.normal)
            showHideBtn.tag = 1
        }
    }
    
    @IBAction func signInBtnAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
//            if emailTF.text == ""{
//                self.showToastForError(message: "Please enter your email")
//            }else if passwordTF.text == ""{
//                self.showToastForError(message: "Please enter your password")
//            }
            loginServiceCall()
        }else
        {
            self.showToastForError(message: languageChangeString(a_str: "Sorry, you need an internet connection. Please connect to a Wi-Fi or mobile network and try again.")!)
        }
    }
    
    //MARK:LOGIN SERVICECALL
    func loginServiceCall(){
       // http://volive.in/voicek/api/services/login
        Services.sharedInstance.loader(view: self.view)
        let parameters: Dictionary<String, Any>
        tokenString = UserDefaults.standard.object(forKey: "deviceToken") as? String
        
        let signin = "\(Base_Url)login"
        if ((tokenString?.count) != nil)  {
             parameters = [ "API-KEY" :APIKEY, "email":self.emailTF.text!, "password" : self.passwordTF.text!, "device":"iOS", "token" :  tokenString!]
        }
        else
        {
            parameters = [ "API-KEY" :APIKEY, "email":self.emailTF.text!, "password" : self.passwordTF.text!, "device":"iOS", "token" :  "fkdjh72364gfdgd72346ndri"]
        }
        
        print(parameters)
        
        Alamofire.request(signin, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            let responseData = response.result.value as? Dictionary<String, Any>
            print("Login Response data Is:\(responseData!)")
            
            let status = responseData!["status"] as! Int
            let message = responseData!["message"] as! String
            
            if status == 1
            {
               
                //UserDefaults.standard.set(self.emailTF.text, forKey: "email")
                //UserDefaults.standard.set(self.passwordTF.text, forKey: "password")
                
                    Services.sharedInstance.dissMissLoader()
                    //self.showToastForAlert(message: message)
                    
                    let userId = responseData!["user_id"] as! String
                    let email = responseData!["email"] as! String
                    let userName = responseData!["username"] as! String
                    let fullName = responseData!["first_name"] as! String
                    let mobile = responseData!["mobile"] as! String
                    let image = responseData!["image"] as! String
                    let is_public = responseData!["is_public"] as! String
                    let bio = responseData!["bio"] as! String
                
                    let new_comments = responseData!["new_comments"] as! String
                    let new_mentions = responseData!["new_mentions"] as! String
                    let new_listeners = responseData!["new_listeners"] as! String
                    let new_likes = responseData!["new_likes"] as! String
                    //let loginType = responseData!["social_type"] as! String
                
                    print("new_comments is :\(new_comments)")
                    print("new_mentions is :\(new_mentions)")
                    print("new_listeners is :\(new_listeners)")
                    print("new_likes is :\(new_likes)")
                
                    UserDefaults.standard.set(userId, forKey: "userid")
                    UserDefaults.standard.set(email, forKey: "email")
                    UserDefaults.standard.set(userName, forKey: "userName")
                    UserDefaults.standard.set(fullName, forKey: "first_name")
                    UserDefaults.standard.set(mobile, forKey: "mobile")
                    UserDefaults.standard.set(image, forKey: "image")
                    UserDefaults.standard.set(is_public, forKey: "is_public")
                    UserDefaults.standard.set(bio, forKey: "bio")
                    UserDefaults.standard.set(new_comments, forKey: "newComments")
                    UserDefaults.standard.set(new_mentions, forKey: "newMentions")
                    UserDefaults.standard.set(new_listeners, forKey: "newListeners")
                    UserDefaults.standard.set(new_likes, forKey: "newLikes")
                    //UserDefaults.standard.set(loginType, forKey: "loginType")
                
                    let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC1")
                    UserDefaults.standard.set("home", forKey: "StringType")
                    UserDefaults.standard.set(true, forKey: "Loggedin")
                    self.present(homeVC, animated: false, completion: nil)
            }
            else
            {
                 self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
    
    @IBAction func forgotPasswordBtn_Action(_ sender: Any) {
        let forgotPassword = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotPassword, animated: false)
    }
    
    @IBAction func signUpBtn_Action(_ sender: Any) {
        let signUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(signUp, animated: false)
    }
    
    @IBAction func twitterBtn_Action(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            twitterDetailsFetch()
        }else
        {
            self.showToastForError(message: languageChangeString(a_str: "Oops! Please Check Your Network Connection")!)
        }
    }
    
    @IBAction func facebookBtn_Action(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            facebookDetailsFetch()
        }else
        {
            self.showToastForError(message: languageChangeString(a_str: "Oops! Please Check Your Network Connection")!)
        }
    }
    
    //MARK:FACEBOOK DETAILS FETCH
    func facebookDetailsFetch(){
            appDelegate.signInFor = "fb"
            let loginButton = LoginButton(readPermissions: [ .publicProfile ])
            loginButton.center = view.center
            loginButton.delegate = self
            loginButton.isHidden = true

            view.addSubview(loginButton)
        
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            print("e2")
                            self.getFBUserData()
                            //fbLoginManager.logOut()
                        }
                    }
                }else{
                    self.getFBUserData()
                    print("ee1\(error!)")
                }
            }
        }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        print("logged in")
        self.getFBUserData()
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }
    
    //MARK:GET FB USER DATA
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name,email,first_name,last_name,picture.width(100).height(100)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as? [String : AnyObject]
                    print(result!)
                    print("dic",self.dict)
                    
                    self.fbIdString = self.dict["id"] as? String
                    self.fbEmailIdString = self.dict["email"] as? String
                    self.fbNameString =  self.dict["name"] as? String
                    self.userNameString =  self.dict["first_name"] as? String
                    self.checkString = "FaceBook"
                  
                    if let profilePictureObj = self.dict["picture"] as? NSDictionary
                    {
                        let data = profilePictureObj["data"] as! NSDictionary
                        self.pictureUrlString  = data["url"] as? String
//                        let social = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialAccounts") as! SocialAccounts
//                        social.fbIdStr = self.fbIdString
//                        social.fbEmailIdStr = self.fbEmailIdString
//                        social.fbNameStr = self.fbNameString
//                        social.userNameStr = self.userNameString
//                        social.imageUrlStr = self.pictureUrlString
//                        social.loginTypeCheckStr = "fb"
//                        self.navigationController?.pushViewController(social, animated: false)
                    }
                      self.checkSocialLoginServiceCall()
                }else{
                    print("ee")
                }
            })
        }else{
            print("pp")
        }
    }
  
    //MARK:TWITTER DETAILS FETCH
    func twitterDetailsFetch(){
//        TWTRTwitter.sharedInstance().logIn {
//            (session, error) -> Void in
//            if (session != nil) {
//                print(session!.userName)
//                print(session!.userID)
//                print(session!.authToken)
//            } else {
//                print("error")
//            }
//        }
        
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if session != nil {
                print("signed in as \(session!.userName)");
                let client = TWTRAPIClient.withCurrentUser()
                let request = client.urlRequest(withMethod: "GET",
                                                urlString: "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true",
                                                parameters: ["include_email": "true", "skip_status": "true"],
                                                error: nil)
                client.sendTwitterRequest(request) { response, data, connectionError in
                    if (connectionError == nil) {
                        do{
                            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                            print("Json response: ", json)
                            self.firstName = json["name"] as! String
                            self.lastName = json["screen_name"] as! String
                            self.email = json["email"] as! String
                            //let userId = json["id"] as! String
                             self.checkString = "Twitter"
                            let x : Int = json["id"] as! Int
                            let xNSNumber = x as NSNumber
                            self.userId = xNSNumber.stringValue
                            
                            self.checkSocialLoginServiceCall()
                            
//                            let social = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialAccounts") as! SocialAccounts
//                            social.fbIdStr = self.userId
//                            social.fbEmailIdStr = self.email
//                            social.fbNameStr = self.firstName
//                            social.userNameStr = self.lastName
//                            social.loginTypeCheckStr = "twit"
//                            self.navigationController?.pushViewController(social, animated: false)
                        } catch {
                        }
                    }
                    else {
                        print("Error: \(connectionError!)")
                    }
                }
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
    }
    
    @objc func hideKeyboard(){
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
    }
    //MARK: FETCH CONTACTS
    func fetchContacts(){
        var count : Int = 0
        fetchContacts(completion: {contacts in
            contacts.forEach({
                
                print("Contact Name: \($0.givenName,$0.familyName), Mobile Number: \($0.phoneNumbers.first?.value.stringValue ?? "nil")")
                //var sorted = contacts.sort{ ($0["name"] as! String) < ($1["name"] as! String) }
                
                let ContactName = $0.givenName + $0.familyName + $0.organizationName
                let mobileNumber = $0.phoneNumbers.first?.value.stringValue ?? ""
                
                self.nameArray.append(ContactName)
                self.nameIdArray.append($0.identifier)
                self.numberArray.append(mobileNumber)
//                if UserDefaults.standard.object(forKey: "contacts") != nil {
//                    if self.selectedIDArray.contains($0.identifier) {
//                        self.checkArray.append("1")
//                        self.checkDict[$0.identifier] = "1"
//                        count += 1
//                    }
//                    else{
//                        self.checkArray.append("0")
//                        self.checkDict[$0.identifier] = "0"
//                    }
//                }
//                else{
//                    self.checkArray.append("0")
//                    self.checkDict[$0.identifier] = "0"
//                }
            })
            
//            self.displayedNamesArr = self.nameIdArray;
//            self.displayedCheckArr = self.checkArray;
//            self.displayedCheckFilterArr = self.checkArray;
//
//            let totalItems:NSArray = self.nameArray as NSArray
//            self.totalTagsArray.addObjects(from: totalItems as! [Any])
//            print("total data array",self.totalTagsArray)
//            self.contactsDataArray = self.totalTagsArray
//
//            //self.nameArray = self.nameArray.sort
//            self.arrayCount = self.nameArray.count
//            self.checkArrayCount = self.checkArray.count
            
            // print("checkArrayCount Is :\(self.checkArrayCount)")
            //print("Name Array Is :\(self.nameArray)")
            //print("Number Array Is :\(self.numberArray)")
//
//            UserDefaults.standard.set(self.nameArray, forKey: "names")
//            UserDefaults.standard.set(self.numberArray, forKey: "numbers")
//
//            DispatchQueue.main.async {
//                if (count != 0){
//                    self.selectedContactsLabel.text = String(format: "%d %@", count,self.languageChangeString(a_str: "Selected")!)
//                }
//                self.totalContactsLabel.text = String(format: "%@(%d)", self.languageChangeString(a_str: "YOUR CONTACTS")!,self.displayedNamesArr.count)
//                self.tableViewList.reloadData()
//
//            }
            
            // let sortedContactArray = contacts.sort{ ($0["name"] as! String) < ($1["name"] as! String) }
        })
    }
    //MARK:CONTACTS FETCH CNCONTACT
    func fetchContacts(completion: @escaping (_ result: [CNContact]) -> Void){
        DispatchQueue.main.async {
            var results = [CNContact]()
            let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactMiddleNameKey,CNContactEmailAddressesKey,CNContactPhoneNumbersKey,CNContactOrganizationNameKey] as [CNKeyDescriptor]
            let fetchRequest = CNContactFetchRequest(keysToFetch: keys)
            fetchRequest.sortOrder = CNContactSortOrder.givenName
            let store = CNContactStore()
            store.requestAccess(for: .contacts, completionHandler: {(grant,error) in
                if grant{
                    do {
                        self.contactList = []
                        try store.enumerateContacts(with: fetchRequest, usingBlock: { (contact, stop) -> Void in
                            results.append(contact)
                            self.contactList.append(contact)
                        })
                        //print("Result Is :\(results)")
                        // print("Result Is :\(self.contactList)")
                    }
                    catch let error {
                        print(error.localizedDescription)
                    }
                    completion(results)
                }else{
                    print("Error \(error?.localizedDescription ?? "")")
                }
            })
        }
    }
    //MARK:- Social login service call check if already exist user or new user
    
    func checkSocialLoginServiceCall(){
        
        // http://volive.in/voicek/api/services/social_check
        Services.sharedInstance.loader(view: self.view)
        let parameters: Dictionary<String, Any>
        tokenString = UserDefaults.standard.object(forKey: "deviceToken") as? String
        var suthId = NSString()
        if self.checkString == "FaceBook"{
            suthId = self.fbIdString! as NSString
        }else{
            suthId = self.userId! as NSString
        }
        
        let signin = "\(Base_Url)social_check"
        if ((tokenString?.count) != nil)  {
            parameters = [ "API-KEY" :APIKEY, "oauth_uid":suthId, "device":"iOS", "token" :  tokenString!]
        }
        else
        {
            parameters = [ "API-KEY" :APIKEY, "oauth_uid":suthId,"device":"iOS", "token" :  "fkdjh72364gfdgd72346ndri"]
        }
        
        print(parameters)
        
        Alamofire.request(signin, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            let responseData = response.result.value as? Dictionary<String, Any>
            print("Login Response data Is:\(responseData!)")
            
            let status = responseData!["status"] as! Int
            let message = responseData!["message"] as! String
            
            if status == 0
            {
               
                if self.checkString == "FaceBook"{
                    let social = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialAccounts") as! SocialAccounts
                    social.fbIdStr = self.fbIdString
                    social.fbEmailIdStr = self.fbEmailIdString
                    social.fbNameStr = self.fbNameString
                    social.userNameStr = self.userNameString
                    social.imageUrlStr = self.pictureUrlString
                    social.loginTypeCheckStr = "fb"
                    self.navigationController?.pushViewController(social, animated: false)
                }else{
                    let social = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SocialAccounts") as! SocialAccounts
                    social.fbIdStr = self.userId
                    social.fbEmailIdStr = self.email
                    social.fbNameStr = self.firstName
                    social.userNameStr = self.lastName
                    social.loginTypeCheckStr = "twit"
                    self.navigationController?.pushViewController(social, animated: false)
                }
            }else if status == 1{
                                //UserDefaults.standard.set(self.emailTF.text, forKey: "email")
                                //UserDefaults.standard.set(self.passwordTF.text, forKey: "password")
                
                                Services.sharedInstance.dissMissLoader()
                                //self.showToastForAlert(message: message)
                
                                let userId = responseData!["user_id"] as! String
                                let email = responseData!["email"] as! String
                                let userName = responseData!["username"] as! String
                                let fullName = responseData!["first_name"] as! String
                                let mobile = responseData!["mobile"] as! String
                                let image = responseData!["image"] as! String
                                let is_public = responseData!["is_public"] as! String
                                let bio = responseData!["bio"] as! String
                
                                let new_comments = responseData!["new_comments"] as! String
                                let new_mentions = responseData!["new_mentions"] as! String
                                let new_listeners = responseData!["new_listeners"] as! String
                                let new_likes = responseData!["new_likes"] as! String
                                //let loginType = responseData!["social_type"] as! String
                
                                print("new_comments is :\(new_comments)")
                                print("new_mentions is :\(new_mentions)")
                                print("new_listeners is :\(new_listeners)")
                                print("new_likes is :\(new_likes)")
                
                                UserDefaults.standard.set(userId, forKey: "userid")
                                UserDefaults.standard.set(email, forKey: "email")
                                UserDefaults.standard.set(userName, forKey: "userName")
                                UserDefaults.standard.set(fullName, forKey: "first_name")
                                UserDefaults.standard.set(mobile, forKey: "mobile")
                                UserDefaults.standard.set(image, forKey: "image")
                                UserDefaults.standard.set(is_public, forKey: "is_public")
                                UserDefaults.standard.set(bio, forKey: "bio")
                                UserDefaults.standard.set(new_comments, forKey: "newComments")
                                UserDefaults.standard.set(new_mentions, forKey: "newMentions")
                                UserDefaults.standard.set(new_listeners, forKey: "newListeners")
                                UserDefaults.standard.set(new_likes, forKey: "newLikes")
                //UserDefaults.standard.set(loginType, forKey: "loginType")
                let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC1")
                //UserDefaults.standard.set("home", forKey: "StringType")
                UserDefaults.standard.set(true, forKey: "Loggedin")
                self.present(homeVC, animated: false, completion: nil)
            }
            else
            {
                self.showToastForError(message: message)
                Services.sharedInstance.dissMissLoader()
            }
        }
    }
}
