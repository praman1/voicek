//
//  BlockedAccounts.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class BlockedAccounts: UIViewController {

    //hideContent
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var blockedLbl: UILabel!
    @IBOutlet weak var decrLbl: UILabel!
    var ownerUserIdStr : String!
    var blockedUserIdStr : String!
    var testStr : String!
    
    var blockedUsersData : [[String : AnyObject]]!
    
    //main Content
    @IBOutlet weak var blockedTable: UITableView!
    

    
    var blocked_userIdArray = [String]()
    var nameArray = [String]()
    var user_nameArray = [String]()
    var is_publicArray = [String]()
    var imageArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = languageChangeString(a_str: "Blocked Accounts")
        
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        blockedUsersListServiceCall()
       
    }
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:BLOCKED USERS LIST SERVICE CALL
    func blockedUsersListServiceCall()
    {
        ownerUserIdStr = (UserDefaults.standard.object(forKey: "userid") as! String)
        // http://volive.in/voicek/api/services/blocked_users?API-KEY=225143&user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let my_activities = "\(Base_Url)blocked_users?"
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(my_activities, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                //let message = responseData["message"] as! String
                if status == 1
                {
                    self.blocked_userIdArray = [String]()
                    self.nameArray = [String]()
                    self.user_nameArray = [String]()
                    self.is_publicArray = [String]()
                    self.imageArray = [String]()
                    
                    Services.sharedInstance.dissMissLoader()
                    
                    self.blockedUsersData = responseData["data"] as? [[String:AnyObject]]
                    
                    for each in self.blockedUsersData
                    {
                        //    blocked_user: "3783858425",
                        //    name: "Volive Solutions",
                        //    user_name: "volive",
                        //    is_public: "2",
                        //    image:
                        let blocked_user = each["blocked_user"] as? String
                        let name = each["name"] as? String
                        let user_name = each["user_name"] as? String
                        let is_public = each["is_public"] as? String
                        let image = each["image"] as? String
                        
                        self.blocked_userIdArray.append(blocked_user!)
                        self.nameArray.append(name!)
                        self.user_nameArray.append(user_name!)
                        self.is_publicArray.append(is_public!)
                        self.imageArray.append(image!)
                        
                    }
                    
                    print("Image Array Is:\(self.imageArray)")
                    DispatchQueue.main.async {
                        
                        self.blockedTable.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.blocked_userIdArray = [String]()
                        self.blockedTable.reloadData()
                        if self.testStr == "1"{
                            let message = responseData["message"] as! String
                            self.showToastForError(message: message)
                        }
                        
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    //MARK:UNBLOCK USER SERVICE CALL
    func unBlockUserServiceCall()
    {
        // http://volive.in/voicek/api/services/block_user?API-KEY=225143&user_id=3783858425&profile_user_id=4182668577
        Services.sharedInstance.loader(view: self.view)
        let block_user = "\(Base_Url)block_user?"
        print("block_user URL Is :\(block_user)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserIdStr! , "profile_user_id" : blockedUserIdStr!]
        
        print(parameters)
        
        Alamofire.request(block_user, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                let message = responseData["message"] as! String
                
                
                if status == 1
                {
                    // let blocked = responseData["blocked"] as! Int
                    
//                    let x : Int = responseData["blocked"] as! Int
//                    let xNSNumber = x as NSNumber
//                    self.blockStr  = xNSNumber.stringValue
//
//                    if self.blockStr == "1"
//                    {
//                        self.blockStr = "Unblock"
//
//                    }else if self.blockStr == "2"
//                    {
//                        self.blockStr = "Block"
//                    }
                    
                    Services.sharedInstance.dissMissLoader()
                    self.blockedUsersListServiceCall()
                    self.showToastForAlert(message: message)
                }
                else
                {
                    DispatchQueue.main.async {
                        self.showToastForError(message: message)
                        Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
    
    @objc func unblockClicked(sender:UIButton){
        let buttonRow = sender.tag
        blockedUserIdStr = blocked_userIdArray [buttonRow]
        unBlockUserServiceCall()
    }
 
}

extension BlockedAccounts: UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return blocked_userIdArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var  cell : BlockedCell
        
        cell = blockedTable.dequeueReusableCell(withIdentifier: "BlockedCell", for: indexPath) as! BlockedCell
        cell.unblock.setTitle(languageChangeString(a_str: "Unblock"), for: UIControlState.normal)
        cell.blockedImg.layer.cornerRadius = cell.blockedImg.frame.size.height/2
        cell.nameLbl.text = nameArray[indexPath.row]
        cell.emailLbl.text = String(format: "@%@", user_nameArray[indexPath.row])
        cell.blockedImg.sd_setImage(with: URL (string: imageArray[indexPath.row]), placeholderImage: UIImage(named: "Logo"))
        cell.unblock.tag = indexPath.row
        cell.unblock.addTarget(self, action: #selector(unblockClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 87
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
}
