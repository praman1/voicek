//
//  AboutUs.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit
import Alamofire

class AboutUs: UIViewController {

    @IBOutlet var voicekNameLabel: UILabel!
    @IBOutlet weak var voicekImage: UIImageView!
    
    @IBOutlet var versionLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    
    var ownerUserId : String!
    var dataDict = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ownerUserId = (UserDefaults.standard.object(forKey: "userid") as! String)
        self.navigationItem.title = languageChangeString(a_str: "About Us")
        self.voicekNameLabel.text = languageChangeString(a_str: "Voicek")
        self.versionLabel.text = languageChangeString(a_str: "Version 1.0")
        let backBtn = UIBarButtonItem(image: UIImage(named:"Back"), style: .plain, target: self, action: #selector(backBtnClicked))
        backBtn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = backBtn
        
        voicekImage.clipsToBounds = false
        voicekImage.layer.shadowColor = UIColor.init(red: 0.0/255.0, green: 226.0/255.0, blue: 174.0/255.0, alpha: 1).cgColor
        voicekImage.layer.shadowOpacity = 1
        voicekImage.layer.shadowOffset = CGSize.zero
        voicekImage.layer.shadowRadius = 3
        //voicekImage.layer.shadowPath = UIBezierPath(roundedRect: outerView.bounds, cornerRadius: 10).cgPath
        
        aboutUsServiceCall()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0, animations: {
                
                self.voicekImage.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                
            }) { finished in
                UIView.animate(withDuration: 0.4, animations: {
                    
                    self.voicekImage.transform = CGAffineTransform.identity
                    
                }) { finished in
                    
                }
                
            }
            
        })
    }
    
    @objc func backBtnClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:ABOUT US SERVICE CALL
    func aboutUsServiceCall()
    {
        // http://volive.in/voicek/api/services/about?API-KEY=225143&user_id=3783858425
        //Services.sharedInstance.loader(view: self.view)
        let aboutUs = "\(Base_Url)about?"
        print("aboutUs URL Is :\(aboutUs)")
        
        let parameters: Dictionary<String, Any> = [ "API-KEY" :APIKEY , "user_id" : ownerUserId! ]
        
        print(parameters)
        
        Alamofire.request(aboutUs, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            if let responseData = response.result.value as? Dictionary<String, Any>{
                print(responseData)
                
                let status = responseData["status"] as! Int
                
                
                self.dataDict = responseData["data"] as! [String : String]
                
                if status == 1
                {
                    let description = self.dataDict["description"]
                    let attrStr = try! NSAttributedString(
                        data: (description?.data(using: .unicode, allowLossyConversion: true)!)!,
                        options:[.documentType: NSAttributedString.DocumentType.html,
                                 .characterEncoding: String.Encoding.utf8.rawValue],
                        documentAttributes: nil)
                    
                    self.aboutTextView.attributedText = attrStr
                    self.aboutTextView.font = UIFont(name: self.aboutTextView.font!.fontName, size: 16)
                    print("strr",attrStr)
                    
                    //Services.sharedInstance.dissMissLoader()
                }
                else
                {
                    DispatchQueue.main.async {
                        let message = responseData["message"] as! String
                        self.showToastForError(message: message)
                        //Services.sharedInstance.dissMissLoader()
                    }
                }
            }
        }
    }
}
