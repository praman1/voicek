//
//  ActiveLabelClickHandler.swift
//  Voicek
//
//  Created by A Sherbeeni on 7/5/17.
//  Copyright © 2017 Dopravo. All rights reserved.
//

import Foundation


/**
Added this protocol to be implemented by presented view controllers. 
just in case the handle/hashtag click has come from a UITableViewCell
 */
@objc protocol ActiveLabelClickDelegate{
    
    @objc optional func didClick(username: String)
    @objc optional func didClick(hashtag: String)
    @objc optional func didClick(url: URL)
    
}
