//
//  SideMenuCell.swift
//  Voicek
//
//  Created by Suman Guntuka on 20/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var menuNameLbl: UILabel!
    
    @IBOutlet weak var menuImg: UIImageView!
    
    @IBOutlet weak var selectView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
