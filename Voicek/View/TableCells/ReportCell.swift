//
//  ReportCell.swift
//  Voicek
//
//  Created by volivesolutions on 11/09/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet var checkBtn: UIButton!
    @IBOutlet var checkImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
