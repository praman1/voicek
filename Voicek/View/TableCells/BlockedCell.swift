//
//  BlockedCell.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class BlockedCell: UITableViewCell {

    @IBOutlet weak var blockedImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var unblock: UIButton!
    // findFriends Cell
    
    @IBOutlet weak var frndImg: UIImageView!
    
    @IBOutlet weak var frndEmailLbl: UILabel!
    @IBOutlet weak var frndLbl: UILabel!
    
    @IBOutlet weak var listenBtn: UIButton!
    @IBOutlet weak var noOfPostsLbl: UILabel!
    
    @IBOutlet weak var listen: UIButton!
    //comments Cell
    @IBOutlet var commentImage: UIImageView!
    @IBOutlet var timeAgoLbl: UILabel!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var commentLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
