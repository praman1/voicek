//
//  FiltersCell.swift
//  Voicek
//
//  Created by volivesolutions on 16/01/19.
//  Copyright © 2019 Volivesolutions. All rights reserved.
//

import UIKit

class FiltersCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var filterNameLabel: UILabel!
    
}
