//
//  TagsCell.swift
//  Voicek
//
//  Created by volivesolutions on 28/08/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class TagsCell: UITableViewCell {

    @IBOutlet weak var noOfPostsLabel: UILabel!
    @IBOutlet weak var tagNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
