//
//  OnboardCell.swift
//  Voicek
//
//  Created by Suman Guntuka on 21/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class OnboardCell: UICollectionViewCell {
    @IBOutlet weak var onboardImg: UIImageView!
    
    @IBOutlet weak var decsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var titleBtn: UIButton!
}
