//
//  HomeCell.swift
//  Voicek
//
//  Created by Suman Guntuka on 23/07/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell
{

    @IBOutlet weak var homeImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var likesLBl: UILabel!
    @IBOutlet weak var playedLbl: UILabel!
    @IBOutlet weak var subNameLbl: UILabel!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var secLbl: UILabel!
    
    @IBOutlet var badgeLabel: UILabel!
    
    @IBOutlet var activityBadgeLabel: UILabel!
    @IBOutlet var postsBadgeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
