//
//  FrequentQnsCell.swift
//  Voicek
//
//  Created by volivesolutions on 08/10/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class FrequentQnsCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
