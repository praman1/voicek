//
//  FollowReq.swift
//  Voicek
//
//  Created by volivesolutions on 24/09/18.
//  Copyright © 2018 Volivesolutions. All rights reserved.
//

import UIKit

class FollowReq: UITableViewCell {

    @IBOutlet weak var reqImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
