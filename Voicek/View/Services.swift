//
//  Services.swift
//  Perfume
//
//  Created by murali krishna on 11/1/17.
//  Copyright © 2017 volivesolutions. All rights reserved.

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import CRNotifications


class Services : NSObject {
    
    static let sharedInstance = Services()
    
    //signUp getperamters
    var _name: String!
    var _email: String!
    var _mobileNumber: String!
    var _message: String!

    var errMessage: String!
    let imageV = UIImageView()
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    func loader(view: UIView) -> () {
        
        indicator.frame = CGRect(x: 0,y: 0,width: 75,height: 75)
        indicator.layer.cornerRadius = 8
        indicator.center = view.center
        indicator.color = UIColor.init(red: 0.0/255.0, green: 228.0/255.0, blue: 218.0/255.0, alpha: 1.0)
        view.addSubview(indicator)
        indicator.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        indicator.bringSubview(toFront: view)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        indicator.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func dissMissLoader()  {
        
         indicator.stopAnimating()
        imageV.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
 
}
extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

//// Validation Toasts



extension UIViewController {
    
    func showToastForAlert(message : String) {
        
        CRNotifications.showNotification(type: CRNotifications.success, title: languageChangeString(a_str: "Success!")!, message: message, dismissDelay: 2, completion: {
        })
    }
    
    func showToastForError(message : String) {
        
        CRNotifications.showNotification(type: CRNotifications.error, title: languageChangeString(a_str: "Alert!")!, message: message, dismissDelay: 2)
    }
}

extension UIViewController {
    
    func showToast(message : String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "LogoHome") //Replace the IconImage text with the image name
        alertView.showInfo( "Alert!" , subTitle: message ,closeButtonTitle: "DONE" , circleIconImage: alertViewIcon)
    }
   
//    func showToastForAlert(message : String) {
//        
//                let message = message
//                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
//        
//                self.present(alert, animated: true)
//                // duration in seconds
//                let duration: Double = 3
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
//                    alert.dismiss(animated: true)
//        
//    }
//}
    
    func showToastForInternet (message : String) {
        
        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        self.present(alert, animated: true)
        // duration in seconds
        let duration: Double = 3
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
    func languageChangeString(a_str: String) -> String?{

        var path: Bundle?
        var selectedLanguage:String = String()
        //UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
        // selectedLanguage = UserDefaults.standard.integer(forKey: )
        selectedLanguage = UserDefaults.standard.string(forKey: "currentLanguage") ?? ""

        if selectedLanguage == ENGLISH_LANGUAGE {
            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        else if selectedLanguage == ARABIC_LANGUAGE {
            if let aType = Bundle.main.path(forResource: "ar", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        else {
            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
                path = Bundle(path: aType)!
            }
        }
        // var languageBundle = Bundle(path: path)

        let str: String = NSLocalizedString(a_str, tableName: "LocalizableVoicek", bundle: path!, value: "", comment: "")

        return str
    }
}

extension String {
    
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return self.substring(to: range.lowerBound)
        }
        
        return self.substring(from: range.upperBound)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

